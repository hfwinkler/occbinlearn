% solve CCHH model with global projection method
% version with endogenous housing
% and learning

%% parameters

%model parameters
betta=1.035^-0.25;
R=1.03^0.25;
Ybar=1;
rhoy=0.9;
sigy=1e-2;
theta=0.5;
delta=0.025;
psi=2;
chi=1;

g=0.002;
sigq=1e-2;
rhomu=0.9;

%algorithm parameters
minC=1e-6;
minH=1e-6;
maxiter=1e3;
convcrit=1e-4;
lambda=0.1;
plotfreq=2;

%% non-stochastic steady-state

Hss=chi*Ybar/(1-betta*(1-delta)-theta*(1-betta*R)+chi*delta+chi*theta*(R-1));
Css=Ybar-delta*Hss-(R-1)*theta*Hss;
Qss=1;
Bss=theta*Hss*Qss;

%% set up grid and nodes

%income grid
ymin=log(Ybar)-3*sigy/sqrt(1-rhoy^2);
ymax=log(Ybar)+3*sigy/sqrt(1-rhoy^2);
Ky=5;
grid.onlyy=linspace(ymin,ymax,Ky);

%debt grid
Bmin=Bss*0.5;
Bmax=Bss*1.5;
KB=15;
grid.onlyB=linspace(Bmin,Bmax,KB);

%housing grid
Hmin=Hss*0.5;
Hmax=Hss*2;
KH=15;
grid.onlyH=linspace(Hmin,Hmax,KH);

%belief grid
mumin=-.001;
mumax=.001;
Kmu=5;
grid.onlymu=linspace(mumin,mumax,Kmu);

%price grid
qmin=log(Qss*0.1);
qmax=log(Qss*10);
Kq=21;
grid.onlyq=linspace(qmin,qmax,Kq);

%overall grid
[grid.y, grid.B, grid.H, grid.mu, grid.q]=ndgrid(grid.onlyy,grid.onlyB,grid.onlyH,grid.onlymu,grid.onlyq);
grid.length=Ky*KB*KH*Kmu*Kq;
fprintf('Nodes: %i\n',grid.length);

%number of quadrature nodes
J=3;
%calculate Gauss-Hermite nodes and weights
CM      = diag(sqrt((1:(J-1))/2),1) + diag(sqrt((1:(J-1))/2),-1);
[V, L]   = eig(CM);
[zeta, ind] = sort(diag(L));
V       = V(:,ind)';
w       = V(:,1).^2; %don't multiply by sqrt(pi) here

%for two shocks
[zzeta1, zzeta2]=ndgrid(zeta,zeta);
ww=w*w';

%for three shocks
[zzzeta1, zzzeta2, zzzeta3]=ndgrid(zeta,zeta,zeta);
www=reshape(kron(w,ww),[J J J]);

%% initial guess for PLM

Hold=grid.H;
Cold=max(exp(grid.y)-delta*Hold-(R-1)*grid.B,minC);

%Hold=Hss*ones(size(grid.y));
%Cold=Css*ones(size(grid.y));

Hnew=Hold;
Cnew=Cold;

%auxiliaries:
Bnew=grid.B;
Xnew=ones(size(grid.y));

%load intermittent results from previous run?
%load cchhtemp;

%% iterate

options=optimset('Display','off','MaxFunEvals',1000,'TolFun',1e-5,'Algorithm','trust-region-dogleg');
Pifun=@(Q,Hminus) Hminus/psi*( (Q-1)*(Q-1+delta*psi)-0.5*(Q-1)^2 );

h=figure;
for n=1:maxiter
    %walk through grid
    FC=griddedInterpolant(grid.y,grid.B,grid.H,grid.mu,grid.q,Cold,'linear','linear');
    parfor node=1:grid.length %can be replaced by parfor if grid is large
        %get grid points
        Y=exp(grid.y(node));
        Q=exp(grid.q(node));
        Bminus=grid.B(node);
        Hminus=grid.H(node);
        W=(1-delta)*Q*Hminus-R*Bminus;
        Pi=Pifun(Q,Hminus);
        %current period:
        Htemp=Hold(node);
        Ctemp=Cold(node);
        Btemp=Ctemp-Y-W+Q*Htemp-Pi;
        %next period:
        y1=rhoy*grid.y(node)+(1-rhoy)*log(Ybar)+sigy*zzzeta1;
        q1=log(Q)+grid.mu(node)+sigq*zzzeta2;
        Htemp=Htemp*ones(J,J,J);
        Btemp=Btemp*ones(J,J,J);
        mu1=rhomu*grid.mu(node)+g*sigq*zzzeta3;
        
        %prevent falling off the grid?
        y1=max(min(y1,ymax),ymin);
        Btemp=max(min(Btemp,Bmax),Bmin);
        Htemp=max(min(Htemp,Hmax),Hmin);
        q1=max(min(q1,qmax),qmin);
        mu1=max(min(mu1,mumax),mumin);
        
        C1=FC(y1,Btemp,Htemp,mu1,q1); %#ok<*PFBNS>
        C1=max(C1,minC); %if future consumption negative, set MU very high
        %take expectation with quadrature
        integrand1=1./C1;
        k1=integrand1(:)'*www(:);
        integrand2=exp(q1)./C1;
        k2=integrand2(:)'*www(:);
        %get new solution: try unconstrained first
        C=1/(betta*R*k1);
        H=chi*C/(Q-betta*C*k2*(1-delta));
        B=C-Y-W+Q*H-Pi;
        if B>theta*Q*H %if borrowing constraint violated, try constrained
            a1=-1/Q/(1-theta)*(theta*betta*R*k1*Q-betta*k2*(1-delta));
            b1=-(Y+W+Pi)*a1-1-chi;
            c1=Y+W+Pi;
            C=0.5/a1*(-b1+sqrt(b1^2-4*a1*c1));
            H=(-C+Y+W+Pi)/Q/(1-theta);
            if H<0
                H=minH;
                C=Y+W+Pi-(1-theta)*Q*H;
            end
            B=theta*Q*H;            
        end
        Cnew(node)=max(C,minC);
        Hnew(node)=max(H,minH);
        
        Bnew(node)=B;
        Xnew(node)=Pi;%B/(theta*Q*H);
    end
    
    %measure distance to old policy function and stop if it's small
    dist=max(abs(Cnew(:)-Cold(:)));
    test=find(abs(Cnew(:)-Cold(:))==dist);
    
    if dist<convcrit
        fprintf('#%i: distance: %3.3g\n',n,dist);
        break; 
    end
    %update policy function with attenuation
    Hold=(1-lambda)*Hnew+lambda*Hold;
    Cold=(1-lambda)*Cnew+lambda*Cold;
    save cchhtemp Cold Hold;
    
    if mod(n,plotfreq)==0 %plot policy function
        fprintf('#%i: distance: %3.3g\n',n,dist);
        subplot(1,2,1);
        surf(grid.onlyy,grid.onlyB,Cnew(:,:,round(KH/2),round(Kmu/2),round(Kq/2))'/Css);   
        xlabel('y'); xlim([ymin ymax]);
        ylabel('B'); ylim([Bmin Bmax]);
        zlabel('C/Css');
        subplot(1,2,2);
        surf(grid.onlymu,grid.onlyq,squeeze(Cnew(round(Ky/2),round(KB/2),round(KH/2),:,:))'/Css); 
        xlabel('mu'); xlim([mumin mumax]);
        ylabel('q'); ylim([qmin qmax]);
        zlabel('C/Css');
        drawnow;
        
        %disp([grid.y(test) grid.B(test) grid.H(test) grid.mu(test) grid.q(test)]');
    end
end
close(h);

%% plot PLM

figure; 
subplot(1,2,1);
surf(grid.onlyy,grid.onlyB,Cnew(:,:,round(KH/2),round(Kmu/2),round(Kq/2))'); 
xlabel('y'); xlim([ymin ymax]);
ylabel('B'); ylim([Bmin Bmax]);
zlabel('C');
subplot(1,2,2);
surf(grid.onlymu,grid.onlyq,squeeze(Cnew(round(Ky/2),round(KB/2),round(KH/2),:,:))'); 
xlabel('mu'); xlim([mumin mumax]);
ylabel('q'); ylim([qmin qmax]);
zlabel('C');
drawnow;

figure; 
subplot(1,2,1);
surf(grid.onlyy,grid.onlyB,Bnew(:,:,round(KH/2),round(Kmu/2),round(Kq/2))'); 
xlabel('y'); xlim([ymin ymax]);
ylabel('B'); ylim([Bmin Bmax]);
zlabel('B');
subplot(1,2,2);
surf(grid.onlymu,grid.onlyq,squeeze(Bnew(round(Ky/2),round(KB/2),round(KH/2),:,:))'); 
xlabel('mu'); xlim([mumin mumax]);
ylabel('q'); ylim([qmin qmax]);
zlabel('B');
drawnow;


figure; 
subplot(1,2,1);
surf(grid.onlyy,grid.onlyB,Xnew(:,:,round(KH/2),round(Kmu/2),round(Kq/2))'); 
xlabel('y'); xlim([ymin ymax]);
ylabel('B'); ylim([Bmin Bmax]);
zlabel('X');
subplot(1,2,2);
surf(grid.onlymu,grid.onlyq,squeeze(Xnew(round(Ky/2),round(KB/2),round(KH/2),:,:))'); 
xlabel('mu'); xlim([mumin mumax]);
ylabel('q'); ylim([qmin qmax]);
zlabel('X');
drawnow;

figure; 
Hsupply=grid.H.*(1+(exp(grid.q)-1)/psi);
subplot(1,2,1);
surf(grid.onlyy,grid.onlyB,Hnew(:,:,round(KH/2),round(Kmu/2),round(Kq/2))'); 
hold on;
surf(grid.onlyy,grid.onlyB,Hsupply(:,:,round(KH/2),round(Kmu/2),round(Kq/2))','FaceAlpha',0.33);
xlabel('y'); xlim([ymin ymax]);
ylabel('B'); ylim([Bmin Bmax]);
zlabel('H');
subplot(1,2,2);
surf(grid.onlymu,grid.onlyq,squeeze(Hnew(round(Ky/2),round(KB/2),round(KH/2),:,:))'); 
hold on;
surf(grid.onlymu,grid.onlyq,squeeze(Hsupply(round(Ky/2),round(KB/2),round(KH/2),:,:))','FaceAlpha',0.33);
xlabel('mu'); xlim([mumin mumax]);
ylabel('q'); ylim([qmin qmax]);
zlabel('H');
drawnow;

%% solve for ALM

%store PLM
CPLM=Cnew;
HPLM=Hnew;
BPLM=Bnew;

%set up grid:

gridALM.onlyy=grid.onlyy;
gridALM.onlyB=grid.onlyB;
gridALM.onlyH=grid.onlyH;
gridALM.onlymu=grid.onlymu;
[gridALM.y, gridALM.B, gridALM.H, gridALM.mu]=ndgrid(gridALM.onlyy,gridALM.onlyB,gridALM.onlyH,gridALM.onlymu);
gridALM.length=Ky*KB*KH*Kmu;

qALM=zeros(size(gridALM.mu));
CALM=zeros(size(gridALM.mu));
BALM=zeros(size(gridALM.mu));
HALM=zeros(size(gridALM.mu));
XALM=zeros(size(gridALM.mu));

FC=griddedInterpolant(grid.y,grid.B,grid.H,grid.mu,grid.q,CPLM,'spline','linear');
FH=griddedInterpolant(grid.y,grid.B,grid.H,grid.mu,grid.q,HPLM,'spline','linear');

options=optimset('Display','off');
troubleshooter=zeros(size(gridALM.mu));
for node=1:gridALM.length
    y=gridALM.y(node);
    mu=gridALM.mu(node);
    Bminus=gridALM.B(node);
    Hminus=gridALM.H(node);
    fun=@(q) FH(y,Bminus,Hminus,mu,q)-(1-delta)*Hminus-Hminus*(exp(q)-1+delta*psi)/psi;
    Hlowq=fun(qmin);
    Hhighq=fun(qmax);
    if sign(Hlowq)==-sign(Hhighq)
        [q,~,exitflag]=fzero(fun,[qmin qmax]);
    else
        [q,~,exitflag]=fsolve(fun,qmax,options);
        if exitflag<0
            %if sign(fun(0))>0; q=qmax; else q=qmin; end
            troubleshooter(node)=sign(fun(0));
            plot(grid.onlyq,arrayfun(fun,grid.onlyq));
            %keyboard;
        end
    end
    H=FH(y,Bminus,Hminus,mu,q);
    C=FC(y,Bminus,Hminus,mu,q);
    B=C-exp(y)+R*Bminus+H-(1-delta)*Hminus+psi/2*(H/Hminus-1)^2*Hminus;
    if B>(theta*exp(q)*H)
        B=theta*exp(q)*H;
        C=exp(y)+B-R*Bminus-H+(1-delta)*Hminus-psi/2*(H/Hminus-1)^2*Hminus;
    end
    qALM(node)=q;
    CALM(node)=max(C,minC);
    BALM(node)=B;
    HALM(node)=H;
    XALM(node)=B/(theta*exp(q)*H);
end

%% plot ALM

figure; 
subplot(1,2,1);
surf(gridALM.onlyy,gridALM.onlyB/Bss,CALM(:,:,round(KH/2),round(Kmu/2))'); 
xlabel('y'); xlim([ymin ymax]);
ylabel('B/Bss'); ylim([Bmin Bmax]/Bss);
zlabel('C');
subplot(1,2,2);
surf(gridALM.onlyH/Hss,gridALM.onlymu,squeeze(CALM(round(Ky/2),round(KB/2),:,:))'); 
xlabel('H/Hss'); xlim([Hmin Hmax]/Hss);
ylabel('mu'); ylim([mumin mumax]);
zlabel('C');

figure; 
subplot(1,2,1);
surf(gridALM.onlyy,gridALM.onlyB/Bss,BALM(:,:,round(KH/2),round(Kmu/2))'); 
xlabel('y'); xlim([ymin ymax]);
ylabel('B/Bss'); ylim([Bmin Bmax]/Bss);
zlabel('B');
subplot(1,2,2);
surf(gridALM.onlyH/Hss,gridALM.onlymu,squeeze(BALM(round(Ky/2),round(KB/2),:,:))'); 
xlabel('H/Hss'); xlim([Hmin Hmax]/Hss);
ylabel('mu'); ylim([mumin mumax]);
zlabel('B');

figure; 
subplot(1,2,1);
surf(gridALM.onlyy,gridALM.onlyB/Bss,XALM(:,:,round(KH/2),round(Kmu/2))'); 
xlabel('y'); xlim([ymin ymax]);
ylabel('B/Bss'); ylim([Bmin Bmax]/Bss);
zlabel('X');
subplot(1,2,2);
surf(gridALM.onlyH/Hss,gridALM.onlymu,squeeze(XALM(round(Ky/2),round(KB/2),:,:))'); 
xlabel('H/Hss'); xlim([Hmin Hmax]/Hss);
ylabel('mu'); ylim([mumin mumax]);
zlabel('X');

figure; 
subplot(1,2,1);
surf(gridALM.onlyy,gridALM.onlyB/Bss,HALM(:,:,round(KH/2),round(Kmu/2))'); 
xlabel('y'); xlim([ymin ymax]);
ylabel('B/Bss'); ylim([Bmin Bmax]/Bss);
zlabel('H');
subplot(1,2,2);
surf(gridALM.onlyH/Hss,gridALM.onlymu,squeeze(HALM(round(Ky/2),round(KB/2),:,:))'); 
xlabel('H/Hss'); xlim([Hmin Hmax]/Hss);
ylabel('mu'); ylim([mumin mumax]);
zlabel('H');

figure; 
subplot(1,2,1);
surf(gridALM.onlyy,gridALM.onlyB/Bss,qALM(:,:,round(KH/2),round(Kmu/2))'); 
xlabel('y'); xlim([ymin ymax]);
ylabel('B/Bss'); ylim([Bmin Bmax]/Bss);
zlabel('q');
subplot(1,2,2);
surf(gridALM.onlyH/Hss,gridALM.onlymu,squeeze(qALM(round(Ky/2),round(KB/2),:,:))'); 
xlabel('H/Hss'); xlim([Hmin Hmax]/Hss);
ylabel('mu'); ylim([mumin mumax]);
zlabel('q');

figure; 
subplot(1,2,1);
surf(gridALM.onlyy,gridALM.onlyB/Bss,troubleshooter(:,:,round(KH/2),round(Kmu/2))'); 
xlabel('y'); xlim([ymin ymax]);
ylabel('B/Bss'); ylim([Bmin Bmax]/Bss);
zlabel('trouble');
subplot(1,2,2);
surf(gridALM.onlyH/Hss,gridALM.onlymu,squeeze(troubleshooter(round(Ky/2),round(KB/2),:,:))'); 
xlabel('H/Hss'); xlim([Hmin Hmax]/Hss);
ylabel('mu'); ylim([mumin mumax]);
zlabel('trouble');

%% simulation

T=40;
Tpre=1;
rng(1);
ysim=zeros(1,T);
Qsim=zeros(1,T);
Xsim=zeros(1,T);
Bsim=zeros(1,T);
Csim=zeros(1,T);
Hsim=zeros(1,T);
musim=zeros(1,T);
troublesim=zeros(1,T);

Blag=Bss*1;
Hlag=Hss*0.8;
ylag=log(Ybar);
mulag=0;
qlag=0;

FC=griddedInterpolant(gridALM.y,gridALM.B,gridALM.H,gridALM.mu,CALM,'linear','nearest');
FH=griddedInterpolant(gridALM.y,gridALM.B,gridALM.H,gridALM.mu,HALM,'linear','nearest');
FB=griddedInterpolant(gridALM.y,gridALM.B,gridALM.H,gridALM.mu,BALM,'linear','nearest');
Fq=griddedInterpolant(gridALM.y,gridALM.B,gridALM.H,gridALM.mu,qALM,'linear','nearest');
Ftrouble=griddedInterpolant(gridALM.y,gridALM.B,gridALM.H,gridALM.mu,troubleshooter,'linear','nearest');

for t=[ones(1,Tpre) 1:T]
    ysim(t)=rhoy*ylag+(1-rhoy)*log(Ybar)+sigy*randn(1)*(t>1);
    %if t>=10 && t<=20; ysim(t)=ymin; end
    Csim(t)=FC(ysim(t),Blag,Hlag,mulag);
    Hsim(t)=FH(ysim(t),Blag,Hlag,mulag);
    Qsim(t)=exp(Fq(ysim(t),Blag,Hlag,mulag));
    Bsim(t)=Csim(t)-exp(ysim(t))+R*Blag+Hsim(t)-(1-delta)*Hlag+psi/2*(Hsim(t)/Hlag-1)^2*Hlag;
    Xsim(t)=Bsim(t)/(theta*Qsim(t)*Hsim(t));
    if Xsim(t)>1
        Xsim(t)=1;
        Bsim(t)=theta*Qsim(t)*Hsim(t);
        Csim(t)=exp(ysim(t))+Bsim(t)-R*Blag-Hsim(t)+(1-delta)*Hlag-psi/2*(Hsim(t)/Hlag-1)^2*Hlag;
    end
    musim(t)=(rhomu-g)*mulag+g*(log(Qsim(t))-qlag)*(t>1);
    troublesim(t)=Ftrouble(ysim(t),Blag,Hlag,mulag);
    ylag=ysim(t);
    Blag=min(max(Bsim(t),Bmin),Bmax);
    Hlag=min(max(Hsim(t),Hmin),Hmax);
    mulag=min(max(musim(t),mumin),mumax);
    qlag=log(Qsim(t));
end

figure;
subplot(2,4,1); plot(1:T,ysim-log(Ybar)); title('income, log dev. from SS');
subplot(2,4,2); plot(1:T,log(Qsim/Qss)); title('house price, log dev from SS');
subplot(2,4,3); plot(1:T,musim); title('belief');
subplot(2,4,4); plot(1:T,Xsim,1:T,ones(1,T)); title('debt as a fraction of debt limit');
subplot(2,4,5); plot(1:T,log(Csim/Css)); title('consumption, log dev from SS');
subplot(2,4,6); plot(1:T,log(Hsim/Hss)); title('housing, log dev from SS');
subplot(2,4,7); plot(1:T,log(Bsim/Bss)); title('debt, log dev from SS');
subplot(2,4,8); plot(1:T,troublesim); title('trouble');
