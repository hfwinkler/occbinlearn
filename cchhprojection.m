% solve CCHH model with global projection method


%% parameters

%model parameters
betta=1.04^-0.25;
R=1.03^0.25;
Ybar=1;
rhoy=0.9;
sigy=1e-2;
theta=0.5;

%algorithm parameters
minC=1e-6;
convcrit=1e-6;
lambda=0.5;
plotfreq=10;

%% non-stochastic steady-state

Css=1/(1-theta*(1-R)/(1-(1-betta*R)*theta-betta))*Ybar;
Qss=1/(1-(1-betta*R)*theta-betta)*Css;
Bss=theta*Qss;

%% set up grid and nodes

%income grid
ymin=log(Ybar)-3*sigy/sqrt(1-rhoy^2);
ymax=log(Ybar)+3*sigy/sqrt(1-rhoy^2);
Ky=6;
grid.onlyy=linspace(ymin,ymax,Ky);

%debt grid
Bmin=Bss*0.5;
Bmax=Bss*1;
KB=40;
grid.onlyB=linspace(Bmin,Bmax,KB);

%overall grid
[grid.y, grid.B]=ndgrid(grid.onlyy,grid.onlyB);
grid.length=Ky*KB;
fprintf('Nodes: %i\n',grid.length);

%number of quadrature nodes
J=3;
%calculate Gauss-Hermite nodes and weights
CM      = diag(sqrt((1:(J-1))/2),1) + diag(sqrt((1:(J-1))/2),-1);
[V, L]   = eig(CM);
[zeta, ind] = sort(diag(L));
V       = V(:,ind)';
w       = V(:,1).^2; %don't multiply by sqrt(pi) here

%for two shocks
%[zzeta1, zzeta2]=ndgrid(zeta,zeta);
%ww=w*w';

%% initial guess for policy function

C0=max(Ybar-(R-1)*grid.B,minC);
Q0=ones(size(grid.y))*Qss;
Q0=max(Q0,(minC+R*grid.B-exp(grid.y))/theta);

Qold=Q0;
Qnew=Q0;
Cold=C0;
Cnew=C0;

%test:
Xnew=ones(size(grid.y));
Bflag=zeros(size(grid.y));

%% iterate

maxiter=1e4;
h=figure;
for n=1:maxiter
    %walk through grid
    FC=griddedInterpolant(grid.y,grid.B,Cold,'linear','linear');
    FQ=griddedInterpolant(grid.y,grid.B,Qold,'linear','linear');
    for node=1:grid.length %can be replaced by parfor if grid is large
        %get grid points
        y=grid.y(node);
        Bminus=grid.B(node);
        %current period:
        Ctemp=Cold(node);
        Qtemp=Qold(node);
        if exp(y)+theta*Qtemp-R*Bminus<minC
            %infeasible point
            Qtemp=(minC-exp(y)+R*Bminus)/theta;
            Ctemp=minC;
        end
        Btemp=Ctemp-exp(y)+R*Bminus;
        if Btemp>theta*Qtemp;
            Btemp=theta*Qtemp;
        end
        Btemp=Btemp*ones(J,1);
        %next period:
        y1=rhoy*y+(1-rhoy)*log(Ybar)+sigy*zeta;
        C1=FC(y1,Btemp); %#ok<*PFBNS>
        C1=max(C1,minC); %if future consumption negative, set MU very high
        Q1=FQ(y1,Btemp);
        %take expectation with quadrature
        integrand1=1./C1;
        k1=sum(integrand1.*w);
        integrand2=Q1./C1;
        k2=sum(integrand2.*w);
        %get new solution: try unconstrained first
        C=1/(betta*R*k1);
        Q=C*(1+betta*k2);
        B=C-exp(y)+R*Bminus;
        if B>theta*Qtemp %if borrowing constraint violated, try constrained
            B=theta*Qtemp; %take old policy for Q to compute B and C
            Bflag(node)=1;
            C=exp(y)+B-R*Bminus;
            lamC=1-betta*R*k1*C;
            Q=C*(1+betta*k2)/(1-lamC*theta); %then update Q
            Q=max(Q,0);
        else
            Bflag(node)=0;
        end
        if Q<0 || isnan(Q) || isinf(Q)
            error('Something wrong with Q');
        end
        Qnew(node)=Q;
        Xnew(node)=B/(theta*Qtemp);
        Cnew(node)=C;
    end
    
    %measure distance to old policy function and stop if it's small
    dist=max(abs(Qnew(:)-Qold(:)));
    if dist<convcrit
        fprintf('#%i: distance: %3.3g\n',n,dist);
        break; 
    end
    %update policy function with attenuation
    Qold=(1-lambda)*Qnew+lambda*Qold;
    Cold=(1-lambda)*Cnew+lambda*Cold;
    %save cchhtemp;
    
    if mod(n,plotfreq)==0 %plot policy function
        fprintf('#%i: distance: %3.3g\n',n,dist);
        min(Cnew(:))
        surf(grid.onlyy,grid.onlyB,Qnew'); 
        xlabel('y'); xlim([ymin ymax]);
        ylabel('B'); ylim([Bmin Bmax]);
        zlabel('Q');
        drawnow;
    end
end
close(h);

%% plot

figure; 
surf(grid.onlyy,grid.onlyB,Qnew'); 
xlabel('y'); xlim([ymin ymax]);
ylabel('B'); ylim([Bmin Bmax]);
zlabel('Q');
title('house price');
figure;
surf(grid.onlyy,grid.onlyB,(theta*Xnew.*Qnew)'); 
xlabel('y'); xlim([ymin ymax]);
ylabel('B'); ylim([Bmin Bmax]);
zlabel('B');
title('debt');
figure;
surf(grid.onlyy,grid.onlyB,Xnew'); 
xlabel('y'); xlim([ymin ymax]);
ylabel('B'); ylim([Bmin Bmax]);
zlabel('X');
title('debt as percent of constraint');
figure;
surf(grid.onlyy,grid.onlyB,Cnew'); 
xlabel('y'); xlim([ymin ymax]);
ylabel('B'); ylim([Bmin Bmax]);
zlabel('C');
title('consumption');

%% simulation

T=1e3;
Tpre=1e4;
rng(10);
ysim=zeros(1,T);
Qsim=zeros(1,T);
Xsim=zeros(1,T);
Bsim=zeros(1,T);
Csim=zeros(1,T);

Blag=Bss*0.5;
ylag=log(Ybar);

FC=griddedInterpolant(grid.y,grid.B,Cold,'linear','linear');
FQ=griddedInterpolant(grid.y,grid.B,Qold,'linear','linear');
    
for t=[ones(1,Tpre) 1:T]
    ysim(t)=rhoy*ylag+(1-rhoy)*log(Ybar)+sigy*randn(1)*(t>1);
    Csim(t)=FC(ysim(t),Blag);
    Qsim(t)=FQ(ysim(t),Blag);
    Bsim(t)=Csim(t)-exp(ysim(t))+R*Blag;
    Xsim(t)=Bsim(t)/(theta*Qsim(t));
    if Xsim(t)>1
        Xsim(t)=1;
        Bsim(t)=Qsim(t)*theta;
        Csim(t)=exp(ysim(t))-R*Blag+Bsim(t);
        if Csim(t)<0
            Csim(t)=nan;
            Bsim(t)=nan;
        end
    end
    ylag=ysim(t);
    Blag=Bsim(t);
end

figure;
subplot(2,3,1); plot(1:T,ysim-log(Ybar)); title('income, log dev. from SS');
subplot(2,3,2); plot(1:T,log(Qsim/Qss)); title('house price, log dev from SS');
subplot(2,3,3); plot(1:T,Xsim,1:T,ones(1,T)); title('debt as a fraction of debt limit');
subplot(2,3,4); plot(1:T,log(Bsim/Bss)); title('debt, log dev from SS');
subplot(2,3,5); plot(1:T,log(Csim/Css)); title('consumption, log dev from SS');
subplot(2,3,6); plot(1:T,1-Csim./exp(ysim)); title('savings rate');

fprintf('Average debt level:\t%3.3g \nAverage house price\t%3.3g\n',mean(Bsim(T-100:T)),mean(Qsim(T-100:T)));
