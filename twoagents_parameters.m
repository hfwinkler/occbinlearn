% parameter file for model with two households

%saver

%borrower
betta=0.9899;
gama=1;
nu=1;
eta=1;
phi=1;
theta=0.2;
chi=1;

%saver
bettas=0.99;
gamas=gama;
nus=nu;
etas=eta;
phis=phi;

%exogenous housing supply
Hbar=1;

%exogenous wage process
sigmaw=0.01;
rhow=0.9;

%learning
g=0.01;
sigmav=0.01;
