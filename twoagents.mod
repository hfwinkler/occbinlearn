% Dynare file to simulate a model with two households
% with the constraint always binding

var Cs Hs Ls B;
var Cb Hb Lb;
var W Q r;
var lambda;

varexo epsw;

parameters bettas gamas nus etas phis;
parameters theta betta gama nu eta phi chi;
parameters sigmaw rhow Hbar;

@#include "twoagents_parameters.m"

model;

%borrower
exp(Cb)=exp(W+Lb)+exp(Q)*(exp(Hb(-1))-exp(Hb))-B(-1)+B/(1+r);
B/(1+r)=theta*exp(Q+Hb);

log(eta)+phi*Lb+gama*Cb=W;
1-lambda=betta*exp(gama*(Cb-Cb(+1)))*(1+r);
exp(Q-gama*Cb)*(1-theta*lambda)=chi*exp(-nu*Hb)+betta*exp(-gama*Cb(+1)+Q(+1));

%saver
exp(Cs)=exp(W+Ls)+exp(Q)*(exp(Hs(-1))-exp(Hs))+B(-1)-B/(1+r);

log(etas)+phis*Ls+gamas*Cs=W;
1=bettas*exp(gamas*(Cs-Cs(+1)))*(1+r);
exp(Q-gamas*Cs)=exp(-nus*Hs)+bettas*exp(-gamas*Cs(+1)+Q(+1));

%housing supply
exp(Hs)+exp(Hb)=Hbar;

%exogenous wage
W=rhow*W(-1)+sigmaw*epsw;

end;

%initial SS values from case with gama=nu=1 and gamas=phis (fingers
%crossed)

%constant often used
k1=1-betta-theta*(1-betta/bettas);

initval;

%exogenous
W=0;
r=1/betta-1;


%consumption levels
Cb=-phi*log( 1 + r*chi*theta/k1 );
Cs=log(1/2) + log( r*theta*exp(Cb)/k1 + sqrt( (r*theta*chi*exp(Cb)/k1)^2 + 4*etas^-gamas) );  

%house price
Q=log( exp(Cs) + theta*chi*exp(Cb)/k1 ) - log(1-bettas) - log(Hbar);

%labor supply
Lb=-1/phi*log(eta) -1/phi*Cb;
Ls=1/phis*(-log(eta) -gama*Cs);

%housing demand
Hb=Cb-Q+log(chi/k1);
Hs=log(Hbar-exp(Hb));

%debt
B=theta*exp(Q+Hb)*(1+r);

%multiplier:
lambda = (1-betta*(1+r))*exp(-gama*Cb);

end;


shocks;
var epsw; stderr 1;
end;

stoch_simul(order=1,irf=100,nocorr,ar=0);
%stoch_simul(order=1,irf=100,nomoments,nograph);

%save SS for learning solution
ys_RE=oo_.dr.ys;
M_RE=M_;
oo_RE=oo_;
save twoagents_SS ys_RE M_RE oo_RE;
cleanup_dyn(M_.fname);
