function [residual, g1, g2] = twoagents_learn_static(y, x, params)
%
% Status : Computes static model for Dynare
%
% Inputs : 
%   y         [M_.endo_nbr by 1] double    vector of endogenous variables in declaration order
%   x         [M_.exo_nbr by 1] double     vector of exogenous variables in declaration order
%   params    [M_.param_nbr by 1] double   vector of parameter values in declaration order
%
% Outputs:
%   residual  [M_.endo_nbr by 1] double    vector of residuals of the static model equations 
%                                          in order of declaration of the equations
%   g1        [M_.endo_nbr by M_.endo_nbr] double    Jacobian matrix of the static model equations;
%                                                    columns: equations in order of declaration
%                                                    rows: variables in declaration order
%   g2        [M_.endo_nbr by (M_.endo_nbr)^2] double   Hessian matrix of the static model equations;
%                                                       columns: equations in order of declaration
%                                                       rows: variables in declaration order
%
%
% Warning : this file is generated automatically by Dynare
%           from model file (.mod)

residual = zeros( 14, 1);

%
% Model equations
%

lhs =exp(y(5));
rhs =exp(y(8)+y(7))-y(4)+y(4)/(1+y(10));
residual(1)= lhs-rhs;
lhs =y(4)*(1+y(10));
rhs =params(6)*exp(y(9)+y(6));
residual(2)= lhs-rhs;
lhs =log(params(10))+y(7)*params(11)+y(5)*params(8);
rhs =y(8);
residual(3)= lhs-rhs;
lhs =1-y(11);
rhs =(1+y(10))*params(7);
residual(4)= lhs-rhs;
lhs =exp(y(9)-y(5)*params(8))*(1-params(6)*y(11));
rhs =params(12)*exp(y(6)*(-params(9)))+params(7)*exp(y(9)+y(5)*(-params(8)));
residual(5)= lhs-rhs;
lhs =exp(y(1));
rhs =y(4)+exp(y(8)+y(3))-y(4)/(1+y(10));
residual(6)= lhs-rhs;
lhs =log(params(4))+y(3)*params(5)+y(1)*params(2);
rhs =y(8);
residual(7)= lhs-rhs;
lhs =1;
rhs =(1+y(10))*params(1);
residual(8)= lhs-rhs;
lhs =exp(y(9)-y(1)*params(2));
rhs =exp(y(2)*(-params(3)))+params(1)*exp(y(9)+y(1)*(-params(2)));
residual(9)= lhs-rhs;
lhs =y(9);
rhs =y(9)+y(12)+params(17)*x(2)-0.5*(params(17)*y(14))^2;
residual(10)= lhs-rhs;
lhs =y(12);
rhs =y(12)+params(16)*(params(17)*x(3)-0.5*(params(17)*y(14))^2);
residual(11)= lhs-rhs;
lhs =y(14);
rhs =x(2);
residual(12)= lhs-rhs;
lhs =y(13);
rhs =y(14);
residual(13)= lhs-rhs;
lhs =y(8);
rhs =y(8)*params(14)+params(13)*x(1);
residual(14)= lhs-rhs;
if ~isreal(residual)
  residual = real(residual)+imag(residual).^2;
end
if nargout >= 2,
  g1 = zeros(14, 14);

  %
  % Jacobian matrix
  %

  g1(1,4)=(-((-1)+1/(1+y(10))));
  g1(1,5)=exp(y(5));
  g1(1,7)=(-exp(y(8)+y(7)));
  g1(1,8)=(-exp(y(8)+y(7)));
  g1(1,10)=(-((-y(4))/((1+y(10))*(1+y(10)))));
  g1(2,4)=1+y(10);
  g1(2,6)=(-(params(6)*exp(y(9)+y(6))));
  g1(2,9)=(-(params(6)*exp(y(9)+y(6))));
  g1(2,10)=y(4);
  g1(3,5)=params(8);
  g1(3,7)=params(11);
  g1(3,8)=(-1);
  g1(4,10)=(-params(7));
  g1(4,11)=(-1);
  g1(5,5)=(1-params(6)*y(11))*exp(y(9)-y(5)*params(8))*(-params(8))-params(7)*(-params(8))*exp(y(9)+y(5)*(-params(8)));
  g1(5,6)=(-(params(12)*(-params(9))*exp(y(6)*(-params(9)))));
  g1(5,9)=exp(y(9)-y(5)*params(8))*(1-params(6)*y(11))-params(7)*exp(y(9)+y(5)*(-params(8)));
  g1(5,11)=exp(y(9)-y(5)*params(8))*(-params(6));
  g1(6,1)=exp(y(1));
  g1(6,3)=(-exp(y(8)+y(3)));
  g1(6,4)=(-(1-1/(1+y(10))));
  g1(6,8)=(-exp(y(8)+y(3)));
  g1(6,10)=(-y(4))/((1+y(10))*(1+y(10)));
  g1(7,1)=params(2);
  g1(7,3)=params(5);
  g1(7,8)=(-1);
  g1(8,10)=(-params(1));
  g1(9,1)=exp(y(9)-y(1)*params(2))*(-params(2))-params(1)*(-params(2))*exp(y(9)+y(1)*(-params(2)));
  g1(9,2)=(-((-params(3))*exp(y(2)*(-params(3)))));
  g1(9,9)=exp(y(9)-y(1)*params(2))-params(1)*exp(y(9)+y(1)*(-params(2)));
  g1(10,12)=(-1);
  g1(10,14)=0.5*params(17)*2*params(17)*y(14);
  g1(11,14)=(-(params(16)*(-(0.5*params(17)*2*params(17)*y(14)))));
  g1(12,14)=1;
  g1(13,13)=1;
  g1(13,14)=(-1);
  g1(14,8)=1-params(14);
  if ~isreal(g1)
    g1 = real(g1)+2*imag(g1);
  end
end
if nargout >= 3,
  %
  % Hessian matrix
  %

  g2 = sparse([],[],[],14,196);
end
end
