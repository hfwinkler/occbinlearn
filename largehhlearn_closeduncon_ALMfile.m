function [ zidx, dphi ] = largehhlearn_closeduncon_ALMfile( dr,M,options )
% this function provides the local derivatives of the equilibrium conditions
% which determine the boundedly rational forecast errors
%
% this function needs to be adapted by hand for every .mod file
% this function needs to be called FNAME_ALMfile.m
%
%


%fetch parameters
for i=1:length(M.params)
    eval([deblank(M.param_names(i,:)) '=M.params(' num2str(i) ');']);
end

%get indices of endovars and exovars
vari=struct;
varj=struct;
varjx=struct;
for i=1:M.exo_nbr
    vari.(deblank(M.exo_names(i,:)))=i;
end
for j=1:M.endo_nbr
    varj.(deblank(M.endo_names(dr.order_var(j),:)))=j;
end
j0=length(dr.state_var)-size(dr.ghx,2); %with order 3, we have to take the last nx variables in state_var!
for j=1:size(dr.ghx,2)
    varjx.(deblank(M.endo_names(dr.state_var(j+j0),:)))=j;
end

%set indices of pseudo forecast errors
zidx=[vari.v, vari.v1];

%derivatives
dphi=struct;

n=M.endo_nbr;
nx=size(dr.ghx,2);
nz=length(zidx);
nuz=M.exo_nbr;


%% first order

dphi.y1=zeros(nz,n);
dphi.y=zeros(nz,n);
dphi.x=zeros(nz,nx);
dphi.uz=zeros(nz,nuz);

dphi.y(1,varj.H)=1;    

dphi.y(2,varj.lagv)=1;
dphi.uz(2,vari.v1)=-1;

%% second order
if options.order==1; return; end

dphi.y1y1=zeros(nz,n,n);
dphi.y1y=zeros(nz,n,n);
dphi.y1x=zeros(nz,n,nx);
dphi.y1uz=zeros(nz,n,nuz);

dphi.yy=zeros(nz,n,n);
dphi.yx=zeros(nz,n,nx);
dphi.yuz=zeros(nz,n,nuz);

dphi.xx=zeros(nz,nx,nx);
dphi.xuz=zeros(nz,nx,nuz);

dphi.uzuz=zeros(nz,nuz,nuz);

%% third order
if options.order==2; return; end

dphi.y1y1y1=zeros(nz,n,n,n);
dphi.y1y1y=zeros(nz,n,n,n);
dphi.y1y1x=zeros(nz,n,n,nx);
dphi.y1y1uz=zeros(nz,n,n,nuz);
dphi.y1yy=zeros(nz,n,n,n);
dphi.y1yx=zeros(nz,n,n,nx);
dphi.y1yuz=zeros(nz,n,n,nuz);
dphi.y1xx=zeros(nz,n,nx,nx);
dphi.y1xuz=zeros(nz,n,nx,nuz);
dphi.y1uzuz=zeros(nz,n,nuz,nuz);

dphi.yyy=zeros(nz,n,n,n);
dphi.yyx=zeros(nz,n,n,nx);
dphi.yyuz=zeros(nz,n,n,nuz);
dphi.yxx=zeros(nz,n,nx,nx);
dphi.yxuz=zeros(nz,n,nx,nuz);
dphi.yuzuz=zeros(nz,n,nuz,nuz);

dphi.xxx=zeros(nz,nx,nx,nx);
dphi.xxuz=zeros(nz,nx,nx,nuz);
dphi.xuzuz=zeros(nz,nx,nuz,nuz);

dphi.uzuzuz=zeros(nz,nuz,nuz,nuz);

end

