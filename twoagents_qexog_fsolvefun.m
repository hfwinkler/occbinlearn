function [funval, funjac, Hs, Hb]=twoagents_qexog_fsolvefun(args,states,params,constrained)

persistent gama gamas betta bettas nu nus eta etas phi phis theta chi minC;

%unpack parameters if first call
if isempty(theta)
    gama=params.gama;
    gamas=params.gamas;
    betta=params.betta;
    bettas=params.bettas;
    nu=params.nu;
    nus=params.nus;
    eta=params.eta;
    etas=params.etas;
    phi=params.phi;
    phis=params.phis;
    theta=params.theta;
    chi=params.chi;
    minC=params.minC;
end

%unpack states
Hbminus=states.Hbminus;
Hsminus=states.Hsminus;
Bminus=states.Bminus;
Q=states.Q;
k=states.k;

%unpack arguments
Cs=exp(args(1));
Cb=exp(args(2));

w=0;
Yb= exp(w)*(exp(w)/eta*Cb^-gama)^(1/phi);
Ys= exp(w)*(exp(w)/etas*Cs^-gamas)^(1/phis);
dYb= exp(w)*(exp(w)/eta)^(1/phi)*Cb^(-gama/phi-1)*(-gama/phi);
dYs= exp(w)*(exp(w)/etas)^(1/phis)*Cs^(-gamas/phis-1)*(-gamas/phis);

Hsterm=Q*Cs^-gamas - bettas*k(2);
if Hsterm>minC
    Hs = Hsterm^(-1/nus);
    dHs = 1/nus*Hsterm^(-1/nus-1)*Q*Cs^(-gamas-1)*gamas;
else
    Hs=minC^(-1/nus);
    dHs=0;
end

funval=nan(2,1);
funjac=nan(2,2);

if constrained
    Hb = ( Yb-Cb + Q*Hbminus - Bminus )/(1-theta)/Q;
    dHb = (dYb-1)/(1-theta)/Q;
    
    funval(1) = Cs + Cb - Ys - Yb - Q*( Hsminus + Hbminus - Hs - Hb ); 
    funjac(1,1) = Cs * ( 1 - dYs + Q*dHs );
    funjac(1,2) = Cb * ( 1 - dYb + Q*dHb );
    
    funval(2) = betta/bettas*Cb^gama*Cs^-gamas*k(3)/k(1) - 1 ...
        + 1/theta*( 1 - (chi*Hb^-nu + betta*k(4))/Q*Cb^gama );
    funjac(2,1)= Cs*( -gamas*betta/bettas*Cb^gama*Cs^(-gamas-1)*k(3)/k(1) );
    funjac(2,2)= Cb*( gama*betta/bettas*Cb^(gama-1)*Cs^-gamas*k(3)/k(1) ...
        - 1/theta*( gama*(chi*Hb^-nu + betta*k(4))/Q*Cb^(gama-1)  -nu*chi*Hb^(-nu-1)/Q*Cb^gama*dHb ) );

else
    Hbterm=Q*Cb^-gama - betta*k(4);
    if Hbterm>minC
        Hb = Hbterm^(-1/nu);
        dHb = 1/nu*Hbterm^(-1/nu-1)*Q*Cb^(-gama-1)*gama;
    else
        Hb=minC^(-1/nu);
        dHb=0;
    end
    funval(1) = Cs + Cb - Ys - Yb - Q*( Hsminus + Hbminus - Hs - Hb ); 
    funjac(1,1) = Cs * ( 1 - dYs + Q*dHs );
    funjac(1,2) = Cb * ( 1 - dYb + Q*dHb );
    
    funval(2) = bettas*Cs^gamas*k(1) - betta*Cb^gama*k(3);
    funjac(2,1) = Cs*( gamas*bettas*Cs^(gamas-1)*k(1) );
    funjac(2,2) = Cb*( -gama*betta*Cb^(gama-1)*k(3) );
end
     
end