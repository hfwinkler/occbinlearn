% solve CCHH model with global projection method
% learning version with work

clear;
%% parameters

%model parameters
betta=1.035^-0.25;
R=1.03^0.25;
theta=0.5;

Ybar=1;
rhow=0.9;
sigw=1e-2;

Hbar=1;

g=0.002;
sigq=0.01;
rhomu=0.9;

%algorithm parameters
minC=1e-3;
minH=1e-3;
convcrit=1e-4;
lambda=0;
plotfreq=5;
loadlast=0;

%% non-stochastic steady-state and output function

Css=1/(1-theta*(1-R)/(1-(1-betta*R)*theta-betta))*Ybar;
Qss=1/(1-(1-betta*R)*theta-betta)*Css;
Bss=theta*Qss;
Wss=Qss*Hbar-R*Bss;
wbar=log(sqrt(Ybar*Css));

%income Y=w*L as a function of w (wage) and X=B-R*Bminus+Q*(Hminus-H) (financial cash flow)
Yfun=@(X,w) 2*w.^2./(X+sqrt(X.^2+4*w.^2));

%% set up grid and nodes

%income grid
wmin=wbar-3*sigw/sqrt(1-rhow^2);
wmax=wbar+3*sigw/sqrt(1-rhow^2);
Kw=5;
grid.onlyw=linspace(wmin,wmax,Kw);

%price grid
qmin=log(Qss*0.001);
qmax=log(Qss*10);
Kq=41;
grid.onlyq=linspace(qmin,qmax,Kq);

%belief grid
mumin=-.005;
mumax=.005;
Kmu=11;
grid.onlymu=linspace(mumin,mumax,Kmu);

%debt grid (for PLM we use wealth, but for ALM we use debt as the state)
Bmin=Bss*0.5;
Bmax=Bss*1.5;
KB=31;
grid.onlyB=linspace(Bmin,Bmax,KB);

%wealth grid set to encompass debt grid with H=Hbar
Wmin=exp(qmin)*Hbar-R*Bmax-1*Wss;
Wmax=exp(qmax)*Hbar-R*Bmin+1*Wss;
KW=KB;
logWtoW=@(logW) Wmin-1*Wss+exp(logW);
WtologW=@(W) log(W-Wmin+1*Bss);
grid.onlyW=logWtoW(linspace(WtologW(Wmin),WtologW(Wmax),KW));

%overall grid
[grid.w, grid.q, grid.mu, grid.W]=ndgrid(grid.onlyw,grid.onlyq,grid.onlymu,grid.onlyW);
grid.length=Kw*Kq*Kmu*KW;
fprintf('Nodes: %i\n',grid.length);

%number of quadrature nodes
J=5;
%calculate Gauss-Hermite nodes and weights
CM          = diag(sqrt((1:(J-1))/2),1) + diag(sqrt((1:(J-1))/2),-1);
[V, L]      = eig(CM);
[zeta, ind] = sort(diag(L));
V           = V(:,ind)';
weight      = V(:,1).^2; %don't multiply by sqrt(pi) here

%for two shocks
[zzeta1, zzeta2]=ndgrid(zeta,zeta);
ww=weight*weight';

%for three shocks
[zzzeta1, zzzeta2, zzzeta3]=ndgrid(zeta,zeta,zeta);
www=reshape(kron(weight,ww),[J J J]);

%% initial guess for PLM

%wild guess:
Hold=Hbar*ones(size(grid.W)); 
Cold=Yfun((1-R)*(exp(grid.q)*Hbar-grid.W),grid.w)+(1-R)*(exp(grid.q)*Hbar-grid.W);

%or load from file:
if loadlast
    load PLMsave;
    Hold=exp(interpn(gridold.w,gridold.q,gridold.mu,gridold.W,log(Hold),grid.w,grid.q,grid.mu,grid.W,'spline'));
    Cold=exp(interpn(gridold.w,gridold.q,gridold.mu,gridold.W,log(Cold),grid.w,grid.q,grid.mu,grid.W,'spline'));
end
gridold=grid;

Cnew=Cold;
Hnew=Hold;

Bnew=ones(size(grid.q));
Xnew=ones(size(grid.q));
Ynew=ones(size(grid.q));
troubleshooter=zeros(size(grid.q));

%% iterate

maxiter=1e4;
dist=Inf;
h=figure;
for n=1:maxiter
    
    %plot policy function
    if mod(n,plotfreq)==0
        fprintf('#%i: distance: %3.3g\n',n,dist);
        subplot(1,2,1);
        surf(grid.onlyq,grid.onlymu,squeeze(log(Hold(round(Kw/2),:,:,round(KW/2))))'); 
        xlabel('q'); xlim([qmin qmax]);
        ylabel('\mu'); ylim([mumin mumax]);
        zlabel('log(H)');
        subplot(1,2,2);
        surf(grid.onlyw,grid.onlyW,squeeze(log(Hold(:,round(Kq/2),round(Kmu/2),:)))'); 
        xlabel('w'); xlim([wmin wmax]);
        ylabel('W'); ylim([Wmin Wmax]);
        zlabel('log(H)');
        drawnow;
    end
    
    %walk through grid
    Fc=griddedInterpolant(grid.w,grid.q,grid.mu,grid.W,log(Cold),'linear','linear');
    parfor node=1:grid.length %can be replaced by parfor if grid is large
        %get grid points
        Q=exp(grid.q(node));
        mu=grid.mu(node);
        W=grid.W(node);
        w=grid.w(node);
        %current period:
        Ctemp=Cold(node); 
        Ytemp=exp(w)^2/Ctemp;
        Htemp=Hold(node);
        Btemp=min(theta*Q*Htemp, Ctemp-Ytemp-W+Q*Htemp);
        %next period:
        w1=(1-rhow)*wbar+rhow*w+sigw*zzzeta1;
        q1=log(Q)+rhomu*mu+sigq*zzzeta2;
        mu1=rhomu*mu+g*sigq*zzzeta3;
        W1=exp(q1)*Htemp-R*Btemp;
        C1=exp(Fc(w1,q1,mu1,W1)); %#ok<*PFBNS>
        C1=max(C1,minC); %if future consumption negative, set MU very high
        %C1=min(C1,1/minC); %to prevent divergence
        %take expectation with quadrature
        integrand1=1./C1;
        k1=integrand1(:)'*www(:);
        integrand2=exp(q1)./C1;
        k2=integrand2(:)'*www(:);
        %get new solution: try unconstrained first
        C=max(1/(betta*R*k1),minC);
        Y=exp(w)^2/C;
        H=1/(Q/C-betta*k2);
        B=C-Y-W+Q*H; %%% Check this.
        if B>theta*Q*H || H<0 
            if W>0
                %solve for H, C, but not Y - faster but doesn't converge for low W
                Y=exp(w)^2/Ctemp;
                k3=Y+W;
                k4=theta*betta*R*k1-betta/Q*k2;
                C=1/(2*k4)*(k3*k4-2*(1-theta)+sqrt(k3^2*k4^2+4*(1-theta)^2));
            else     
                %solve for everything - it's a cubic equation in C
                k3=exp(w)^2;
                k4=theta/(1-theta)*betta*R*k1-betta*k2/Q;
                Croots=roots([k4,2-k4*W,-k3*k4-W,-k3]);
                C=Croots(3); %take largest one, it will be positive
                Y=exp(w)^2/C;
                C=max(C,minC);
            end
        
            H=(Y-C+W)/Q/(1-theta);            
            B=theta*Q*H;
            troubleshooter(node)=1;
            if H<minH
                H=minH;
                C=Y+W-(1-theta)*Q*H;
                troubleshooter(node)=2;
            end
            Y=exp(w)^2/C;
            
        else
            troubleshooter(node)=0;
        end
        if ~isreal(C)
            error('Something wrong with C');
        end
        if isnan(H) || isinf(H)
            error('Something wrong with H');
        end
        Cnew(node)=max(C,minC);
        Hnew(node)=max(H,minH);
        Bnew(node)=B;
        Xnew(node)=B/(theta*Q*H);
        Ynew(node)=Y;
    end
    
    %measure distance to old policy function and stop if it's small
    dist=max(abs([Hnew(:); Cnew(:)]-[Hold(:); Cold(:)]));
    if dist<convcrit
        fprintf('#%i: distance: %3.3g\n',n,dist);
        break; 
    end
    %update policy function with attenuation
    Cold=(1-lambda)*Cnew+lambda*Cold;
    Hold=(1-lambda)*Hnew+lambda*Hold;
    save PLMsave Cold Hold gridold;
    
end
close(h);



%% plot PLM

figure; 
subplot(1,2,1);
surf(grid.onlyq,grid.onlymu,squeeze(log(Hnew(round(Kw/2),:,:,round(KW/2))))'); 
xlabel('q'); xlim([qmin qmax]);
ylabel('\mu'); ylim([mumin mumax]);
zlabel('log(H)');
subplot(1,2,2);
surf(grid.onlyw,grid.onlyW,squeeze(log(Hnew(:,round(Kq/2),round(Kmu/2),:)))'); 
xlabel('w'); xlim([wmin wmax]);
ylabel('W'); ylim([Wmin Wmax]);
zlabel('log(H)');

figure; 
subplot(1,2,1);
surf(grid.onlyq,grid.onlymu,squeeze(log(Cnew(round(Kw/2),:,:,round(KW/2))))'); 
xlabel('q'); xlim([qmin qmax]);
ylabel('\mu'); ylim([mumin mumax]);
zlabel('log(C)');
subplot(1,2,2);
surf(grid.onlyw,grid.onlyW,squeeze(log(Cnew(:,round(Kq/2),round(Kmu/2),:)))'); 
xlabel('w'); xlim([wmin wmax]);
ylabel('W'); ylim([Wmin Wmax]);
zlabel('log(C)');

figure; 
subplot(1,2,1);
surf(grid.onlyq,grid.onlymu,squeeze(Xnew(round(Kw/2),:,:,round(KW/2)))'); 
xlabel('q'); xlim([qmin qmax]);
ylabel('\mu'); ylim([mumin mumax]);
zlabel('X');
title('debt as percent of constraint (PLM)');
subplot(1,2,2);
surf(grid.onlyw,grid.onlyW,squeeze(Xnew(:,round(Kq/2),round(Kmu/2),:))'); 
xlabel('w'); xlim([wmin wmax]);
ylabel('W'); ylim([Wmin Wmax]);
zlabel('X');
title('debt as percent of constraint (PLM)');




%% simulate PLM (just checking...) 

T=1e5;
Tpre=0;
wsim=zeros(1,T);
Qsim=zeros(1,T);
musim=zeros(1,T);
Xsim=zeros(1,T);
Wsim=zeros(1,T);
Bsim=zeros(1,T);
Hsim=zeros(1,T);
Csim=zeros(1,T);
Ysim=zeros(1,T);
wsim=zeros(1,T);

%THIS IS IMPORTANT - not logging the policy functions is highly inaccurate,
%as is linear interpolation for B if used
Fc=griddedInterpolant(grid.w,grid.q,grid.mu,grid.W,log(Cnew),'spline','linear');
Fh=griddedInterpolant(grid.w,grid.q,grid.mu,grid.W,log(Hnew),'spline','linear'); 
FB=griddedInterpolant(grid.w,grid.q,grid.mu,grid.W,Bnew,'spline','linear');

wlag=wbar;
Blag=0*Bss;
Hlag=Hbar;
mulag=0;
qlag=log(1*Qss);
    
%rng(0);

for t=[ones(1,Tpre) 1:T]
    wsim(t)=(1-rhow)*wbar+rhow*wlag+sigw*randn(1);
    Qsim(t)=exp(qlag+rhomu*mulag+sigq*randn(1));
    musim(t)=rhomu*mulag+g*sigq*randn(1);
    Wsim(t)=Qsim(t)*Hlag-R*Blag;
    Hsim(t)=exp(Fh(wsim(t),log(Qsim(t)),musim(t),Wsim(t)));
   %use C policy function
%    Csim(t)=exp(Fc(wsim(t),log(Qsim(t)),musim(t),Wsim(t)));
%    Ysim(t)=exp(wsim(t))^2/Csim(t);
%    Bsim(t)=Csim(t)-Ysim(t)+Qsim(t)*Hsim(t)-Wsim(t);
    %use B policy function - seems less accurate
    Bsim(t)=min(FB(wsim(t),log(Qsim(t)),musim(t),Wsim(t)), theta*Qsim(t)*Hsim(t));
    Ysim(t)=Yfun(Bsim(t)+Wsim(t)-Qsim(t)*Hsim(t),wsim(t));
    Csim(t)=Ysim(t)+Bsim(t)+Wsim(t)-Qsim(t)*Hsim(t);
    Xsim(t)=Bsim(t)/(theta*Qsim(t)*Hsim(t));
    Blag=Bsim(t);
    Hlag=Hsim(t);
    wlag=wsim(t);
    mulag=musim(t);
    qlag=log(Qsim(t));
end
disp(mean(diff(log(Csim))));

keep = 3000:3500;
T    = length(keep);

figure;
subplot(2,4,1); plot(1:T,exp(wsim(keep)-wbar)); title('wage, fraction of SS');
axis tight
subplot(2,4,2); plot(1:T,Qsim(keep)/Qss); title('house price, fraction of SS');
axis tight
subplot(2,4,3); plot(1:T,Bsim(keep)/Bss); title('debt, fraction of SS');
axis tight
subplot(2,4,4); plot(1:T,Xsim(keep)); title('debt as percent of constraint');
axis tight
subplot(2,4,5); plot(1:T,musim(keep)); title('belief');
axis tight
subplot(2,4,6); plot(1:T,Csim(keep)/Css); title('consumption, fraction of SS');
axis tight
subplot(2,4,7); plot(1:T,Hsim(keep)/Hbar); title('housing, fraction of SS');
axis tight
subplot(2,4,8); plot(1:T,Ysim(keep)/Ybar); title('income, fraction of SS');
axis tight
%% solve for ALM

%store PLM
CPLM=Cnew;
HPLM=Hnew;
BPLM=Bnew;
Hbar=0.1;
%set up grid:

%switch to a debt grid, because W is endogenous now
gridALM.onlyw=grid.onlyw;
gridALM.onlymu=grid.onlymu;
gridALM.onlyB=grid.onlyB;
[gridALM.w, gridALM.mu, gridALM.B]=ndgrid(gridALM.onlyw,gridALM.onlymu,gridALM.onlyB);
gridALM.length=Kw*Kmu*KB;

qALM=zeros(size(gridALM.mu));
CALM=zeros(size(gridALM.mu));
BALM=zeros(size(gridALM.mu));
XALM=zeros(size(gridALM.mu));
diffCALM=zeros(size(gridALM.mu));

Fc=griddedInterpolant(grid.w,grid.q,grid.mu,grid.W,log(CPLM),'spline','linear');
Fh=griddedInterpolant(grid.w,grid.q,grid.mu,grid.W,log(HPLM),'spline','linear');
%FB=griddedInterpolant(grid.w,grid.q,grid.mu,grid.W,BPLM,'spline','linear');

troubleshooter=zeros(size(gridALM.mu));
options=optimset('Display','off');
for node=1:gridALM.length
    w=gridALM.w(node);
    mu=gridALM.mu(node);
    Bminus=gridALM.B(node);
    fun=@(q) exp(Fh(w,q,mu,exp(q)*Hbar-R*Bminus))-Hbar;
    tryfuns=arrayfun(fun,grid.onlyq);
    signchangeidx=find(diff(sign(tryfuns)),1,'last');
    if isempty(signchangeidx)
        %q0=grid.onlyq(abs(tryfuns)==min(abs(tryfuns)));
        q0=qmin;
        troubleshooter(node)=sign(tryfuns(1));    
        [q,fval,exitflag]=fsolve(fun,q0,optimset('Display','off'));   
        if exitflag<0; troubleshooter(node)=2*sign(tryfuns(1)); end
        q=max(min(q,qmax),qmin);
    else
        q0=grid.onlyq(signchangeidx);
        troubleshooter(node)=0;
        [q,fval,exitflag]=fzero(fun,grid.onlyq(signchangeidx+[0 1]));
    end

    if exitflag<0
        %q=qmin;
        q=fsolve(fun,grid.onlyq(abs(tryfuns)==min(abs(tryfuns))),optimset('Display','off'));
        troubleshooter(node)=-1;
%         plot(grid.onlyq,tryfuns,grid.onlyq,zeros(size(grid.onlyq)));
%         ylim([-1 1]);
%         vline(q);
%         keyboard;
    end
    Q=exp(q);
    %use C policy function
%    C=exp(Fc(w,log(Q),mu,Q*Hbar-R*Bminus));
%    Y=exp(w)^2/C;
%    B=C-Y+R*Bminus;
    %use B policy function - seems less accurate
    B=min(FB(w,log(Q),mu,Q*Hbar-R*Bminus),theta*Q*Hbar);
    Y=Yfun(B-R*Bminus,wbar);
    C=Y+B-R*Bminus;
    diffC = C - exp(Fc(w,log(Q),mu,Q*Hbar-R*Bminus));
%     if B>(theta*Q*Hbar)
%         B=theta*Q*Hbar;
%         Y=Yfun(B-R*Bminus,w);
%         C=Y+B-R*Bminus;
%     end
    qALM(node)=log(Q);
    CALM(node)=max(C,minC);
    BALM(node)=B;
    XALM(node)=B/(theta*Q*Hbar);
    diffCALM(node) = diffC;
end

%display outcomes
figure;
hist(troubleshooter(:));

%% plot ALM

figure; 
subplot(1,2,1);
surf(gridALM.onlyw,gridALM.onlyB/Bss,squeeze(qALM(:,round(Kmu/2),:))'); 
xlabel('w'); xlim([wmin wmax]);
ylabel('B/Bss'); ylim([Bmin/Bss Bmax/Bss]);
zlabel('log(Q)');
subplot(1,2,2);
surf(gridALM.onlymu,gridALM.onlyB/Bss,squeeze(qALM(round(Kw/2),:,:))'); 
xlabel('\mu'); xlim([mumin mumax]);
ylabel('B/Bss'); ylim([Bmin/Bss Bmax/Bss]);
zlabel('log(Q)');

figure; 
subplot(1,2,1);
surf(gridALM.onlyw,gridALM.onlyB/Bss,squeeze(BALM(:,round(Kmu/2),:))'/Bss); 
xlabel('w'); xlim([wmin wmax]);
ylabel('B/Bss'); ylim([Bmin/Bss Bmax/Bss]);
zlabel('B/Bss');
subplot(1,2,2);
surf(gridALM.onlymu,gridALM.onlyB/Bss,squeeze(BALM(round(Kw/2),:,:))'/Bss); 
xlabel('\mu'); xlim([mumin mumax]);
ylabel('B/Bss'); ylim([Bmin/Bss Bmax/Bss]);
zlabel('B/Bss');

figure;
subplot(1,2,1);
surf(gridALM.onlyw,gridALM.onlyB/Bss,squeeze(XALM(:,round(Kmu/2),:))'); 
xlabel('w'); xlim([wmin wmax]);
ylabel('B/Bss'); ylim([Bmin/Bss Bmax/Bss]);
zlabel('X');
subplot(1,2,2);
surf(gridALM.onlymu,gridALM.onlyB/Bss,squeeze(XALM(round(Kw/2),:,:))'); 
xlabel('\mu'); xlim([mumin mumax]);
ylabel('B/Bss'); ylim([Bmin/Bss Bmax/Bss]);
zlabel('X');

figure;
subplot(1,2,1);
surf(gridALM.onlyw,gridALM.onlyB/Bss,squeeze(log(CALM(:,round(Kmu/2),:)))'); 
xlabel('w'); xlim([wmin wmax]);
ylabel('B/Bss'); ylim([Bmin/Bss Bmax/Bss]);
zlabel('log(C)');
subplot(1,2,2);
surf(gridALM.onlymu,gridALM.onlyB/Bss,squeeze(log(CALM(round(Kw/2),:,:)))'); 
xlabel('\mu'); xlim([mumin mumax]);
ylabel('B/Bss'); ylim([Bmin/Bss Bmax/Bss]);
zlabel('log(C)');

figure;
subplot(1,2,1);
surf(gridALM.onlyw,gridALM.onlyB/Bss,squeeze(troubleshooter(:,round(Kmu/2),:))'); 
xlabel('w'); xlim([wmin wmax]);
ylabel('B/Bss'); ylim([Bmin/Bss Bmax/Bss]);
zlabel('trouble');
subplot(1,2,2);
surf(gridALM.onlymu,gridALM.onlyB/Bss,squeeze(troubleshooter(round(Kw/2),:,:))'); 
xlabel('\mu'); xlim([mumin mumax]);
ylabel('B/Bss'); ylim([Bmin/Bss Bmax/Bss]);
zlabel('trouble');

%% simulate ALM

T=200;
Tpre=1e3;
wsim=zeros(1,T);
Qsim=zeros(1,T);
musim=zeros(1,T);
Xsim=zeros(1,T);
Wsim=zeros(1,T);
Bsim=zeros(1,T);
Csim=zeros(1,T);
Ysim=zeros(1,T);

Fc=griddedInterpolant(grid.w,grid.q,grid.mu,grid.W,log(CPLM),'spline','linear');
Fh=griddedInterpolant(grid.w,grid.q,grid.mu,grid.W,log(HPLM),'spline','linear');
FB=griddedInterpolant(grid.w,grid.q,grid.mu,grid.W,BPLM,'spline','linear');

wlag=wbar;
Blag=0.1*Bss;
mulag=0;
qlag=interpn(gridALM.w,gridALM.mu,gridALM.B,qALM,wlag,mulag,Blag);


rng(0);

for t=[ones(1,Tpre) 1:T]
    %solve for the price    
    wsim(t)=(1-rhow)*wbar+rhow*wlag+sigw*randn(1)*(t>1);
    fun=@(q) exp(Fh(wsim(t),q,mulag,exp(q)*Hbar-R*Blag))-Hbar;
    tryfuns=arrayfun(fun,grid.onlyq);
    signchangeidx=find(diff(sign(tryfuns)),1,'last');
    if isempty(signchangeidx)
        %q0=grid.onlyq(abs(tryfuns)==min(abs(tryfuns)));
        q0=qmin;
        troubleshooter(node)=0.5;    
        [q,fval,exitflag]=fsolve(fun,q0,optimset('Display','off'));        
    else
        q0=grid.onlyq(signchangeidx);
        troubleshooter(node)=0;
        [q,fval,exitflag]=fzero(fun,grid.onlyq(signchangeidx+[0 1]));
    end

    if exitflag<0
        %q=qmin;
        q=fsolve(fun,grid.onlyq(abs(tryfuns)==min(abs(tryfuns))),optimset('Display','off'));
        troubleshooter(node)=-1;
%         plot(grid.onlyq,tryfuns,grid.onlyq,zeros(size(grid.onlyq)));
%         ylim([-1 1]);
%         vline(q);
%         keyboard;
    end
    q=max(q,log(0.8)+qlag);
    Qsim(t)=exp(q);
    Wsim(t)=Qsim(t)*Hbar-R*Blag;
    if Wsim(t)>Wmax || Wsim(t)<Wmin; warning AAAH; end
    %use C policy function
    Csim(t)=exp(Fc(wsim(t),q,mu,Wsim(t)));
    %Xsim(t)=Csim(t)/Hbar/Qsim(t)/(1-betta);
    Ysim(t)=exp(wsim(t))^2/Csim(t);
    Bsim(t)=Csim(t)-Ysim(t)+R*Blag;
%     if Bsim(t)>theta*Qsim(t)*Hbar
%         Bsim(t)=theta*Qsim(t)*Hbar;
%         Ysim(t)=Yfun(Bsim(t)-R*Blag,wsim(t));
%         Csim(t)=Ysim(t)+Bsim(t)-R*Blag;      
%     end
    %use B policy function
     Bsim(t)=FB(wsim(t),q, mu,Wsim(t));
     Bsim(t)=min(Bsim(t),theta*Qsim(t)*Hbar);
%     Bsim(t)=max(min(Bsim(t),Bmax),Bmin);
     Ysim(t)=Yfun(Bsim(t)-R*Blag,wsim(t));
     Csim(t)=Ysim(t)+Bsim(t)-R*Blag;

    musim(t)=(rhomu-g)*mulag+g*(q-qlag)*(t>1);
    musim(t)=max(min(musim(t),mumax),mumin);
    Xsim(t)=Bsim(t)/(theta*Qsim(t)*Hbar);
    
    wlag=wsim(t);
    Blag=max(min(Bsim(t),Bmax),Bmin);
    mulag=musim(t);
    qlag=q;
end

figure;
subplot(2,4,1); plot(1:T,exp(wsim-wbar)); title('wage, fraction of SS');
subplot(2,4,2); plot(1:T,Qsim/Qss); title('house price, fraction of SS');
subplot(2,4,3); plot(1:T,Bsim/Bss); title('debt, fraction of SS');
subplot(2,4,4); plot(1:T,Xsim); title('debt, fraction of debt limit');
subplot(2,4,5); plot(1:T,musim); title('belief');
subplot(2,4,6); plot(1:T,Csim/Css); title('consumption, fraction of SS');
subplot(2,4,7); plot(1:T,Wsim/Wss); title('wealth, fraction of SS');
subplot(2,4,8); plot(1:T,Ysim/Ybar); title('income, fraction of SS');
