clear all
close all



% Parameters
% Household
bet    =            .99; % consistent with 2% annual real rate
phi    =         1/0.25; % inverse of IES
nu     =              2; % 1/Frish
N      =              0; % Labor Scaling
chi    =            0.70; % fraction of borrowers

theta_grid            = 0.245;%0.05:0.005:0.95; % Borrowing Constraint (% of Assets)
dist2constr_grid      = NaN(length(theta_grid),3);
dist2constr_grid(:,1) = theta_grid;

% Housing Producers
psi_H   =             2; % IAC for housing
delta_H =         0.025; % Depreciation housing
H_ss    =             1;

% Goods Producers
rho_a  =            0.9; % TFP persistence
sig_a  =           0.009; % TFP stdev

psi_P  =             40; % Rotemberg AC - Not yet implemented
eta    =              7; % Demand Elasticity, so that (1+lambda) = 1.17

% Monetary Policy
phi_pi =            1.5; % TR inflation - Not yet implemented
phi_Y  =            0.5; % TR output    - Not yet implemented

ii_max = length(theta_grid);

% Compute the Steady State for the given parameters
for ii = 1:ii_max
    
    theta = theta_grid(ii);
    
    % Compute the Steady State in the unconstrained economy
    % Variables: r, Q, a_t, MC, W, H, omega, Pi, C, Y, lambda
    r             = 1/bet - 1;
    Q             = 0;
    a_t           = 0;
    MC            = (eta - 1)/eta;
    W             = log(MC * exp(a_t));
    
    C = log(0.5);
    diff = 10;
    while diff > 1e-7
        omega = exp(W)/(exp(C)^phi * exp(N)^nu);
        Pi = exp(a_t)* exp(N) * (1-MC);
        H  = log( (exp(W+N) + Pi - exp(C))/delta_H );
        C_jjp1  = log( ( exp(-H) / (1 - bet*(1-delta_H)) )^(-1/phi) );
        diff = abs(C_jjp1 - C);
        C = C_jjp1;
    end
    diff
    b           = exp(Q)*exp(H)*( 1 - chi*(1-delta_H) ) - chi*(exp(W)*exp(N)+Pi);
    dist2constr = b - theta*exp(Q)*exp(H);
    dH          = log(exp(H)*delta_H);
    Y           = log(exp(C) + exp(dH));
    lambda      = 0;
    SS_uncon    = [r, Q, a_t, MC, W, H, omega, Pi, C, Y, lambda, dist2constr];
    
    dist2constr_grid(ii,2) = dist2constr;
    
    if dist2constr > 0
        %Guess
        C             = log(0.5);
        lambda        = 1;
        %Solution
        r             = 1/(bet*(1+chi*lambda)) - 1;
        Q             = 0;
        a_t           = 0;
        MC            = (eta - 1)/eta;
        W             = log(MC * exp(a_t));
        diff = 10;
        while diff > 1e-7
            omega=exp(W)/exp(C)^phi*(1+chi*lambda)*exp(N)^nu;
            Pi = exp(a_t)* exp(N) * (1-MC);
            b  = theta*exp(Q)*exp(H);
            H = log(b + chi*(exp(W+N)+(1-delta_H)*exp(H)+Pi));
            C_jjp1      = log(exp(W+N) + Pi - delta_H*exp(H));
            %lambda_jjp1 =  (exp(-H)/exp(C_jjp1)^(-phi) - 1 + bet*(1-delta_H))/((1-theta) - bet*(1-delta_H)*chi); 
            lambda_jjp1      = (exp(-H) + bet*(1-delta_H)*exp(Q)*exp(-phi*C_jjp1)*(1+chi*lambda) - exp(Q)*exp(-phi*C_jjp1))/(exp(Q)*exp(-phi*C_jjp1)*(1-theta));
            diff = max(abs(C_jjp1 - C), abs(lambda - lambda_jjp1));
            C      = C_jjp1;
            lambda = lambda_jjp1;
        end
        diff
        dist2constr = b - theta*exp(Q)*exp(H);
        dH         = log(exp(H)*delta_H);
        Y          = log(exp(C) + exp(dH));
        SS_con   = [r, Q, a_t, MC, W, H, omega, Pi, C, Y, lambda, dist2constr];
        dist2constr_grid(ii,3) = dist2constr;
    end
end

% Steady-state state vector

H_ss = H;
a_ss = a_t;

% Productivity Shocks - Gauss-Hermite nodes

n_gh   = 13;
n_grid = 80;
n_min  = 0.90;
n_max  = 1.10;

[eps_a_t_gh, w]=hermquad(n_gh);

w = w/sqrt(pi);

a_grid = linspace(-3*sig_a/(1-rho_a), 3*sig_a/(1-rho_a), n_gh);

% Construct the approximation grid

if dist2constr < 0
    H_grid            = linspace(log(exp(H)*n_min), log(exp(H)*n_max), n_grid);
    [H_gr,a_gr]       = ndgrid(H_grid, a_grid);
    C_guess           = linspace(log(exp(C)*n_min), log(exp(C)*n_max), n_grid);
    [C_gr,a_gr]       = ndgrid(C_guess, a_grid);
    Q_guess           = linspace(log(exp(Q)*n_min), log(exp(Q)*n_max), n_grid);
    [Q_gr,a_gr]       = ndgrid(Q_guess, a_grid);
    lambda_guess      = max(0,linspace(lambda*n_min, lambda*n_max, n_grid));
    [lambda_gr,a_gr]  = ndgrid(lambda_guess, a_grid);
    dist2constr_guess      = linspace(dist2constr, dist2constr, n_grid);
    [dist2constr_gr,a_gr]  = ndgrid(dist2constr_guess, a_grid);
    V_gr                   = C_gr.^(1-phi)/(1-phi);
    b_grid              = linspace(b*n_min, b*n_max, n_grid);
    [b_gr,a_gr]         = ndgrid(b_grid, a_grid);
else
    H_grid            = linspace(log(exp(H)*n_min), log(exp(H)*n_max), n_grid);
    [H_gr,a_gr]       = ndgrid(H_grid, a_grid);
    C_guess           = linspace(log(exp(C)*n_min), log(exp(C)*n_max), n_grid);
    [C_gr,a_gr]       = ndgrid(C_guess, a_grid);
    Q_guess           = linspace(log(exp(Q)*n_min), log(exp(Q)*n_max), n_grid);
    [Q_gr,a_gr]       = ndgrid(Q_guess, a_grid);
    lambda_guess      = max(0,linspace(lambda*n_min, lambda*n_max, n_grid));
    [lambda_gr,a_gr]  = ndgrid(lambda_guess, a_grid);
    dist2constr_guess      = linspace(dist2constr, dist2constr, n_grid);
    [dist2constr_gr,a_gr]  = ndgrid(dist2constr_guess, a_grid);
    V_gr                   = C_gr.^(1-phi)/(1-phi);
    b_grid                 = linspace(b*n_min, b*n_max, n_grid);
    [b_gr,a_gr]            = ndgrid(b_grid, a_grid);
    b_gr_p1                = b_gr + 1e-3;
end


H_p1_gr             = H_gr;
Q_gr_p1             = Q_gr;
lambda_gr_p1        = NaN(size(C_gr));
dist2constr_gr_p1   = NaN(size(C_gr));
N_grid              = linspace(log(exp(N)*n_min), log(exp(N)*n_max), n_grid);
[N_gr,a_gr]         = ndgrid(N_grid, a_grid);
r_gr                = NaN(size(C_gr));
dH_gr               = NaN(size(C_gr));

U_gr                = 1/(1-bet)*(exp(C_gr).^(1-phi)/(1-phi) + omega*exp(N_gr).^(1+nu)/(1+nu));

disp('The model is solved around a Steady State with')

lambda_ss = lambda

dist2constr

pause

C_gr_p1 = C_gr;

diff = 10

load_sol_sw = 1;

if load_sol_sw == 1
    
    if lambda_ss == 0
        load solution_uncon
    else
        load solution
    end
    
    if n_grid > size(H_p1_gr,1) || n_gh > size(H_p1_gr,2)
    
        H_p1_gr   = padarray(H_p1_gr   ,[(n_grid - size(H_p1_gr,1))/2,   (n_gh - size(H_p1_gr,2))/2],'replicate','both');
        lambda_gr = padarray(lambda_gr ,[(n_grid - size(lambda_gr,1))/2, (n_gh - size(lambda_gr,2))/2],'replicate','both');
        C_gr      = padarray(C_gr      ,[(n_grid - size(C_gr,1))/2,      (n_gh - size(C_gr,2))/2],'replicate','both');
        Q_gr      = padarray(Q_gr      ,[(n_grid - size(Q_gr,1))/2,      (n_gh - size(Q_gr,2))/2],'replicate','both');
        b_gr      = padarray(b_gr      ,[(n_grid - size(b_gr,1))/2,      (n_gh - size(b_gr,2))/2],'replicate','both');
    
    else
        
        H_p1_gr   = H_p1_gr(1:n_grid, 1:n_gh);
        lambda_gr = lambda_gr(1:n_grid, 1:n_gh);
        C_gr      = C_gr(1:n_grid, 1:n_gh);
        Q_gr      = Q_gr(1:n_grid, 1:n_gh);
        b_gr      = b_gr(1:n_grid, 1:n_gh);
        
    end
        
end

while diff > 1e-8
    
    for ii = 1:n_grid
        for jj = 1:n_gh
            
            H           = H_gr(ii,jj);
            H_p1        = H_p1_gr(ii,jj);
            a_t         = a_gr(ii,jj);
            C           = C_gr(ii,jj);
            Q           = Q_gr(ii,jj);
            lambda      = lambda_gr(ii,jj);
            b           = b_gr(ii,jj);
            
            a_t_p1      = rho_a*a_t + sig_a*eps_a_t_gh;
            
            %dist2constr = dist2constr_gr(ii,jj);
            FC = griddedInterpolant(H_gr, a_gr, C_gr,'linear','linear');
            FQ = griddedInterpolant(H_gr, a_gr, Q_gr,'linear','linear');
            FL = griddedInterpolant(H_gr, a_gr, lambda_gr,'linear','linear');
            FN = griddedInterpolant(H_gr, a_gr, N_gr,'linear','linear');
            FU = griddedInterpolant(H_gr, a_gr, U_gr,'linear','linear');
            
            % Utility Function
            % U(H,a_t) = exp(C(H,a_t))^(1-phi)/(1-phi) + omega*exp(N(H,a_t))^(1+nu)/(1+nu))
            % Value Function
            % V = U(H,a_t) + bet*V'(H_p1,a_t_p1)s
            
            % Compute the solution under RE
            
            if b < (theta*exp(Q)*exp(H_p1))*0.99999
                
                lambda_ni   = 0;
                
                Exp1        = w'*(1./(exp(-phi*FC(H_p1*ones(n_gh,1), a_t_p1)).*(1+chi*max(0,FL(H_p1*ones(n_gh,1), a_t_p1)))));
                
                Exp2        = w'*(bet*(1-delta_H)*exp(FQ(H_p1*ones(n_gh,1), a_t_p1)).*exp(-phi*FC(H_p1*ones(n_gh,1), a_t_p1)).*(1+chi*max(0,FL(H_p1*ones(n_gh,1), a_t_p1))));
                
                C_ni        = log( max( (1/(exp(Q)*(1+(1-theta)*lambda_ni)) * ( exp(-H_p1) + Exp2 ))^(-1 /phi), 1e-9) );
                
                r           = 1/bet * exp(-phi*C_ni)*Exp1 - 1;
                
                MC          = (eta - 1)/eta;
                
                W           = log( MC * exp(a_t) );
                
                N           = log ( (exp(W)*exp(-phi*C_ni)*(1+chi*lambda)/omega)^(1/nu) );
                
                Y           = log(exp(a_t)*exp(N));
                
                %exp(Y) = exp(C) + dH + psi_H/2*(dH/exp(H) - delta_H)^2; % this solves for dH
                
                p           = [psi_H/(2*exp(H)^2) (1 - psi_H*delta_H/exp(H)) psi_H*delta_H^2/2 + exp(C_ni) - exp(Y)];
                
                %dH         = log(max(max(real(roots(p))),1e-9));
                
                dH          = max((-p(2) + sqrt(p(2)^2 - 4*p(1)*p(3)))/(2*p(1)),-(1-delta_H)*exp(H));
                
                H_p1        = log(dH+(1-delta_H)*exp(H));
                
                % a_t_p1      = rho_a*a_t + sig_a*eps_a_t_gh;
                
                Pi          = exp(a_t) * exp(N) * (1-MC) + exp(Q)*exp(H_p1) - dH-(1-delta_H)*exp(Q)*exp(H)-psi_H/2*(dH/exp(H)-delta_H)^2;
                
                b_ni        = max(0,exp(Q+H_p1) - (chi*(exp(W)*exp(N)+(1-delta_H)*exp(Q)*exp(H)+Pi)));
                
                dist2constr_ni = b_ni - theta*exp(Q)*exp(H_p1);
                
                Q_ni           = log(1+psi_H*(dH/exp(H)-delta_H));
                
%                 if dist2constr_ni > 0
%                     lambda_ni = max(0,1/(1-theta) * ( (exp(-H_p1) + Exp2 )/exp(Q-phi*C_ni) - 1));
%                 else
%                     lambda_ni   = 0;    
%                 end
                
            else
                
                Exp1        = w'*(1./(exp(-phi*FC(H_p1*ones(n_gh,1), a_t_p1)).*(1+chi*max(0,FL(H_p1*ones(n_gh,1), a_t_p1)))));
                
                Exp2        = w'*(bet*(1-delta_H)*exp(FQ(H_p1*ones(n_gh,1), a_t_p1)).*exp(-phi*FC(H_p1*ones(n_gh,1), a_t_p1)).*(1+chi*max(0,FL(H_p1*ones(n_gh,1), a_t_p1))));
                
                lambda_ni   = max(0,1/(1-theta) * ( (exp(-H_p1) + Exp2 )/exp(Q-phi*C) - 1));
                
                % lambda_try  = (exp(-H_p1) + Exp2 - exp(Q)*exp(-phi*C))/(exp(Q)*exp(-phi*C)*(1-theta));
                
                MC          = (eta - 1)/eta;
                
                W           = log( MC * exp(a_t) );
                
                N           = log ( (exp(W)*exp(-phi*C)*(1+chi*lambda_ni)/omega)^(1/nu) );
                
                Y           = log(exp(a_t)*exp(N));
                
                %exp(Y) = exp(C) + dH + psi_H/2*(dH/exp(H) - delta_H)^2; % this solves for dH
                
                % p           = [psi_H/(2*exp(H)^2) (1 - psi_H*delta_H/exp(H)) psi_H*delta_H^2/2 + exp(C) - exp(Y)];
                
                p           = [psi_H/2*1/exp(H)^2 (1 - psi_H*delta_H/exp(H))  psi_H/2*delta_H^2 + 1/chi*(exp(Q)*exp(H_p1)*(1-chi) - b) - exp(Y)];
                
                dH          =  max((-p(2) + sqrt(p(2)^2 - 4*p(1)*p(3)))/(2*p(1)),-(1-delta_H)*exp(H));
                
                H_p1        = log(dH+(1-delta_H)*exp(H));
                
                % a_t_p1      = rho_a*a_t + sig_a*eps_a_t_gh;
                
                Q_ni      = log(1+psi_H*(dH/exp(H)-delta_H));
                
                Pi        = exp(a_t) * exp(N) * (1-MC) + exp(Q_ni)*exp(H_p1) - dH-(1-delta_H)*exp(Q_ni)*exp(H)-psi_H/2*(dH/exp(H)-delta_H)^2;
                
                C_ni      = log(max(exp(Y) - dH - psi_H/2*(dH/exp(H) - delta_H)^2, 1e-9));
                
                r         = 1/bet * exp(-phi*C_ni)*Exp1 - 1;
                
                dist2constr_ni = 0;
                
                b_ni      = theta*exp(Q_ni)*exp(H_p1);
                
%                 b_ni      = theta*exp(Q)*exp(H_p1);
%                 
%                 dist2constr_ni = 0;
%                 
%                 Exp1        = w'*(1./(exp(-phi*FC(H_p1*ones(n_gh,1), a_t_p1)).*(1+chi*max(0,FL(H_p1*ones(n_gh,1), a_t_p1)))));
%                 
%                 Exp2        = w'*(bet*(1-delta_H)*exp(FQ(H_p1*ones(n_gh,1), a_t_p1)).*exp(-phi*FC(H_p1*ones(n_gh,1), a_t_p1)).*(1+chi*max(0,FL(H_p1*ones(n_gh,1), a_t_p1))));
%                 
%                 lambda_ni   = max(0,1/(1-theta) * ( (exp(-H_p1) + Exp2 )/exp(Q-phi*C_ni) - 1));
                
%                 dist2constr_ni = 0;
%                 
%                 r         = 1/bet * exp(-phi*C)*Exp1 - 1;
%                 

% 
%                 
%                 MC        = (eta - 1)/eta;
%                 
%                 W         = log( MC * exp(a_t) );
%                 
%                 N         = log ( (exp(W)*exp(-phi*C)*(1+chi*lambda)/omega)^(1/nu) );
% 
%                 Y         = log(exp(a_t)*exp(N));
%                 
%                 p         = [psi_H/2*1/exp(H)^2 (1 - psi_H*delta_H/exp(H))  psi_H/2*delta_H^2 + 1/chi*(exp(Q)*exp(H_p1)*(1-chi) - b) - exp(Y)];
%                 
%                 dH        =  max((-p(2) + sqrt(p(2)^2 - 4*p(1)*p(3)))/(2*p(1)),-(1-delta_H)*exp(H));
% 
%                 a_t_p1    = rho_a*a_t + sig_a*eps_a_t_gh;
%                                 
%                 H_p1      = log(dH+(1-delta_H)*exp(H));
%                 
%                 Q_ni      = log(1+psi_H*(dH/exp(H)-delta_H));
%                 
%                 C_ni      = log(max(exp(Y) - dH - psi_H/2*(dH/exp(H) - delta_H)^2, 1e-9));
%                 
                
               
                
            end
            
            C_gr_p1(ii,jj)           = C_ni;
            b_gr_p1(ii,jj)           = b_ni;
            Q_gr_p1(ii,jj)           = Q_ni;
            H_p1_gr(ii,jj)           = H_p1;
            lambda_gr_p1(ii,jj)      = lambda_ni;
            dist2constr_gr_p1(ii,jj) = dist2constr_ni;
            N_gr(ii,jj)              = N;
            r_gr(ii,jj)              = r;
            dH_gr(ii,jj)             = dH;

            U_gr(ii,jj)              = exp(C_ni).^(1-phi)/(1-phi) + omega*exp(N).^(1+nu)/(1+nu) + bet*w'*FU(H_p1*ones(n_gh,1), a_t_p1);
            
        end
        
    end
    
    diff           = max(max(max(max(abs(C_gr_p1 - C_gr))), max(max(abs(lambda_gr_p1 - lambda_gr)))), max(max(abs(dist2constr_gr_p1 - dist2constr_gr))))
    %diff           = max(max(max(abs(lambda_gr_p1.*dist2constr_gr_p1))), diff)
    C_gr           = C_gr_p1;
    b_gr           = b_gr_p1;
    Q_gr           = Q_gr_p1;
    lambda_gr      = lambda_gr_p1;
    dist2constr_gr = dist2constr_gr_p1;
    
    
    surf(H_gr, a_gr, lambda_gr)
    title('lambda')
    pause(.005)
end



%% Simulations

n_simul = 5000;
n_burn  = 4800;

Fb   = griddedInterpolant(H_gr, a_gr, b_gr,'linear','linear');
FdH  = griddedInterpolant(H_gr, a_gr, dH_gr,'linear','linear');
FH   = griddedInterpolant(H_gr, a_gr, H_p1_gr,'linear','linear');
Fr   = griddedInterpolant(H_gr, a_gr, r_gr,'linear','linear');

x0     = [H_ss, a_ss];
a_t(1) = a_ss;

C_simul = NaN(n_simul,1);
Q_simul = NaN(n_simul,1);
b_simul = NaN(n_simul,1);
N_simul = NaN(n_simul,1);
r_simul = NaN(n_simul,1);
lambda_simul = NaN(n_simul,1);
H_p1_simul   = NaN(n_simul,1);
dH_simul     = NaN(n_simul,1);
 

for ii = 1:n_simul
    
    C_simul(ii)         = FC(x0);
    Q_simul(ii)         = FQ(x0);
    b_simul(ii)         = Fb(x0);
    N_simul(ii)         = FN(x0);
    r_simul(ii)         = Fr(x0);
    lambda_simul(ii)    = FL(x0);
    dH_simul(ii)        = FdH(x0);
    H_p1_simul(ii)      = FH(x0);
    
    if ii < n_simul
        a_t(ii+1)             = rho_a*a_t(ii) + sig_a*randn(1);
    end
    
    x0           = [FH(x0), a_t(ii)];
    
end

figure;
subplot(2,4,1)
plot([C_simul(n_burn:end)])
title('Consumption')
axis tight
subplot(2,4,2)
plot([Q_simul(n_burn:end)])
title('Q')
axis tight
subplot(2,4,3)
plot([b_simul(n_burn:end)])
title('b')
axis tight
subplot(2,4,4)
plot([N_simul(n_burn:end)])
title('N')
axis tight
subplot(2,4,5)
plot([r_simul(n_burn:end)])
title('r')
axis tight
subplot(2,4,6)
plot([lambda_simul(n_burn:end)])
title('lambda')
axis tight
subplot(2,4,7)
plot([H_p1_simul(n_burn:end)])
title('H_{p1}')
axis tight
subplot(2,4,8)
plot([a_t(n_burn:end)])
title('a_t')
axis tight

H_p1_em = mean(H_p1_simul);

%% IRF

n_simul = 300;
n_burn  = 200;

Fb   = griddedInterpolant(H_gr, a_gr, b_gr,'linear','linear');
FdH  = griddedInterpolant(H_gr, a_gr, dH_gr,'linear','linear');
FH   = griddedInterpolant(H_gr, a_gr, H_p1_gr,'linear','linear');
Fr   = griddedInterpolant(H_gr, a_gr, r_gr,'linear','linear');

x0     = [H_ss, a_ss];
a_t(1) = a_ss;

C_simul = NaN(n_simul,1);
Q_simul = NaN(n_simul,1);
b_simul = NaN(n_simul,1);
N_simul = NaN(n_simul,1);
r_simul = NaN(n_simul,1);
lambda_simul = NaN(n_simul,1);
H_p1_simul   = NaN(n_simul,1);
dH_simul     = NaN(n_simul,1);
 

for ii = 1:n_simul
    
    C_simul(ii)         = FC(x0);
    Q_simul(ii)         = FQ(x0);
    b_simul(ii)         = Fb(x0);
    N_simul(ii)         = FN(x0);
    r_simul(ii)         = Fr(x0);
    lambda_simul(ii)    = FL(x0);
    dH_simul(ii)        = FdH(x0);
    H_p1_simul(ii)      = FH(x0);
    
    if ii >= n_burn && ii <= n_burn+20
        a_t(ii+1)             = -3*sig_a;
    else
        a_t(ii+1)             = rho_a*a_t(ii);
    end
    
    x0           = [FH(x0), a_t(ii)];
    
end

figure;
subplot(2,4,1)
plot([C_simul(n_burn:end)-C_simul(n_burn)])
title('Consumption')
axis tight
subplot(2,4,2)
plot([Q_simul(n_burn:end)-Q_simul(n_burn)])
title('Q')
axis tight
subplot(2,4,3)
plot([b_simul(n_burn:end)-b_simul(n_burn)])
title('b')
axis tight
subplot(2,4,4)
plot([N_simul(n_burn:end)-N_simul(n_burn)])
title('N')
axis tight
subplot(2,4,5)
plot([r_simul(n_burn:end)-r_simul(n_burn)])
title('r')
axis tight
subplot(2,4,6)
plot([lambda_simul(n_burn:end)])
title('lambda')
axis tight
subplot(2,4,7)
plot([H_p1_simul(n_burn:end)-H_p1_simul(n_burn)])
title('H_{p1}')
axis tight
subplot(2,4,8)
plot([a_t(n_burn:end)])
title('a_t')
axis tight
