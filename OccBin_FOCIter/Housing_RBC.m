clear all
close all



% Parameters
% Household
bet    =            .99; % consistent with 2% annual real rate
phi    =          1/0.3; % inverse of IES
nu     =              2; % 1/Frish
N      =              0; % Labor Scaling
chi    =            0.7; % fraction of borrowers

theta_grid            = 0.6;%0.05:0.005:0.95; % Borrowing Constraint (% of Assets)
dist2constr_grid      = NaN(length(theta_grid),3);
dist2constr_grid(:,1) = theta_grid;

% Housing Producers
psi_H   =             2; % IAC for housing
delta_H =         0.025; % Depreciation housing
H_ss    =             1;

% Goods Producers
rho_a  =            0.9; % TFP persistence
sig_a  =           0.01; % TFP stdev

psi_P  =             40; % Rotemberg AC - Not yet implemented
eta    =              7; % Demand Elasticity, so that (1+lambda) = 1.17

% Monetary Policy
phi_pi =            1.5; % TR inflation - Not yet implemented
phi_Y  =            0.5; % TR output    - Not yet implemented

ii_max = length(theta_grid);

% Compute the Steady State for the given parameters
for ii = 1:ii_max
    
    theta = theta_grid(ii);
    
    % Compute the Steady State in the unconstrained economy
    % Variables: r, Q, a_t, MC, W, H, omega, Pi, C, Y, lambda
    r             = 1/bet - 1;
    Q             = 0;
    a_t           = 0;
    MC            = (eta - 1)/eta;
    W             = log(MC * exp(a_t));
    
    C = log(0.5);
    diff = 10;
    while diff > 1e-7
        omega = exp(W)/(exp(C)^phi * exp(N)^nu);
        Pi = exp(a_t)* exp(N) * (1-MC);
        H  = log( (exp(W+N) + Pi - exp(C))/delta_H );
        C_jjp1  = log( ( exp(-H) / (1 - bet*(1-delta_H)) )^(-1/phi) );
        diff = abs(C_jjp1 - C);
        C = C_jjp1;
    end
    diff
    b           = exp(Q)*exp(H)*( 1 - chi*(1-delta_H) ) - chi*(exp(W)*exp(N)+Pi);
    dist2constr = b - theta*exp(Q)*exp(H);
    dH          = log(exp(H)*delta_H);
    Y           = log(exp(C) + exp(dH));
    lambda      = 0;
    SS_uncon    = [r, Q, a_t, MC, W, H, omega, Pi, C, Y, lambda, dist2constr];
    
    dist2constr_grid(ii,2) = dist2constr;
    
    if dist2constr > 0
        %Guess
        C             = log(0.5);
        lambda        = 1;
        %Solution
        r             = 1/(bet*(1+chi*lambda)) - 1;
        Q             = 0;
        a_t           = 0;
        MC            = (eta - 1)/eta;
        W             = log(MC * exp(a_t));
        diff = 10;
        while diff > 1e-7
            omega=exp(W)/exp(C)^phi*(1+chi*lambda)*exp(N)^nu;
            Pi = exp(a_t)* exp(N) * (1-MC);
            b  = theta*exp(Q)*exp(H);
            H = log(b + chi*(exp(W+N)+(1-delta_H)*exp(H)+Pi));
            C_jjp1      = log(exp(W+N) + Pi - delta_H*exp(H));
            lambda_jjp1 =  (exp(-H)/exp(C_jjp1)^(-phi) - 1 + bet*(1-delta_H))/((1-theta) - bet*(1-delta_H)*chi);
            diff = max(abs(C_jjp1 - C), abs(lambda - lambda_jjp1));
            C      = C_jjp1;
            lambda = lambda_jjp1;
        end
        diff
        dist2constr = b - theta*exp(Q)*exp(H);
        dH         = log(exp(H)*delta_H);
        Y          = log(exp(C) + exp(dH));
        SS_con   = [r, Q, a_t, MC, W, H, omega, Pi, C, Y, lambda, dist2constr];
        dist2constr_grid(ii,3) = dist2constr;
    end
end

% Productivity Shocks - Gauss-Hermite nodes

n_gh   = 5;
n_grid = 100;
n_min  = 0.70;
n_max  = 1.30;

[eps_a_t_gh, w]=hermquad(n_gh);

a_grid = linspace(-2*sig_a/(1-rho_a), 2*sig_a/(1-rho_a), n_gh);

% Construct the approximation grid

if dist2constr < 0
    H_grid            = linspace(log(exp(H)*n_min), log(exp(H)*n_max), n_grid);
    [H_gr,a_gr]       = ndgrid(H_grid, a_grid);
    C_guess           = linspace(log(exp(C)*n_min), log(exp(C)*n_max), n_grid);
    [C_gr,a_gr]       = ndgrid(C_guess, a_grid);
    Q_guess           = linspace(log(exp(Q)*n_min), log(exp(Q)*n_max), n_grid);
    [Q_gr,a_gr]       = ndgrid(Q_guess, a_grid);
    lambda_guess      = max(0,linspace(lambda*n_min, lambda*n_max, n_grid));
    [lambda_gr,a_gr]  = ndgrid(lambda_guess+1, a_grid);
    dist2constr_guess      = linspace(dist2constr, dist2constr, n_grid);
    [dist2constr_gr,a_gr]  = ndgrid(dist2constr_guess, a_grid);
else
    H_grid            = linspace(log(exp(H)*n_min), log(exp(H)*n_max), n_grid);
    [H_gr,a_gr]       = ndgrid(H_grid, a_grid);
    C_guess           = linspace(log(exp(C)*n_min), log(exp(C)*n_max), n_grid);
    [C_gr,a_gr]       = ndgrid(C_guess, a_grid);
    Q_guess           = linspace(log(exp(Q)*n_min), log(exp(Q)*n_max), n_grid);
    [Q_gr,a_gr]       = ndgrid(Q_guess, a_grid);
    lambda_guess      = max(0,linspace(lambda*n_min, lambda*n_max, n_grid));
    [lambda_gr,a_gr]  = ndgrid(lambda_guess+1, a_grid);
    dist2constr_guess      = linspace(dist2constr, dist2constr, n_grid);
    [dist2constr_gr,a_gr]  = ndgrid(dist2constr_guess, a_grid);
end

b_grid              = linspace(b*n_min, b*n_max, n_grid);
[b_gr,a_gr]         = ndgrid(b_grid, a_grid);
H_p1_gr             = H_gr;
b_gr_p1             = b_gr;
Q_gr_p1             = Q_gr;
lambda_gr_p1        = NaN(size(C_gr));
dist2constr_gr_p1   = NaN(size(C_gr));
N_gr                = NaN(size(C_gr));
r_gr                = NaN(size(C_gr));

disp('The model is solved around a Steady State with')

lambda

dist2constr

pause

C_gr_p1 = C_gr;

diff = 10

while diff > 1e-10
    
    for ii = 1:n_grid
        for jj = 1:n_gh
            
            H           = H_gr(ii,jj);
            H_p1        = H_p1_gr(ii,jj);
            a_t         = a_gr(ii,jj);
            C           = C_gr(ii,jj);
            Q           = Q_gr(ii,jj);
            lambda      = lambda_gr(ii,jj);
            b           = b_gr(ii,jj);
            
            %dist2constr = dist2constr_gr(ii,jj);
            FC = griddedInterpolant(H_gr, a_gr, C_gr,'linear','linear');
            FQ = griddedInterpolant(H_gr, a_gr, Q_gr,'linear','linear');
            FL = griddedInterpolant(H_gr, a_gr, lambda_gr,'linear','linear');
            
            % Compute the solution under RE
            
            if b < theta*exp(Q)*exp(H_p1)
                
                lambda      = 0;
                
                MC          = (eta - 1)/eta;
                
                W           = log( MC * exp(a_t) );
                
                N           = log ( (exp(W)*exp(-phi*C)*(1+chi*lambda)/omega)^(1/nu) );
                
                Y           = log(exp(a_t)*exp(N));
                
                %exp(Y) = exp(C) + dH + psi_H/2*(dH/exp(H) - delta_H)^2; % this solves for dH
                
                p           = [psi_H/(2*exp(H)^2) (1 - psi_H*delta_H/exp(H)) psi_H*delta_H^2/2 + exp(C) - exp(Y)];
                
                %dH         = log(max(max(real(roots(p))),1e-9));
                
                dH          = max((-p(2) + sqrt(p(2)^2 - 4*p(1)*p(3)))/(2*p(1)),-(1-delta_H)*exp(H));
                
                H_p1        = log(dH+(1-delta_H)*exp(H));
                
                a_t_p1      = rho_a*a_t + sig_a*eps_a_t_gh;
                
                b_ni        = max(0,exp(Q+H_p1) - (chi*(exp(W)*exp(N)+(1-delta_H)*exp(Q)*exp(H)+Pi)));
                
                dist2constr_ni = b_ni - theta*exp(Q)*exp(H_p1);
                
                lambda_ni = 0;
                
                Q_ni        = log(1+psi_H*(dH/exp(H)-delta_H));
                
                Pi          = exp(a_t) * exp(N) * (1-MC) + exp(Q)*exp(H_p1) - dH-(1-delta_H)*exp(Q)*exp(H)-psi_H/2*(dH/exp(H)-delta_H)^2;
                
                Exp1        = w'*(1./(exp(-phi*FC(H_p1*ones(n_gh,1), a_t_p1)).*(1+chi*max(0,FL(H_p1*ones(n_gh,1), a_t_p1)))));
                
                Exp2        = w'*(bet*(1-delta_H)*exp(FQ(H_p1*ones(n_gh,1), a_t_p1)).*exp(-phi*FC(H_p1*ones(n_gh,1), a_t_p1)).*(1+chi*max(0,FL(H_p1*ones(n_gh,1), a_t_p1))));
                
                C_ni        = log( max( (1/(exp(Q)*(1+(1-theta)*lambda_ni)) * ( exp(-H_p1) + Exp2 ))^(-1 /phi), 1e-9));
                
                r           = 1/bet * exp(-phi*C_ni)*Exp1 - 1;
                
                if dist2constr_ni < 0
                    lambda_ni = max(0,1/(1-theta) * ( (exp(-H_p1) + Exp2 )/exp(Q-phi*C_ni) - 1));
                else
                    lambda_ni   = 0;    
                end
                
            elseif b >= theta*exp(Q)*exp(H_p1)
                
                b         = theta*exp(Q)*exp(H_p1);
                
                MC        = (eta - 1)/eta;
                
                W         = log( MC * exp(a_t) );
                
                N         = log ( (exp(W)*exp(-phi*C)*(1+chi*lambda)/omega)^(1/nu) );

                Y         = log(exp(a_t)*exp(N));
                
                p         = [psi_H/2*1/exp(H)^2 (1 - psi_H*delta_H/exp(H))  psi_H/2*delta_H^2 + 1/chi*(exp(Q)*exp(H_p1)*(1-chi) - b) - exp(Y)];
                
                dH        =  max((-p(2) + sqrt(p(2)^2 - 4*p(1)*p(3)))/(2*p(1)),-(1-delta_H)*exp(H));

                a_t_p1    = rho_a*a_t + sig_a*eps_a_t_gh;
                                
                H_p1      = log(dH+(1-delta_H)*exp(H));
                
                Q_ni      = log(1+psi_H*(dH/exp(H)-delta_H));
                
                C_ni      = log(max(exp(Y) - dH - psi_H/2*(dH/exp(H) - delta_H)^2, 1e-9));
                
                Exp1      = w'*(1./(exp(-phi*FC(H_p1*ones(n_gh,1), a_t_p1)).*(1+chi*max(0,FL(H_p1*ones(n_gh,1), a_t_p1)))));
                
                Exp2      = w'*(bet*(1-delta_H)*exp(FQ(H_p1*ones(n_gh,1), a_t_p1)).*exp(-phi*FC(H_p1*ones(n_gh,1), a_t_p1)).*(1+chi*max(0,FL(H_p1*ones(n_gh,1), a_t_p1))));
                
                lambda_ni = 1/(1-theta) * ( (exp(-H_p1) + Exp2 )/exp(Q-phi*C_ni) - 1);
                
                dist2constr_ni = 0;
                
                r         = 1/bet * exp(-phi*C_ni)*Exp1 - 1;
                
                b_ni      = theta*exp(Q)*exp(H_p1);
                
            end
            
            
            C_gr_p1(ii,jj)           = C_ni;
            Q_gr_p1(ii,jj)           = Q_ni;
            H_p1_gr(ii,jj)           = H_p1;
            lambda_gr_p1(ii,jj)      = lambda_ni;
            b_gr_p1(ii,jj)           = b_ni;
            dist2constr_gr_p1(ii,jj) = dist2constr_ni;
            N_gr(ii,jj)              = N;
            r_gr(ii,jj)              = r;
            
        end
        
    end
    
    diff           = max(max(max(max(abs(C_gr_p1 - C_gr))), max(max(abs(lambda_gr_p1 - lambda_gr)))), max(max(abs(dist2constr_gr_p1 - dist2constr_gr))))
    %diff           = max(max(max(abs(lambda_gr_p1.*dist2constr_gr_p1))), diff)
    C_gr           = C_gr_p1;
    lambda_gr      = lambda_gr_p1;
    dist2constr_gr = dist2constr_gr_p1;
    Q_gr           = Q_gr_p1;
    b_gr           = b_gr_p1;
    %surf(H_gr, a_gr, lambda_gr)
    %pause(.1)
end


% surf(H_gr, a_gr, b_gr./(theta*exp(Q_gr).*exp(H_p1_gr)))
% title('LTV ratio')
% figure;
% surf(H_gr, a_gr, lambda_gr)
% title('Multiplier')


% update state

% dH        = exp(H_p1) - (1-delta_H) * exp(H);
%C_ni      = log( max(exp(W+N) + Pi - exp(Q)*(exp(H_p1) - (1-delta_H)*exp(H)),1e-9) );