% Dynare file to simulate large household model
% with the constraint never binding
% closed economy version

var C H N b B;
var W Q r;
var lambda;
var disttoconstraint;

varexo epsw;

parameters betta nu omega phi;
parameters chi theta;
parameters sigmaw rhow;

@#include "largehh_parameters.m"

%shorthand notation:

model;

%borrower BC
exp(Q+H) = b + chi*(exp(W+N)+exp(Q+H(-1))+B-(1+r(-1))*B(-1));
%aggregate BC
exp(C) = exp(W+N)+B-(1+r(-1))*B(-1);
%borrowing constraint (removed)
%chi*B+b = theta*exp(Q+H);
disttoconstraint=1-(chi*B+b)/(theta*exp(Q+H));
%borrower FOC for housing:
exp(Q-phi*C)*(1+(1-theta)*lambda) = exp(-H) + betta*exp(Q(+1))*exp(-phi*C(+1))*(1+chi*lambda(+1));
%lender FOC for loans:
exp(-phi*C) = betta*(1+r)*exp(-phi*C(+1))*(1+chi*lambda(+1));
%labor FOC:
omega*exp(nu*N)=exp(W-phi*C)*(1+chi*lambda);

%labor demand
W=rhow*W(-1)+sigmaw*epsw;
%housing supply
H=0;
%aggregate loan supply
B=0;

%constraint not binding
lambda=0;

end;

%initial SS values from case with phi=1 (fingers crossed)
initval;

%exogenous
W=0;
H=0;
B=0;

%multiplier:
lambda = 0;

%other endogenous:
r = 1/betta-1;
N = -1/(nu + 1)*log(omega);
C = W + N;
Q = C - H -log(1-betta);
b = exp(Q+H) - chi*(exp(W+N)+exp(Q+H));
disttoconstraint=1-(chi*B+b)/(theta*exp(Q+H));

end;


shocks;
var epsw; stderr 1;
end;

%stoch_simul(order=1,irf=20,nocorr,ar=0);
stoch_simul(order=1,irf=100,nomoments,nograph);
%save SS for learning solution
ys_RE=oo_.dr.ys;
save largehh_closeduncon_SS ys_RE;
cleanup_dyn(M_.fname);
