%
% Status : main Dynare file 
%
% Warning : this file is generated automatically by Dynare
%           from model file (.mod)

clear all
tic;
global M_ oo_ options_ ys0_ ex0_ estimation_info
options_ = [];
M_.fname = 'largehh_closeduncon';
%
% Some global variables initialization
%
global_initialization;
diary off;
diary('largehh_closeduncon.log');
M_.exo_names = 'epsw';
M_.exo_names_tex = 'epsw';
M_.exo_names_long = 'epsw';
M_.endo_names = 'C';
M_.endo_names_tex = 'C';
M_.endo_names_long = 'C';
M_.endo_names = char(M_.endo_names, 'H');
M_.endo_names_tex = char(M_.endo_names_tex, 'H');
M_.endo_names_long = char(M_.endo_names_long, 'H');
M_.endo_names = char(M_.endo_names, 'N');
M_.endo_names_tex = char(M_.endo_names_tex, 'N');
M_.endo_names_long = char(M_.endo_names_long, 'N');
M_.endo_names = char(M_.endo_names, 'b');
M_.endo_names_tex = char(M_.endo_names_tex, 'b');
M_.endo_names_long = char(M_.endo_names_long, 'b');
M_.endo_names = char(M_.endo_names, 'B');
M_.endo_names_tex = char(M_.endo_names_tex, 'B');
M_.endo_names_long = char(M_.endo_names_long, 'B');
M_.endo_names = char(M_.endo_names, 'W');
M_.endo_names_tex = char(M_.endo_names_tex, 'W');
M_.endo_names_long = char(M_.endo_names_long, 'W');
M_.endo_names = char(M_.endo_names, 'Q');
M_.endo_names_tex = char(M_.endo_names_tex, 'Q');
M_.endo_names_long = char(M_.endo_names_long, 'Q');
M_.endo_names = char(M_.endo_names, 'r');
M_.endo_names_tex = char(M_.endo_names_tex, 'r');
M_.endo_names_long = char(M_.endo_names_long, 'r');
M_.endo_names = char(M_.endo_names, 'lambda');
M_.endo_names_tex = char(M_.endo_names_tex, 'lambda');
M_.endo_names_long = char(M_.endo_names_long, 'lambda');
M_.endo_names = char(M_.endo_names, 'disttoconstraint');
M_.endo_names_tex = char(M_.endo_names_tex, 'disttoconstraint');
M_.endo_names_long = char(M_.endo_names_long, 'disttoconstraint');
M_.param_names = 'betta';
M_.param_names_tex = 'betta';
M_.param_names_long = 'betta';
M_.param_names = char(M_.param_names, 'nu');
M_.param_names_tex = char(M_.param_names_tex, 'nu');
M_.param_names_long = char(M_.param_names_long, 'nu');
M_.param_names = char(M_.param_names, 'omega');
M_.param_names_tex = char(M_.param_names_tex, 'omega');
M_.param_names_long = char(M_.param_names_long, 'omega');
M_.param_names = char(M_.param_names, 'phi');
M_.param_names_tex = char(M_.param_names_tex, 'phi');
M_.param_names_long = char(M_.param_names_long, 'phi');
M_.param_names = char(M_.param_names, 'chi');
M_.param_names_tex = char(M_.param_names_tex, 'chi');
M_.param_names_long = char(M_.param_names_long, 'chi');
M_.param_names = char(M_.param_names, 'theta');
M_.param_names_tex = char(M_.param_names_tex, 'theta');
M_.param_names_long = char(M_.param_names_long, 'theta');
M_.param_names = char(M_.param_names, 'sigmaw');
M_.param_names_tex = char(M_.param_names_tex, 'sigmaw');
M_.param_names_long = char(M_.param_names_long, 'sigmaw');
M_.param_names = char(M_.param_names, 'rhow');
M_.param_names_tex = char(M_.param_names_tex, 'rhow');
M_.param_names_long = char(M_.param_names_long, 'rhow');
M_.exo_det_nbr = 0;
M_.exo_nbr = 1;
M_.endo_nbr = 10;
M_.param_nbr = 8;
M_.orig_endo_nbr = 10;
M_.aux_vars = [];
M_.Sigma_e = zeros(1, 1);
M_.Correlation_matrix = eye(1, 1);
M_.H = 0;
M_.Correlation_matrix_ME = 1;
options_.block=0;
options_.bytecode=0;
options_.use_dll=0;
erase_compiled_function('largehh_closeduncon_static');
erase_compiled_function('largehh_closeduncon_dynamic');
M_.lead_lag_incidence = [
 0 5 15;
 1 6 0;
 0 7 0;
 0 8 0;
 2 9 0;
 3 10 0;
 0 11 16;
 4 12 0;
 0 13 17;
 0 14 0;]';
M_.nstatic = 3;
M_.nfwrd   = 3;
M_.npred   = 4;
M_.nboth   = 0;
M_.nsfwrd   = 3;
M_.nspred   = 4;
M_.ndynamic   = 7;
M_.equations_tags = {
};
M_.static_and_dynamic_models_differ = 0;
M_.exo_names_orig_ord = [1:1];
M_.maximum_lag = 1;
M_.maximum_lead = 1;
M_.maximum_endo_lag = 1;
M_.maximum_endo_lead = 1;
oo_.steady_state = zeros(10, 1);
M_.maximum_exo_lag = 0;
M_.maximum_exo_lead = 0;
oo_.exo_steady_state = zeros(1, 1);
M_.params = NaN(8, 1);
M_.NNZDerivatives = zeros(3, 1);
M_.NNZDerivatives(1) = 41;
M_.NNZDerivatives(2) = -1;
M_.NNZDerivatives(3) = -1;
M_.params( 1 ) = 0.99;
betta = M_.params( 1 );
M_.params( 2 ) = .33;
nu = M_.params( 2 );
M_.params( 3 ) = 1;
omega = M_.params( 3 );
M_.params( 4 ) = 2;
phi = M_.params( 4 );
M_.params( 5 ) = 0.7;
chi = M_.params( 5 );
M_.params( 6 ) = 0.3;
theta = M_.params( 6 );
M_.params( 7 ) = 0.01;
sigmaw = M_.params( 7 );
M_.params( 8 ) = 0.9;
rhow = M_.params( 8 );
rwedge=0.995;
psi=1;
deltaH=0.025;
g=0.005;
sigmav=0.01;
%
% INITVAL instructions
%
options_.initval_file = 0;
oo_.steady_state( 6 ) = 0;
oo_.steady_state( 2 ) = 0;
oo_.steady_state( 5 ) = 0;
oo_.steady_state( 9 ) = 0;
oo_.steady_state( 8 ) = 1/M_.params(1)-1;
oo_.steady_state( 3 ) = (-1)/(1+M_.params(2))*log(M_.params(3));
oo_.steady_state( 1 ) = oo_.steady_state(6)+oo_.steady_state(3);
oo_.steady_state( 7 ) = oo_.steady_state(1)-oo_.steady_state(2)-log(1-M_.params(1));
oo_.steady_state( 4 ) = exp(oo_.steady_state(2)+oo_.steady_state(7))-M_.params(5)*(exp(oo_.steady_state(2)+oo_.steady_state(7))+exp(oo_.steady_state(6)+oo_.steady_state(3)));
oo_.steady_state( 10 ) = 1-(M_.params(5)*oo_.steady_state(5)+oo_.steady_state(4))/(exp(oo_.steady_state(2)+oo_.steady_state(7))*M_.params(6));
if M_.exo_nbr > 0;
	oo_.exo_simul = [ones(M_.maximum_lag,1)*oo_.exo_steady_state'];
end;
if M_.exo_det_nbr > 0;
	oo_.exo_det_simul = [ones(M_.maximum_lag,1)*oo_.exo_det_steady_state'];
end;
%
% SHOCKS instructions
%
make_ex_;
M_.exo_det_length = 0;
M_.Sigma_e(1, 1) = (1)^2;
M_.sigma_e_is_diagonal = 1;
options_.irf = 100;
options_.nograph = 1;
options_.nomoments = 1;
options_.order = 1;
var_list_=[];
info = stoch_simul(var_list_);
ys_RE=oo_.dr.ys;
save largehh_closeduncon_SS ys_RE;
cleanup_dyn(M_.fname);
save('largehh_closeduncon_results.mat', 'oo_', 'M_', 'options_');
if exist('estim_params_', 'var') == 1
  save('largehh_closeduncon_results.mat', 'estim_params_', '-append');
end
if exist('bayestopt_', 'var') == 1
  save('largehh_closeduncon_results.mat', 'bayestopt_', '-append');
end
if exist('dataset_', 'var') == 1
  save('largehh_closeduncon_results.mat', 'dataset_', '-append');
end
if exist('estimation_info', 'var') == 1
  save('largehh_closeduncon_results.mat', 'estimation_info', '-append');
end


disp(['Total computing time : ' dynsec2hms(toc) ]);
if ~isempty(lastwarn)
  disp('Note: warning(s) encountered in MATLAB/Octave code')
end
diary off
