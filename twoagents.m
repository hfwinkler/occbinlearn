%
% Status : main Dynare file 
%
% Warning : this file is generated automatically by Dynare
%           from model file (.mod)

clear all
tic;
global M_ oo_ options_ ys0_ ex0_ estimation_info
options_ = [];
M_.fname = 'twoagents';
%
% Some global variables initialization
%
global_initialization;
diary off;
diary('twoagents.log');
M_.exo_names = 'epsw';
M_.exo_names_tex = 'epsw';
M_.exo_names_long = 'epsw';
M_.endo_names = 'Cs';
M_.endo_names_tex = 'Cs';
M_.endo_names_long = 'Cs';
M_.endo_names = char(M_.endo_names, 'Hs');
M_.endo_names_tex = char(M_.endo_names_tex, 'Hs');
M_.endo_names_long = char(M_.endo_names_long, 'Hs');
M_.endo_names = char(M_.endo_names, 'Ls');
M_.endo_names_tex = char(M_.endo_names_tex, 'Ls');
M_.endo_names_long = char(M_.endo_names_long, 'Ls');
M_.endo_names = char(M_.endo_names, 'B');
M_.endo_names_tex = char(M_.endo_names_tex, 'B');
M_.endo_names_long = char(M_.endo_names_long, 'B');
M_.endo_names = char(M_.endo_names, 'Cb');
M_.endo_names_tex = char(M_.endo_names_tex, 'Cb');
M_.endo_names_long = char(M_.endo_names_long, 'Cb');
M_.endo_names = char(M_.endo_names, 'Hb');
M_.endo_names_tex = char(M_.endo_names_tex, 'Hb');
M_.endo_names_long = char(M_.endo_names_long, 'Hb');
M_.endo_names = char(M_.endo_names, 'Lb');
M_.endo_names_tex = char(M_.endo_names_tex, 'Lb');
M_.endo_names_long = char(M_.endo_names_long, 'Lb');
M_.endo_names = char(M_.endo_names, 'W');
M_.endo_names_tex = char(M_.endo_names_tex, 'W');
M_.endo_names_long = char(M_.endo_names_long, 'W');
M_.endo_names = char(M_.endo_names, 'Q');
M_.endo_names_tex = char(M_.endo_names_tex, 'Q');
M_.endo_names_long = char(M_.endo_names_long, 'Q');
M_.endo_names = char(M_.endo_names, 'r');
M_.endo_names_tex = char(M_.endo_names_tex, 'r');
M_.endo_names_long = char(M_.endo_names_long, 'r');
M_.endo_names = char(M_.endo_names, 'lambda');
M_.endo_names_tex = char(M_.endo_names_tex, 'lambda');
M_.endo_names_long = char(M_.endo_names_long, 'lambda');
M_.param_names = 'bettas';
M_.param_names_tex = 'bettas';
M_.param_names_long = 'bettas';
M_.param_names = char(M_.param_names, 'gamas');
M_.param_names_tex = char(M_.param_names_tex, 'gamas');
M_.param_names_long = char(M_.param_names_long, 'gamas');
M_.param_names = char(M_.param_names, 'nus');
M_.param_names_tex = char(M_.param_names_tex, 'nus');
M_.param_names_long = char(M_.param_names_long, 'nus');
M_.param_names = char(M_.param_names, 'etas');
M_.param_names_tex = char(M_.param_names_tex, 'etas');
M_.param_names_long = char(M_.param_names_long, 'etas');
M_.param_names = char(M_.param_names, 'phis');
M_.param_names_tex = char(M_.param_names_tex, 'phis');
M_.param_names_long = char(M_.param_names_long, 'phis');
M_.param_names = char(M_.param_names, 'theta');
M_.param_names_tex = char(M_.param_names_tex, 'theta');
M_.param_names_long = char(M_.param_names_long, 'theta');
M_.param_names = char(M_.param_names, 'betta');
M_.param_names_tex = char(M_.param_names_tex, 'betta');
M_.param_names_long = char(M_.param_names_long, 'betta');
M_.param_names = char(M_.param_names, 'gama');
M_.param_names_tex = char(M_.param_names_tex, 'gama');
M_.param_names_long = char(M_.param_names_long, 'gama');
M_.param_names = char(M_.param_names, 'nu');
M_.param_names_tex = char(M_.param_names_tex, 'nu');
M_.param_names_long = char(M_.param_names_long, 'nu');
M_.param_names = char(M_.param_names, 'eta');
M_.param_names_tex = char(M_.param_names_tex, 'eta');
M_.param_names_long = char(M_.param_names_long, 'eta');
M_.param_names = char(M_.param_names, 'phi');
M_.param_names_tex = char(M_.param_names_tex, 'phi');
M_.param_names_long = char(M_.param_names_long, 'phi');
M_.param_names = char(M_.param_names, 'chi');
M_.param_names_tex = char(M_.param_names_tex, 'chi');
M_.param_names_long = char(M_.param_names_long, 'chi');
M_.param_names = char(M_.param_names, 'sigmaw');
M_.param_names_tex = char(M_.param_names_tex, 'sigmaw');
M_.param_names_long = char(M_.param_names_long, 'sigmaw');
M_.param_names = char(M_.param_names, 'rhow');
M_.param_names_tex = char(M_.param_names_tex, 'rhow');
M_.param_names_long = char(M_.param_names_long, 'rhow');
M_.param_names = char(M_.param_names, 'Hbar');
M_.param_names_tex = char(M_.param_names_tex, 'Hbar');
M_.param_names_long = char(M_.param_names_long, 'Hbar');
M_.exo_det_nbr = 0;
M_.exo_nbr = 1;
M_.endo_nbr = 11;
M_.param_nbr = 15;
M_.orig_endo_nbr = 11;
M_.aux_vars = [];
M_.Sigma_e = zeros(1, 1);
M_.Correlation_matrix = eye(1, 1);
M_.H = 0;
M_.Correlation_matrix_ME = 1;
M_.sigma_e_is_diagonal = 1;
options_.block=0;
options_.bytecode=0;
options_.use_dll=0;
erase_compiled_function('twoagents_static');
erase_compiled_function('twoagents_dynamic');
M_.lead_lag_incidence = [
 0 5 16;
 1 6 0;
 0 7 0;
 2 8 0;
 0 9 17;
 3 10 0;
 0 11 0;
 4 12 0;
 0 13 18;
 0 14 0;
 0 15 0;]';
M_.nstatic = 4;
M_.nfwrd   = 3;
M_.npred   = 4;
M_.nboth   = 0;
M_.nsfwrd   = 3;
M_.nspred   = 4;
M_.ndynamic   = 7;
M_.equations_tags = {
};
M_.static_and_dynamic_models_differ = 0;
M_.exo_names_orig_ord = [1:1];
M_.maximum_lag = 1;
M_.maximum_lead = 1;
M_.maximum_endo_lag = 1;
M_.maximum_endo_lead = 1;
oo_.steady_state = zeros(11, 1);
M_.maximum_exo_lag = 0;
M_.maximum_exo_lead = 0;
oo_.exo_steady_state = zeros(1, 1);
M_.params = NaN(15, 1);
M_.NNZDerivatives = zeros(3, 1);
M_.NNZDerivatives(1) = 51;
M_.NNZDerivatives(2) = -1;
M_.NNZDerivatives(3) = -1;
M_.params( 7 ) = 0.9898;
betta = M_.params( 7 );
M_.params( 8 ) = 1;
gama = M_.params( 8 );
M_.params( 9 ) = 1;
nu = M_.params( 9 );
M_.params( 10 ) = 1;
eta = M_.params( 10 );
M_.params( 11 ) = 1;
phi = M_.params( 11 );
M_.params( 6 ) = 0.2;
theta = M_.params( 6 );
M_.params( 12 ) = 1;
chi = M_.params( 12 );
M_.params( 1 ) = 0.99;
bettas = M_.params( 1 );
M_.params( 2 ) = M_.params(8);
gamas = M_.params( 2 );
M_.params( 3 ) = M_.params(9);
nus = M_.params( 3 );
M_.params( 4 ) = M_.params(10);
etas = M_.params( 4 );
M_.params( 5 ) = M_.params(11);
phis = M_.params( 5 );
M_.params( 15 ) = 1;
Hbar = M_.params( 15 );
M_.params( 13 ) = 0.01;
sigmaw = M_.params( 13 );
M_.params( 14 ) = 0.9;
rhow = M_.params( 14 );
g=0.01;
sigmav=0.01;
k1=1-betta-theta*(1-betta/bettas);
%
% INITVAL instructions
%
options_.initval_file = 0;
oo_.steady_state( 8 ) = 0;
oo_.steady_state( 10 ) = 1/M_.params(7)-1;
oo_.steady_state( 5 ) = (-M_.params(11))*log(1+oo_.steady_state(10)*M_.params(12)*M_.params(6)/k1);
oo_.steady_state( 1 ) = (-0.6931471805599453)+log(oo_.steady_state(10)*M_.params(6)*exp(oo_.steady_state(5))/k1+sqrt((exp(oo_.steady_state(5))*M_.params(12)*oo_.steady_state(10)*M_.params(6)/k1)^2+4*M_.params(4)^(-M_.params(2))));
oo_.steady_state( 9 ) = log(exp(oo_.steady_state(1))+exp(oo_.steady_state(5))*M_.params(12)*M_.params(6)/k1)-log(1-M_.params(1))-log(M_.params(15));
oo_.steady_state( 7 ) = (-1)/M_.params(11)*log(M_.params(10))-oo_.steady_state(5)*1/M_.params(11);
oo_.steady_state( 3 ) = 1/M_.params(5)*((-log(M_.params(10)))-M_.params(8)*oo_.steady_state(1));
oo_.steady_state( 6 ) = oo_.steady_state(5)-oo_.steady_state(9)+log(M_.params(12)/k1);
oo_.steady_state( 2 ) = log(M_.params(15)-exp(oo_.steady_state(6)));
oo_.steady_state( 4 ) = M_.params(6)*exp(oo_.steady_state(9)+oo_.steady_state(6))*(1+oo_.steady_state(10));
oo_.steady_state( 11 ) = (1-M_.params(7)*(1+oo_.steady_state(10)))*exp(oo_.steady_state(5)*(-M_.params(8)));
if M_.exo_nbr > 0;
	oo_.exo_simul = [ones(M_.maximum_lag,1)*oo_.exo_steady_state'];
end;
if M_.exo_det_nbr > 0;
	oo_.exo_det_simul = [ones(M_.maximum_lag,1)*oo_.exo_det_steady_state'];
end;
%
% SHOCKS instructions
%
make_ex_;
M_.exo_det_length = 0;
M_.Sigma_e(1, 1) = (1)^2;
options_.ar = 0;
options_.irf = 100;
options_.nocorr = 1;
options_.order = 1;
var_list_=[];
info = stoch_simul(var_list_);
ys_RE=oo_.dr.ys;
M_RE=M_;
oo_RE=oo_;
save twoagents_SS ys_RE M_RE oo_RE;
cleanup_dyn(M_.fname);
save('twoagents_results.mat', 'oo_', 'M_', 'options_');
if exist('estim_params_', 'var') == 1
  save('twoagents_results.mat', 'estim_params_', '-append');
end
if exist('bayestopt_', 'var') == 1
  save('twoagents_results.mat', 'bayestopt_', '-append');
end
if exist('dataset_', 'var') == 1
  save('twoagents_results.mat', 'dataset_', '-append');
end
if exist('estimation_info', 'var') == 1
  save('twoagents_results.mat', 'estimation_info', '-append');
end


disp(['Total computing time : ' dynsec2hms(toc) ]);
if ~isempty(lastwarn)
  disp('Note: warning(s) encountered in MATLAB/Octave code')
end
diary off
