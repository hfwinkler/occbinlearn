% solve CCHH model with global projection method
% and exogenous price

%% parameters

%model parameters
betta=1.04^-0.25;
R=1.03^0.25;
theta=0.5;

Ybar=1;
Qbar=100;
rhoq=0.99;
sigq=0.01;

%algorithm parameters
minC=1e-6;
minH=1e-6;
convcrit=1e-5;
lambda=0.1;
plotfreq=10;

%% non-stochastic steady-state

Hss=Ybar/Qbar/(1-betta-(1-betta*R)*theta+(R-1)*theta);
Css=Qbar*Hss*(1-betta-(1-betta*R)*theta);
Bss=theta*Qbar*Hss;
Wss=Qbar*Hss*(1-R*theta);

%% set up grid and nodes

%price grid
qmin=log(Qbar)-3*sigq/sqrt(1-rhoq^2);
qmax=log(Qbar)+3*sigq/sqrt(1-rhoq^2);
Kq=12;
grid.onlyq=linspace(qmin,qmax,Kq);

%wealth grid
Wmin=0;
Wmax=5*Wss;
KW=50;
grid.onlyW=linspace(Wmin,Wmax,KW);

%overall grid
[grid.q, grid.W]=ndgrid(grid.onlyq,grid.onlyW);
grid.length=Kq*KW;
fprintf('Nodes: %i\n',grid.length);

%number of quadrature nodes
J=3;
%calculate Gauss-Hermite nodes and weights
CM      = diag(sqrt((1:(J-1))/2),1) + diag(sqrt((1:(J-1))/2),-1);
[V, L]   = eig(CM);
[zeta, ind] = sort(diag(L));
V       = V(:,ind)';
w       = V(:,1).^2; %don't multiply by sqrt(pi) here

%for two shocks
%[zzeta1, zzeta2]=ndgrid(zeta,zeta);
%ww=w*w';

%% initial guess for policy function

C0=Ybar*ones(size(grid.q)); 
H0=grid.W./exp(grid.q)/(1-R*theta);

Cold=C0;
Cnew=C0;
Hold=H0;
Hnew=H0;

Bnew=ones(size(grid.q));
Xnew=ones(size(grid.q));
troubleshooter=zeros(size(grid.q));

%% iterate

maxiter=1e4;
h=figure;
for n=1:maxiter
    %walk through grid
    FC=griddedInterpolant(grid.q,grid.W,Cold,'linear','linear');
    for node=1:grid.length %can be replaced by parfor if grid is large
        %get grid points
        Q=exp(grid.q(node));
        W=grid.W(node);
        if Ybar+W<0
            %infeasible point - sell all your H and still have negative C
            Cnew(node)=minC;
            Hnew(node)=minH;
            Bnew(node)=theta*Q*minH;
            continue;
        end
        %current period:
        Ctemp=Cold(node); 
        Htemp=Hold(node);
        Btemp=Ctemp-Ybar-W+Q*Htemp;
        Btemp=Btemp*ones(J,1);
        Htemp=Htemp*ones(J,1);
        %next period:
        q1=rhoq*log(Q)+(1-rhoq)*log(Qbar)+sigq*zeta;
        W1=exp(q1).*Htemp-R*Btemp;
        C1=FC(q1,W1); %#ok<*PFBNS>
        C1=max(C1,minC); %if future consumption negative, set MU very high
        %take expectation with quadrature
        integrand1=1./C1;
        k1=sum(integrand1.*w);
        integrand2=exp(q1)./C1;
        k2=sum(integrand2.*w);
        %get new solution: try unconstrained first
        C=1/(betta*R*k1);
        H=C/(Q-betta*C*k2);
        B=C-Ybar-W+Q*H;
        if B>theta*Q*H || H<0 %if borrowing constraint violated, try constrained
            k3=Ybar+W;
            k4=theta*betta*R*k1-betta/Q*k2;
            C=1/(2*k4)*(k3*k4-2*(1-theta)+sqrt(k3^2*k4^2+4*(1-theta)^2));
            H=(Ybar-C+W)/Q/(1-theta);
            if H<0
                H=minH;
                C=Ybar+W-(1-theta)*Q*H;
            end
            B=theta*Q*H;
        end
        troubleshooter(node)=1/C-betta*R*k1;
        C=max(C,minC);
        H=max(H,minH);
        if H<0 || isnan(H) || isinf(H)
            error('Something wrong with H');
        end
        Cnew(node)=C;
        Hnew(node)=H;
        Bnew(node)=B;
        Xnew(node)=B/(theta*Q*H);
    end
    
    %measure distance to old policy function and stop if it's small
    dist=max(abs(Cnew(:)-Cold(:)));
    if dist<convcrit
        fprintf('#%i: distance: %3.3g\n',n,dist);
        break; 
    end
    %update policy function with attenuation
    Cold=(1-lambda)*Cnew+lambda*Cold;
    Hold=(1-lambda)*Hnew+lambda*Hold;
    %save cchhtemp;
    
    if mod(n,plotfreq)==0 %plot policy function
        fprintf('#%i: distance: %3.3g\n',n,dist);
        surf(grid.onlyq,grid.onlyW,Cnew'); 
        xlabel('q'); xlim([qmin qmax]);
        ylabel('W'); ylim([Wmin Wmax]);
        drawnow;
    end
end
close(h);

%% plot

figure; 
surf(grid.onlyq,grid.onlyW,Hnew'); 
xlabel('q'); xlim([qmin qmax]);
ylabel('W'); ylim([Wmin Wmax]);
zlabel('H');
title('housing');
figure;
surf(grid.onlyq,grid.onlyW,Bnew'); 
xlabel('q'); xlim([qmin qmax]);
ylabel('W'); ylim([Wmin Wmax]);
zlabel('B');
title('debt');
figure;
surf(grid.onlyq,grid.onlyW,Xnew'); 
xlabel('q'); xlim([qmin qmax]);
ylabel('W'); ylim([Wmin Wmax]);
zlabel('X');
title('debt as percent of constraint');
figure;
surf(grid.onlyq,grid.onlyW,Cnew'); 
xlabel('q'); xlim([qmin qmax]);
ylabel('W'); ylim([Wmin Wmax]);
zlabel('C');
title('consumption');

%% simulation

T=1e3;
rng(10);
qsim=zeros(1,T);
Hsim=zeros(1,T);
Xsim=zeros(1,T);
Wsim=zeros(1,T);
Bsim=zeros(1,T);
Csim=zeros(1,T);

Blag=Bss;
Hlag=Hss;
qlag=log(Qbar);

FC=griddedInterpolant(grid.q,grid.W,Cold,'linear','linear');
FH=griddedInterpolant(grid.q,grid.W,Hold,'linear','linear');
    
for t=1:T
    qsim(t)=rhoq*qlag+(1-rhoq)*log(Qbar)+sigq*randn(1);
    Wsim(t)=exp(qsim(t))*Hlag-R*Blag;
    Csim(t)=FC(qsim(t),Wsim(t));
    Hsim(t)=FH(qsim(t),Wsim(t));
    Bsim(t)=Csim(t)-Ybar-Wsim(t)+exp(qsim(t))*Hsim(t);
    if Bsim(t)>theta*exp(qsim(t))*Hsim(t)
        Bsim(t)=theta*exp(qsim(t))*Hsim(t);
        Csim(t)=Ybar+Wsim(t)-(1-theta)*exp(qsim(t))*Hsim(t);
    end
    Xsim(t)=Bsim(t)/(theta*exp(qsim(t))*Hsim(t));
    qlag=qsim(t);
    Blag=Bsim(t);
    Hlag=Hsim(t);
end

figure;
subplot(2,3,1); plot(1:T,qsim-log(Qbar)); title('house price, log dev. from SS');
subplot(2,3,2); plot(1:T,log(Hsim/Hss)); title('housing, log dev from SS');
subplot(2,3,3); plot(1:T,Xsim,1:T,ones(1,T)); title('debt as a fraction of debt limit');
subplot(2,3,4); plot(1:T,log(Bsim/Bss)); title('debt, log dev from SS');
subplot(2,3,5); plot(1:T,log(Csim/Css)); title('consumption, log dev from SS');
subplot(2,3,6); plot(1:T,log(Wsim/Wss)); title('wealth, log dev from SS');

fprintf('Average debt level:\t%3.6g \nAverage house price\t%3.6g\n',mean(Bsim(T/2:T)),mean(exp(qsim(T/2:T))));
