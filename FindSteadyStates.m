% find which parameters support which steady state

% the first model name is the constrained one, the second unconstrained
mnames={'largehh_closed_Hprod' 'largehh_closeduncon_Hprod'};

N=31; %gridsize
parnames={'chi','theta'};
parmins=[0.01,0.01];
parmaxs=[0.99,0.99];

[grid1,grid2]=ndgrid(linspace(parmins(1),parmaxs(1),N),linspace(parmins(2),parmaxs(2),N));

%% run Dynare for every model, compute and store steady states

for i=1:length(mnames)
    model=mnames{i};
    clear M_ oo_;
    fprintf('Running Dynare...');
    evalc('dynare(model,''noclearall'');');
    
    for j=1:M_.endo_nbr
        jdx.(deblank(M_.endo_names(j,:)))=j;
    end

    SS.(model)=nan(M_.endo_nbr,N^2);
    params=nan(M_.param_nbr,N^2);
    
    options_.noprint=1;
    options_.irf=0;
    options_.ar=0;
    options_.nomoments=0;
    options_.periods=0;
    
    fprintf('looping...');
    for n=[1 1:N^2] %why the repeat is necessary I don't know, but it is!
        %set parameter to loop over
        set_param_value(parnames{1},grid1(n));
        set_param_value(parnames{2},grid2(n));
        params(:,n)=M_.params;
        
        switch model
        case 'largehh_closed'
            oo_.steady_state( 6 ) = 0;
            oo_.steady_state( 2 ) = 0;
            oo_.steady_state( 5 ) = 0;
            oo_.steady_state( 9 ) = 1/(1-M_.params(6)-M_.params(1)*M_.params(5))*((-(1-M_.params(1)))+(1-M_.params(6)-M_.params(5))/M_.params(5));
            oo_.steady_state( 8 ) = 1/(1+M_.params(5)*oo_.steady_state(9))/M_.params(1)-1;
            oo_.steady_state( 3 ) = (-1)/(1+M_.params(2))*log(M_.params(3)/(1+M_.params(5)*oo_.steady_state(9)));
            oo_.steady_state( 1 ) = oo_.steady_state(6)+oo_.steady_state(3);
            oo_.steady_state( 7 ) = oo_.steady_state(1)+log(M_.params(5)/(1-M_.params(6)-M_.params(5)));
            oo_.steady_state( 4 ) = M_.params(6)*exp(oo_.steady_state(7)+oo_.steady_state(2));
            oo_.steady_state( 10 ) = 0;
        case 'largehh_closeduncon'
            oo_.steady_state( 6 ) = 0;
            oo_.steady_state( 2 ) = 0;
            oo_.steady_state( 5 ) = 0;
            oo_.steady_state( 9 ) = 0;
            oo_.steady_state( 8 ) = 1/M_.params(1)-1;
            oo_.steady_state( 3 ) = (-1)/(1+M_.params(2))*log(M_.params(3));
            oo_.steady_state( 1 ) = oo_.steady_state(6)+oo_.steady_state(3);
            oo_.steady_state( 7 ) = oo_.steady_state(1)-oo_.steady_state(2)-log(1-M_.params(1));
            oo_.steady_state( 4 ) = exp(oo_.steady_state(2)+oo_.steady_state(7))-M_.params(5)*(exp(oo_.steady_state(2)+oo_.steady_state(7))+exp(oo_.steady_state(6)+oo_.steady_state(3)));
            oo_.steady_state( 10 ) = exp(oo_.steady_state(2)+oo_.steady_state(7))*M_.params(6)-(M_.params(5)*oo_.steady_state(5)+oo_.steady_state(4));
        case 'largehh_closed_Hprod'
            oo_.steady_state( 6 ) = 0;
            oo_.steady_state( 5 ) = 0;
            oo_.steady_state( 7 ) = 0;
            oo_.steady_state( 12 ) = 0;
            oo_.steady_state( 9 ) = (1-M_.params(6)-M_.params(5)*(1+M_.params(10)+(1-M_.params(1))*(1-M_.params(10))))/M_.params(5)/(1-M_.params(6)-(1-M_.params(10))*M_.params(5)*M_.params(1));
            oo_.steady_state( 1 ) = 1/(1+M_.params(2))*((-log(M_.params(3)))+log(1+M_.params(5)*oo_.steady_state(9)))-M_.params(2)/(1+M_.params(2))*log(1+M_.params(5)*M_.params(10)/(1-M_.params(6)-M_.params(5)));
            oo_.steady_state( 2 ) = oo_.steady_state(1)+log(M_.params(5))-log(1-M_.params(6)-M_.params(5));
            oo_.steady_state( 3 ) = ((-log(M_.params(3)))+log(1+M_.params(5)*oo_.steady_state(9)))*1/M_.params(2)-oo_.steady_state(1)*1/M_.params(2);
            oo_.steady_state( 11 ) = log(M_.params(10))+oo_.steady_state(2);
            oo_.steady_state( 8 ) = 1/(1+M_.params(5)*oo_.steady_state(9))/M_.params(1)-1;
            oo_.steady_state( 4 ) = M_.params(6)*exp(oo_.steady_state(2)+oo_.steady_state(7));
            oo_.steady_state( 10 ) = 0;
        case 'largehh_closeduncon_Hprod'
            oo_.steady_state( 6 ) = 0;
            oo_.steady_state( 5 ) = 0;
            oo_.steady_state( 7 ) = 0;
            oo_.steady_state( 12 ) = 0;
            oo_.steady_state( 9 ) = 0;
            oo_.steady_state( 1 ) = (-1)/(1+M_.params(2))*log(M_.params(3))-M_.params(2)/(1+M_.params(2))*log(1+M_.params(10)/(1-M_.params(1)*(1-M_.params(10))));
            oo_.steady_state( 2 ) = oo_.steady_state(1)-log(1-M_.params(1)*(1-M_.params(10)));
            oo_.steady_state( 3 ) = (-1)/M_.params(2)*(log(M_.params(3))+oo_.steady_state(1));
            oo_.steady_state( 11 ) = log(M_.params(10))+oo_.steady_state(2);
            oo_.steady_state( 8 ) = 1/M_.params(1)-1;
            oo_.steady_state( 4 ) = exp(oo_.steady_state(2))-M_.params(5)*(exp(oo_.steady_state(6)+oo_.steady_state(3))+(1-M_.params(10))*exp(oo_.steady_state(2)));
            oo_.steady_state( 10 ) = 1-(M_.params(5)*oo_.steady_state(5)+oo_.steady_state(4))/(M_.params(6)*exp(oo_.steady_state(2)+oo_.steady_state(7)));
        end
        
        try
            %info=stoch_simul('');
            evalc('[ys,~,info]=evaluate_steady_state(oo_.steady_state,M_,options_,oo_,1)');
        catch
            info=1;
        end
        if ~info
            SS.(model)(:,n)=ys;
        end
        
    end
    
    fprintf('done.\n');
    cleanup_dyn(model);
    
end

%% show results

figure;

ConstrainedValid1=grid1(SS.(mnames{1})(jdx.lambda,:)>0);
ConstrainedValid2=grid2(SS.(mnames{1})(jdx.lambda,:)>0);
scatter(ConstrainedValid1,ConstrainedValid2,'xr');
hold on;
UnconstrainedValid1=grid1(SS.(mnames{2})(jdx.disttoconstraint,:)>0);
UnconstrainedValid2=grid2(SS.(mnames{2})(jdx.disttoconstraint,:)>0);
scatter(UnconstrainedValid1,UnconstrainedValid2,'b');

xlabel(parnames{1}); ylabel(parnames{2}); legend('Constrained','Unconstrained');
title('Existence of steady state');

figure;
plotdata=SS.(mnames{1})(jdx.Q,:);
plotdata(SS.(mnames{1})(jdx.lambda,:)<=0)=nan;
surf(grid1,grid2,reshape(plotdata,[N N]));
hold on;
plotdata=SS.(mnames{2})(jdx.Q,:);
plotdata(SS.(mnames{2})(jdx.disttoconstraint,:)<=0)=nan;
mesh(grid1,grid2,reshape(plotdata,[N N]));
title('Steady-state Q');

figure;
plotdata=SS.(mnames{1})(jdx.H,:);
plotdata(SS.(mnames{1})(jdx.lambda,:)<=0)=nan;
surf(grid1,grid2,reshape(plotdata,[N N]));
hold on;
plotdata=SS.(mnames{2})(jdx.H,:);
plotdata(SS.(mnames{2})(jdx.disttoconstraint,:)<=0)=nan;
mesh(grid1,grid2,reshape(plotdata,[N N]));
title('Steady-state H');

figure;
plotdata=SS.(mnames{1})(jdx.C,:);
plotdata(SS.(mnames{1})(jdx.lambda,:)<=0)=nan;
surf(grid1,grid2,reshape(plotdata,[N N]));
hold on;
plotdata=SS.(mnames{2})(jdx.C,:);
plotdata(SS.(mnames{2})(jdx.disttoconstraint,:)<=0)=nan;
mesh(grid1,grid2,reshape(plotdata,[N N]));
title('Steady-state C');

figure;
plotdata=SS.(mnames{1})(jdx.N,:);
plotdata(SS.(mnames{1})(jdx.lambda,:)<=0)=nan;
surf(grid1,grid2,reshape(plotdata,[N N]));
hold on;
plotdata=SS.(mnames{2})(jdx.N,:);
plotdata(SS.(mnames{2})(jdx.disttoconstraint,:)<=0)=nan;
mesh(grid1,grid2,reshape(plotdata,[N N]));
title('Steady-state N');
