% Dynare file to simulate large household model
% with the constraint never binding
% closed economy version
% with learning

var C H N b B;
var W Q r;
var lambda;
var disttoconstraint;

varexo epsw;

parameters betta nu omega phi;
parameters chi theta;
parameters sigmaw rhow;

%additional learning:
var mu, lagv, auxv;
varexo v, v1;
parameters g, sigmav;

@#include "largehh_parameters.m"

%shorthand notation:

model;

%borrower BC
exp(Q+H) = b + chi*(exp(W+N)+exp(Q+H(-1))+B-(1+r(-1))*B(-1));
%aggregate BC
exp(C) = exp(W+N)+B-(1+r(-1))*B(-1);
%borrowing constraint (removed)
%chi*B+b = theta*exp(Q+H);
disttoconstraint=1-(chi*B+b)/(theta*exp(Q+H));
%borrower FOC for housing:
exp(Q-phi*C)*(1+(1-theta)*lambda) = exp(-H) + betta*exp(Q(+1))*exp(-phi*C(+1))*(1+chi*lambda(+1));
%lender FOC for loans:
exp(-phi*C) = betta*(1+r)*exp(-phi*C(+1))*(1+chi*lambda(+1));
%labor FOC:
omega*exp(nu*N)=exp(W-phi*C)*(1+chi*lambda);

%labor demand
W=rhow*W(-1)+sigmaw*epsw;
%housing supply
%H=0;
%aggregate loan supply
B=0;

%constraint not binding
lambda=0;

%learning
Q=Q(-1)+mu(-1)+sigmav*v-0.5*(sigmav*auxv(+1))^2; 
mu=mu(-1)+g*(sigmav*v1-0.5*(sigmav*auxv(+1))^2);
auxv=v;
lagv=auxv(-1);

end;

shocks;
var epsw; stderr 1;
end;

%load steady state from RE (compute it first!!)
load largehh_closeduncon_SS;
oo_.steady_state(1:length(ys_RE))=ys_RE;

%stoch_simul(order=1,irf=20,nocorr,ar=0);
stoch_simul(order=1,irf=100,nomoments,nograph);
cleanup_dyn(M_.fname);

