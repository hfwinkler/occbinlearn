% solve CCHH model with global projection method
% learning version

clear;
%% parameters

%model parameters
betta=1.04^-0.25;
R=1.03^0.25;
theta=0.8;

Ybar=1;
rhoy=0.9;
sigy=1e-2;

Hbar=1;

g=0.0003;
sigq=0.02;
rhomu=.99;

%algorithm parameters
minC=1e-6;
minH=1e-6;
convcrit=1e-5;
lambda=0;
plotfreq=10;

%% non-stochastic steady-state

Css=1/(1-theta*(1-R)/(1-(1-betta*R)*theta-betta))*Ybar;
Qss=1/(1-(1-betta*R)*theta-betta)*Css;
Bss=theta*Qss;
Wss=Qss*Hbar-R*Bss;

%% set up grid and nodes

%price grid
qmin=log(Qss*0.01);
qmax=log(Qss*10);
Kq=21;
grid.onlyq=linspace(qmin,qmax,Kq);

%belief grid
mumin=-.001;
mumax=.001;
Kmu=15;
grid.onlymu=linspace(mumin,mumax,Kmu);

%debt grid (for PLM we use wealth, but for PLM we use debt as the state)
Bmin=Bss*0.1;
Bmax=Bss*0.6;
KB=15;
grid.onlyB=linspace(Bmin,Bmax,KB);

%wealth grid set to match up with what we have
Wmin=exp(qmin)*Hbar-R*Bmax*1.1;
Wmax=exp(qmax)*Hbar-R*Bmin;
KW=KB;
grid.onlyW=linspace(Wmin,Wmax,KW);

%income grid
ymin=log(Ybar)-3*sigy/sqrt(1-rhoy^2);
ymax=log(Ybar)+3*sigy/sqrt(1-rhoy^2);
Ky=6;
grid.onlyy=linspace(ymin,ymax,Ky);

%overall grid
[grid.q, grid.mu, grid.W, grid.y]=ndgrid(grid.onlyq,grid.onlymu,grid.onlyW,grid.onlyy);
grid.length=Kq*Kmu*KW*Ky;
fprintf('Nodes: %i\n',grid.length);

%number of quadrature nodes
J=3;
%calculate Gauss-Hermite nodes and weights
CM      = diag(sqrt((1:(J-1))/2),1) + diag(sqrt((1:(J-1))/2),-1);
[V, L]   = eig(CM);
[zeta, ind] = sort(diag(L));
V       = V(:,ind)';
w       = V(:,1).^2; %don't multiply by sqrt(pi) here

%for two shocks
[zzeta1, zzeta2]=ndgrid(zeta,zeta);
ww=w*w';

%for three shocks
[zzzeta1, zzzeta2, zzzeta3]=ndgrid(zeta,zeta,zeta);
www=reshape(kron(w,ww),[J J J]);

%% initial guess for PLM

%wild guess:
Cold=exp(grid.y).*ones(size(grid.q)); 
Hold=grid.W./exp(grid.q)/(1-R*theta);

%or load from file:
%load PLMsave;

Cnew=Cold;
Hnew=Hold;

Bnew=ones(size(grid.q));
Xnew=ones(size(grid.q));
troubleshooter=zeros(size(grid.q));

%% iterate

maxiter=1e4;
h=figure;
for n=1:maxiter
    %walk through grid
    Fc=griddedInterpolant(grid.q,grid.mu,grid.W,grid.y,log(Cold),'linear','linear');
    parfor node=1:grid.length %can be replaced by parfor if grid is large
        %get grid points
        Y=exp(grid.y(node));
        Q=exp(grid.q(node));
        mu=grid.mu(node);
        W=grid.W(node);
        if Y+W<=0
            %infeasible point - sell all your H and still have negative C
            Cnew(node)=minC;
            Hnew(node)=minH;
            Bnew(node)=theta*Q*minH;
            continue;
        end
        %current period:
        Ctemp=Cold(node); 
        Htemp=Hold(node);
        Btemp=Ctemp-Y-W+Q*Htemp;
        %next period:
        y1=rhoy*grid.y(node)+(1-rhoy)*log(Ybar)+sigy*zzzeta1;
        q1=log(Q)+mu+sigq*zzzeta2;
        mu1=rhomu*mu+g*sigq*zzzeta3;
        W1=exp(q1)*Htemp-R*Btemp;
        %prevent falling off the wealth grid?
        %W1=max(min(W1,Wmax),Wmin);
        C1=exp(Fc(q1,mu1,W1,y1)); %#ok<*PFBNS>
        C1=max(C1,minC); %if future consumption negative, set MU very high
        %take expectation with quadrature
        integrand1=1./C1;
        k1=integrand1(:)'*www(:);
        integrand2=exp(q1)./C1;
        k2=integrand2(:)'*www(:);
        %get new solution: try unconstrained first
        C=1/(betta*R*k1);
        H=C/(Q-betta*C*k2);
        B=C-Y-W+Q*H;
        if B>theta*Q*H || H<0 %if borrowing constraint violated, try constrained
            k3=Y+W;
            k4=theta*betta*R*k1-betta/Q*k2;
            C=1/(2*k4)*(k3*k4-2*(1-theta)+sqrt(k3^2*k4^2+4*(1-theta)^2));
            troubleshooter(node)=2;
            H=(Y-C+W)/Q/(1-theta);
            if H<0
                H=minH;
                C=Y+W-(1-theta)*Q*H;
                troubleshooter(node)=1;
            end
            B=theta*Q*H;            
        else
            troubleshooter(node)=0;
        end

        if H<0 || isnan(H) || isinf(H) || ~isreal(H)
            error('Something wrong with H');
        end
        Cnew(node)=max(C,minC);
        Hnew(node)=max(H,minH);
        Bnew(node)=B;
        Xnew(node)=B/(theta*Q*H);
    end
    
    %measure distance to old policy function and stop if it's small
    dist=max(abs(Hnew(:)-Hold(:)));
    if dist<convcrit
        fprintf('#%i: distance: %3.3g\n',n,dist);
        break; 
    end
    %update policy function with attenuation
    Cold=(1-lambda)*Cnew+lambda*Cold;
    Hold=(1-lambda)*Hnew+lambda*Hold;
    %save cchhtemp;
    
    if mod(n,plotfreq)==0 %plot policy function
        fprintf('#%i: distance: %3.3g\n',n,dist);
        subplot(1,2,1);
        surf(grid.onlyq,grid.onlymu,Hnew(:,:,round(KW/2),round(Ky/2))'); 
        xlabel('q'); xlim([qmin qmax]);
        ylabel('\mu'); ylim([mumin mumax]);
        zlabel('H');
        subplot(1,2,2);
        surf(grid.onlyW,grid.onlyy,squeeze(Hnew(round(Kq/2),round(Kmu/2),:,:))'); 
        xlabel('W'); xlim([Wmin Wmax]);
        ylabel('y'); ylim([ymin ymax]);
        zlabel('H');
        drawnow;
    end
end
close(h);

save PLMsave Hold Cold;

%% plot PLM

figure; 
subplot(1,2,1);
surf(grid.onlyq,grid.onlymu,Hnew(:,:,round(KW/2),round(Ky/2))'); 
hold on;
surf(grid.onlyq,grid.onlymu,Hbar*ones(Kmu,Kq),'FaceAlpha',0.33);
xlabel('q'); xlim([qmin qmax]);
ylabel('\mu'); ylim([mumin mumax]);
zlabel('H');
subplot(1,2,2);
surf(grid.onlyW,grid.onlyy,squeeze(Hnew(round(Kq/2),round(Kmu/2),:,:))'); 
xlabel('W'); xlim([Wmin Wmax]);
ylabel('y'); ylim([ymin ymax]);
zlabel('H');

figure; 
subplot(1,2,1);
surf(grid.onlyq,grid.onlymu,Cnew(:,:,round(KW/2),round(Ky/2))'); 
xlabel('q'); xlim([qmin qmax]);
ylabel('\mu'); ylim([mumin mumax]);
zlabel('C');
subplot(1,2,2);
surf(grid.onlyW,grid.onlyy,squeeze(Cnew(round(Kq/2),round(Kmu/2),:,:))'); 
xlabel('W'); xlim([Wmin Wmax]);
ylabel('y'); ylim([ymin ymax]);
zlabel('C');

figure; 
subplot(1,2,1);
surf(grid.onlyq,grid.onlymu,Xnew(:,:,round(KW/2),round(Ky/2))'); 
xlabel('q'); xlim([qmin qmax]);
ylabel('\mu'); ylim([mumin mumax]);
zlabel('X');
subplot(1,2,2);
surf(grid.onlyW,grid.onlyy,squeeze(Xnew(round(Kq/2),round(Kmu/2),:,:))'); 
xlabel('W'); xlim([Wmin Wmax]);
ylabel('y'); ylim([ymin ymax]);
zlabel('X');

%% solve for ALM

%store PLM
CPLM=Cnew;
HPLM=Hnew;
BPLM=Bnew;

%set up grid:

%switch to a debt grid, because W is endogenous now
gridALM.onlymu=grid.onlymu;
gridALM.onlyB=grid.onlyB;
gridALM.onlyy=grid.onlyy;
[gridALM.mu, gridALM.B, gridALM.y]=ndgrid(gridALM.onlymu,gridALM.onlyB,gridALM.onlyy);
gridALM.length=Kmu*KB*Ky;

qALM=zeros(size(gridALM.mu));
CALM=zeros(size(gridALM.mu));
BALM=zeros(size(gridALM.mu));
XALM=zeros(size(gridALM.mu));

FC=griddedInterpolant(grid.q,grid.mu,grid.W,grid.y,CPLM,'spline','nearest');
FH=griddedInterpolant(grid.q,grid.mu,grid.W,grid.y,HPLM,'spline','nearest');
% 
% %error checking figures
% Wtest=exp(grid.q)*Hbar-R*grid.B;
% Htest=FH(grid.q,grid.mu,Wtest);
% figure;
% surf(grid.onlyq,grid.onlymu,Htest(:,:,round(KB/2+8))'); 
% hold on;
% surf(grid.onlyq,grid.onlymu,Hbar*ones(Kmu,Kq),'FaceAlpha',0.33);
% xlabel('q'); xlim([qmin qmax]);
% ylabel('\mu'); ylim([mumin mumax]);
% zlabel('H');
% title('housing');
% figure; 
% surf(grid.onlyq,grid.onlyB,squeeze(Htest(:,round(Kmu/2),:))'); 
% hold on;
% surf(grid.onlyq,grid.onlyB,Hbar*ones(KB,Kq),'FaceAlpha',0.33);
% xlabel('q'); xlim([qmin qmax]);
% ylabel('B'); ylim([Bmin Bmax]);
% zlabel('H');
% title('housing');

troubleshooter=zeros(size(gridALM.mu));
for node=1:gridALM.length
    y=gridALM.y(node);
    mu=gridALM.mu(node);
    Bminus=gridALM.B(node);
    fun=@(q) FH(q,mu,exp(q)*Hbar-R*Bminus,y)-Hbar;
    Hlowq=fun(qmin);
    Hhighq=fun(qmax);
    if Hlowq>0&&Hhighq>0
        Q=exp(qmax);
        troubleshooter(node)=1;
%         plot(grid.onlyq,arrayfun(fun,grid.onlyq));
%         keyboard;
    elseif Hlowq<0&&Hhighq<0
        Q=exp(qmin);
        troubleshooter(node)=-1;
%         plot(grid.onlyq,arrayfun(fun,grid.onlyq));
%         keyboard;
    else
        [q,~,exitflag]=fzero(fun,qmax);
        troubleshooter(node)=0;
        if exitflag<0; 
        	[q,~,exitflag]=fzero(fun,[qmin qmax]);
            troubleshooter(node)=-0.5;
        end
        Q=exp(q);
    end
    C=FC(log(Q),mu,Q*Hbar-R*Bminus,y);
    B=C-exp(y)+R*Bminus;
    if B>(theta*Q*Hbar)
        B=theta*Q*Hbar;
        C=exp(y)+B-R*Bminus;
    end
    qALM(node)=log(Q);
    CALM(node)=max(C,minC);
    BALM(node)=B;
    XALM(node)=B/(theta*Q*Hbar);
end

%% plot ALM

figure; 
surf(gridALM.onlymu,gridALM.onlyB,qALM(:,:,round(Ky/2))'); 
xlabel('\mu'); ylim([mumin mumax]);
ylabel('B'); ylim([Bmin Bmax]);
zlabel('log(Q)');
title('house price (ALM)');

figure;
surf(gridALM.onlymu,gridALM.onlyB,BALM(:,:,round(Ky/2))'); 
xlabel('\mu'); ylim([mumin mumax]);
ylabel('B'); ylim([Bmin Bmax]);
zlabel('B');
title('debt (ALM)');

figure;
surf(gridALM.onlymu,gridALM.onlyB,XALM(:,:,round(Ky/2))'); 
xlabel('\mu'); ylim([mumin mumax]);
ylabel('B'); ylim([Bmin Bmax]);
zlabel('X');
title('debt as percent of constraint (ALM)');

figure;
surf(gridALM.onlymu,gridALM.onlyB,CALM(:,:,round(Ky/2))'); 
xlabel('\mu'); ylim([mumin mumax]);
ylabel('B'); ylim([Bmin Bmax]);
zlabel('C');
title('consumption (ALM)');

figure; 
surf(gridALM.onlymu,gridALM.onlyB,troubleshooter(:,:,round(Ky/2))'); 
xlabel('\mu'); ylim([mumin mumax]);
ylabel('B'); ylim([Bmin Bmax]);
title('DIAGNOSTIC');

%% simulation

rng(10);

T=1e2;
Tpre=1e3;
Qsim=zeros(1,T);
musim=zeros(1,T);
Xsim=zeros(1,T);
Wsim=zeros(1,T);
Bsim=zeros(1,T);
Csim=zeros(1,T);
Ysim=zeros(1,T);

FC=griddedInterpolant(gridALM.mu,gridALM.B,gridALM.y,CALM,'linear','linear');
Fq=griddedInterpolant(gridALM.mu,gridALM.B,gridALM.y,qALM,'linear','nearest');

Blag=0.2*Bss;
mulag=0;
Ylag=Ybar;
Qlag=exp(Fq(mulag,Blag,log(Ylag)));

    
for t=[ones(1,Tpre) 1:T]
    Ysim(t)=Ylag^rhoy*Ybar^(1-rhoy)*exp(sigy*randn(1)*(t>10));
    Csim(t)=FC(mulag,Blag,log(Ysim(t)));
    Qsim(t)=exp(Fq(mulag,Blag,log(Ysim(t))));
    Wsim(t)=Qsim(t)*Hbar-R*Blag;
    Bsim(t)=Csim(t)-Ysim(t)+R*Blag;
    if Bsim(t)>theta*Qsim(t)*Hbar
        if Ysim(t)+theta*Qsim(t)*Hbar-R*Blag<=0 %no consumption feasible
            %Csim(t)=nan;
            %Bsim(t)=nan;
            Csim(t)=-Css;
            Bsim(t)=theta*Qsim(t)*Hbar;
        else
        Bsim(t)=theta*Qsim(t)*Hbar;
        Csim(t)=Ysim(t)+Bsim(t)-R*Blag;
        end
    end
    musim(t)=(1-g)*mulag+g*log(Qsim(t)/Qlag)*(t>1);
    Xsim(t)=Bsim(t)/(theta*Qsim(t)*Hbar);
    Blag=max(min(Bsim(t),Bmax),Bmin);
    mulag=musim(t);
    Qlag=Qsim(t);
    Ylag=Ysim(t);
end

figure;
subplot(2,3,1); plot(1:T,Qsim/Qss); title('house price, fraction of SS');
subplot(2,3,2); plot(1:T,Ysim/Ybar); title('income, fraction of SS');
subplot(2,3,3); plot(1:T,Xsim); title('debt as a fraction of debt limit');
subplot(2,3,4); plot(1:T,musim); title('belief');
subplot(2,3,5); plot(1:T,Csim/Css); title('consumption, fraction of SS');
subplot(2,3,6); plot(1:T,Bsim/Bss); title('debt, fraction of SS');
