
%household
betta=0.99;
nu=.33;
omega=1;
phi=2;
chi=0.5;
theta=0.3;

%exogenous wage process
sigmaw=0.01;
rhow=0.9;

% for open economy version
rwedge=0.995;

% for housing production
psi=10;
deltaH=0.025;

%learning
g=0.01;
sigmav=0.01;
