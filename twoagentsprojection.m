% solve twoagents model with global projection method


%% parameters

%model parameters from file
twoagents_parameters;

%algorithm parameters
minC=1e-6;
convcrit=2e-5;
lambda=0;
plotfreq=1;
loadprevious=0;

%% steady state from Dynare calculation (run it first!)
load twoagents_SS;
for i=1:M_RE.endo_nbr
    SS.(deblank(M_RE.endo_names(i,:)))=oo_RE.dr.ys(i);
    jdx.(deblank(M_RE.endo_names(i,:)))=oo_RE.dr.inv_order_var(i);
end

%% set up grid and nodes

%wage grid
wmin=log(1)-3*sigmaw/sqrt(1-rhow^2);
wmax=log(1)+3*sigmaw/sqrt(1-rhow^2);
Kw=11;
grid.onlyw=linspace(wmin,wmax,Kw);

%debt grid
Bmin=SS.B*0.7;
Bmax=SS.B*1.3;
KB=41;
grid.onlyB=linspace(Bmin,Bmax,KB);

%borrower housing grid
Hbmin=max(exp(SS.Hb)-0.12,.01*Hbar);
Hbmax=min(exp(SS.Hb)+0.12,.99*Hbar);
KHb=41;
grid.onlyHb=linspace(Hbmin,Hbmax,KHb);

%overall grid
[grid.w, grid.B, grid.Hb]=ndgrid(grid.onlyw,grid.onlyB,grid.onlyHb);
grid.length=Kw*KB*KHb;
savedgrid=grid; %for saving to file
fprintf('Nodes: %i\n',grid.length);

%number of quadrature nodes
J=3;
%calculate Gauss-Hermite nodes and weights
CM      = diag(sqrt((1:(J-1))/2),1) + diag(sqrt((1:(J-1))/2),-1);
[V, L]   = eig(CM);
[zeta, ind] = sort(diag(L));
V       = V(:,ind)';
weights = V(:,1).^2; %don't multiply by sqrt(pi) here

%for two shocks
%[zzeta1, zzeta2]=ndgrid(zeta,zeta);
%ww=w*w';

%% initial guess for policy function:

%simple guess
Hb0=exp(SS.Hb)*ones(size(grid.w));
B0=SS.B*ones(size(grid.w));
Q0=exp(SS.Q)*ones(size(grid.w));
Cb0=(exp(SS.Cb)+SS.r*SS.B)*exp(grid.w)-SS.r*SS.B;
Cs0=(exp(SS.Cs)-SS.r*SS.B)*exp(grid.w)+SS.r*SS.B;

Csnew=Cs0;
Cbnew=Cb0;
Hbnew=Hb0;
Bnew=B0;
Qnew=Q0;

%load previous solution?
if loadprevious
    load twoagents_polfuns;
    %interpolate to new grid
    Csnew=interpn(savedgrid.w,savedgrid.B,savedgrid.Hb,Csnew,grid.w,grid.B,grid.Hb);
    Cbnew=interpn(savedgrid.w,savedgrid.B,savedgrid.Hb,Cbnew,grid.w,grid.B,grid.Hb);
    Hbnew=interpn(savedgrid.w,savedgrid.B,savedgrid.Hb,Hbnew,grid.w,grid.B,grid.Hb);
    Bnew=interpn(savedgrid.w,savedgrid.B,savedgrid.Hb,Bnew,grid.w,grid.B,grid.Hb);
    Qnew=interpn(savedgrid.w,savedgrid.B,savedgrid.Hb,Qnew,grid.w,grid.B,grid.Hb);
    savedgrid=grid; 
end

Csold=Csnew;
Cbold=Cbnew;
Hbold=Hbnew;
Bold=Bnew;
Qold=Qnew;

multiplier=zeros(size(grid.w));
Rnew=zeros(size(grid.w));

%% iterate

%helper functions
Ybfun=@(Cb,w) exp(w)*(exp(w)/eta*Cb^-gama)^(1/phi);
Ysfun=@(Cs,w) exp(w)*(exp(w)/etas*Cs^-gamas)^(1/phis);

maxiter=1e4;
options=optimset('Display','off');
h=figure;

for n=1:maxiter
    %walk through grid
    FCs=griddedInterpolant(grid.w,grid.B,grid.Hb,Csold,'linear','linear');
    FCb=griddedInterpolant(grid.w,grid.B,grid.Hb,Cbold,'linear','linear');
    FQ=griddedInterpolant(grid.w,grid.B,grid.Hb,Qold,'linear','linear');
    parfor node=1:grid.length %can be replaced by parfor if grid is large
        %get grid points
        w=grid.w(node);
        Bminus=grid.B(node);
        Hbminus=grid.Hb(node);
        Hsminus=Hbar-Hbminus;
        %current period:
        Btemp=Bold(node)*ones(J,1);
        Hbtemp=Hbold(node)*ones(J,1);
        %don't fall off the grid
%         Btemp=max(min(Btemp,Bmax),Bmin);
%         Hbtemp=max(min(Hbtemp,Hbmax),Hbmin);
        %next period:
        w1=rhow*w+sigmaw*zeta;
        Cs1=FCs(w1,Btemp,Hbtemp); %#ok<*PFBNS>
        Cb1=FCb(w1,Btemp,Hbtemp); %#ok<*PFBNS>
        Q1=FQ(w1,Btemp,Hbtemp); %#ok<*PFBNS>
        Cs1=max(Cs1,minC); %if future consumption negative, set MU very high
        Cb1=max(Cb1,minC); %if future consumption negative, set MU very high
        %take expectation with quadrature
        integrand1=Cs1.^-gamas;
        k1=sum(integrand1.*weights);
        integrand2=Q1.*Cs1.^-gamas;
        k2=sum(integrand2.*weights);
        integrand3=Cb1.^-gama;
        k3=sum(integrand3.*weights);
        integrand4=Q1.*Cb1.^-gama;
        k4=sum(integrand4.*weights);
        %set up solver for constrained and unconstrained case
        unconfun=@(cscbhb) [ ...
            exp(cscbhb(1))+exp(cscbhb(2)) - Ysfun(exp(cscbhb(1)),w) - Ybfun(exp(cscbhb(2)),w); ...
            bettas*exp(gamas*cscbhb(1))*k1 - betta*exp(gama*cscbhb(2))*k3; ...
            exp(gamas*cscbhb(1))*((Hbar-exp(cscbhb(3)))^-nus + bettas*k2) - exp(gama*cscbhb(2))*(chi*exp(-nu*cscbhb(3)) + betta*k4) ...
            ];
        confun=@(cscbhb) [ ...
                exp(cscbhb(1))+exp(cscbhb(2)) - Ysfun(exp(cscbhb(1)),w) - Ybfun(exp(cscbhb(2)),w); ...
                exp(gama*cscbhb(2))*(chi*exp(-nu*cscbhb(3)) + betta*k4) ...
                - (1-theta*(1-betta/bettas*exp(gama*cscbhb(2)-gamas*cscbhb(1))*k3/k1)) ...
                * exp(gamas*cscbhb(1))*((Hbar-exp(cscbhb(3)))^-nus + bettas*k2) ; ...
                -exp(cscbhb(2)) + Ybfun(exp(cscbhb(2)),w) - Bminus + (Hbminus-(1-theta)*exp(cscbhb(3)))*( exp(gamas*cscbhb(1))*((Hbar-exp(cscbhb(3)))^-nus + bettas*k2) ) ...
        ];  
        %get new solution
        if multiplier(node)<=0 %if previously unconstrained, try unconstrained first
            multiplier(node)=0;
            solution=fsolve(unconfun,log([Csold(node), Cbold(node), Hbold(node)]),options);
            Cs=exp(solution(1)); Cb=exp(solution(2)); Hb=exp(solution(3));
            Hs=Hbar-Hb;
            R=1/(bettas*Cs^gamas*k1);
            Q=Cs^gamas*(Hs^-nus + bettas*k2);
            B=(Cb-Ybfun(Cb,w)+Q*(Hb-Hbminus)+Bminus)*R;
            %if borrowing constraint violated, try constrained
            if B/R>theta*Q*Hb  
                solution=fsolve(confun,solution,options);
                Cs=exp(solution(1)); Cb=exp(solution(2)); Hb=exp(solution(3));
                Hs=Hbar-Hb;
                R=1/(bettas*Cs^gamas*k1);
                Q=Cs^gamas*(Hs^-nus + bettas*k2);
                B=theta*Q*Hb*R;
                multiplier(node)=1-betta*Cb^gama*k3*R;
            end
        else
            solution=fsolve(confun,log([Csold(node), Cbold(node), Hbold(node)]),options);
            Cs=exp(solution(1)); Cb=exp(solution(2)); Hb=exp(solution(3));
            Hs=Hbar-Hb;
            R=1/(bettas*Cs^gamas*k1);
            Q=Cs^gamas*(Hs^-nus + bettas*k2);
            B=theta*Q*Hb*R;
            multiplier(node)=1-betta*Cb^gama*k3*R;
            if multiplier(node)<0
                multiplier(node)=0;
                solution=fsolve(unconfun,solution,options);
                Cs=exp(solution(1)); Cb=exp(solution(2)); Hb=exp(solution(3));
                Hs=Hbar-Hb;
                R=1/(bettas*Cs^gamas*k1);
                Q=Cs^gamas*(Hs^-nus + bettas*k2);
                B=(Cb-Ybfun(Cb,w)+Q*(Hb-Hbminus)+Bminus)*R;
            end
        end
        %update policy functions
        Csnew(node)=Cs;
        Cbnew(node)=Cb;
        Hbnew(node)=Hb;
        Bnew(node)=B;
        Qnew(node)=Q;
        Rnew(node)=R;
    end
    
    %measure distance to old policy function and stop if it's small
    dist=max(abs(Cbnew(:)-Cbold(:)));
    if dist<convcrit
        fprintf('#%i: distance: %3.3g\n',n,dist);
        save twoagents_polfuns Csnew Cbnew Hbnew Bnew Qnew Rnew savedgrid;
        break; 
    end
    %update policy functions with attenuation
    Csold=(1-lambda)*Csnew+lambda*Csold;
    Cbold=(1-lambda)*Cbnew+lambda*Cbold;
    Hbold=(1-lambda)*Hbnew+lambda*Hbold;
    Bold=(1-lambda)*Bnew+lambda*Bold;
    Qold=(1-lambda)*Qnew+lambda*Qold;
    %save twoagents_temp Csnew Cbnew Hbnew Bnew Qnew Rnew savedgrid;
    
    if mod(n,plotfreq)==0 %plot policy function
        fprintf('#%i: distance: %3.3g\n',n,dist);
        
        subplot(1,2,1);
        surf(grid.onlyw,grid.onlyB,multiplier(:,:,round(KHb/2))'); 
        zlabel('\lambda');
        xlabel('w'); xlim([wmin wmax]);
        ylabel('B'); ylim([Bmin Bmax]);

        subplot(1,2,2);
        surf(grid.onlyB,grid.onlyHb,squeeze(multiplier(round(Kw/2),:,:))'); 
        zlabel('\lambda');
        xlabel('B'); xlim([Bmin Bmax]);
        ylabel('H_b'); ylim([Hbmin Hbmax]);
        
        drawnow;
    end
end
close(h);

%% plot

figure; 
subplot(1,2,1);
surf(grid.onlyw,grid.onlyB,Cbnew(:,:,round(KHb/2))'); 
zlabel('C_b');
xlabel('w'); xlim([wmin wmax]);
ylabel('B'); ylim([Bmin Bmax]);
title('borrower consumption');
subplot(1,2,2);
surf(grid.onlyB,grid.onlyHb,squeeze(Cbnew(round(Kw/2),:,:))'); 
zlabel('C_b');
xlabel('B'); xlim([Bmin Bmax]);
ylabel('H_b'); ylim([Hbmin Hbmax]);
title('borrower consumption');

figure; 
subplot(1,2,1);
surf(grid.onlyw,grid.onlyB,Qnew(:,:,round(KHb/2))'); 
zlabel('Q');
xlabel('w'); xlim([wmin wmax]);
ylabel('B'); ylim([Bmin Bmax]);
title('house price');
subplot(1,2,2);
surf(grid.onlyB,grid.onlyHb,squeeze(Qnew(round(Kw/2),:,:))'); 
zlabel('Q');
xlabel('B'); xlim([Bmin Bmax]);
ylabel('H_b'); ylim([Hbmin Hbmax]);
title('house price');

figure; 
subplot(1,2,1);
surf(grid.onlyw,grid.onlyB,Rnew(:,:,round(KHb/2))'); 
zlabel('R');
xlabel('w'); xlim([wmin wmax]);
ylabel('B'); ylim([Bmin Bmax]);
title('interest rate');
subplot(1,2,2);
surf(grid.onlyB,grid.onlyHb,squeeze(Rnew(round(Kw/2),:,:))'); 
zlabel('R');
xlabel('B'); xlim([Bmin Bmax]);
ylabel('H_b'); ylim([Hbmin Hbmax]);
title('interest rate');

figure; 
subplot(1,2,1);
surf(grid.onlyw,grid.onlyB,Bnew(:,:,round(KHb/2))'); 
zlabel('B');
xlabel('w'); xlim([wmin wmax]);
ylabel('B'); ylim([Bmin Bmax]);
title('debt');
subplot(1,2,2);
surf(grid.onlyB,grid.onlyHb,squeeze(Bnew(round(Kw/2),:,:))'); 
zlabel('B');
xlabel('B'); xlim([Bmin Bmax]);
ylabel('H_b'); ylim([Hbmin Hbmax]);
title('debt');

figure; 
subplot(1,2,1);
surf(grid.onlyw,grid.onlyB,multiplier(:,:,round(KHb/2))'); 
zlabel('\lambda');
xlabel('w'); xlim([wmin wmax]);
ylabel('B'); ylim([Bmin Bmax]);
title('multiplier');
subplot(1,2,2);
surf(grid.onlyB,grid.onlyHb,squeeze(multiplier(round(Kw/2),:,:))'); 
zlabel('\lambda');
xlabel('B'); xlim([Bmin Bmax]);
ylabel('H_b'); ylim([Hbmin Hbmax]);
title('multiplier');

%% simulation

T=600;
Tpre=2e3;
rng(10);

wsim=zeros(1,T);
Qsim=zeros(1,T);
multsim=zeros(1,T);
Bsim=zeros(1,T);
Cbsim=zeros(1,T);
Cssim=zeros(1,T);
Hbsim=zeros(1,T);
Rsim=zeros(1,T);

multsim=zeros(1,T);

Blag=SS.B*1;
Hblag=exp(SS.Hb);
wlag=0;

FCs=griddedInterpolant(grid.w,grid.B,grid.Hb,Csnew,'linear','linear');
FCb=griddedInterpolant(grid.w,grid.B,grid.Hb,Cbnew,'linear','linear');
FHb=griddedInterpolant(grid.w,grid.B,grid.Hb,Hbnew,'linear','linear');
FQ=griddedInterpolant(grid.w,grid.B,grid.Hb,Qnew,'linear','linear');
FR=griddedInterpolant(grid.w,grid.B,grid.Hb,Rnew,'linear','linear');
FB=griddedInterpolant(grid.w,grid.B,grid.Hb,Bnew,'linear','linear');

Fmult=griddedInterpolant(grid.w,grid.B,grid.Hb,multiplier,'linear','linear');

for t=[ones(1,Tpre) 1:T]
    wsim(t)=rhow*wlag+0*sigmaw*randn(1)*(t>1);
    if t>=100 && t<= 150; wsim(t)=wmin; end
    wsim(t)=max(min(wsim(t),wmax),wmin);
    Cssim(t)=FCs(wsim(t),Blag,Hblag);
    Cbsim(t)=FCb(wsim(t),Blag,Hblag);
    Qsim(t)=FQ(wsim(t),Blag,Hblag);
    Rsim(t)=FR(wsim(t),Blag,Hblag);
    
    %interpolated multiplier might be positive even when constraint is not binding
    %if interpolated between binding and non-binding node
%     Hbsim(t)=FHb(wsim(t),Blag,Hblag);
%     Bsim(t)=(Cbsim(t)-Ybfun(Cbsim(t),wsim(t))+Blag+Qsim(t)*(Hbsim(t)-Hblag))*Rsim(t);
%     if Bsim(t)>=(theta-1e-3)*Qsim(t)*Hbsim(t)*Rsim(t)
%         multsim(t)=Fmult(wsim(t),Blag,Hblag);
%     else
%         multsim(t)=0; 
%     end
    
%     multsim(t)=Fmult(wsim(t),Blag,Hblag);
%     if multsim(t)>0
%         Hbsim(t)=(Cbsim(t)-Ybfun(Cbsim(t),wsim(t))+Blag-Qsim(t)*Hblag)/(theta-1)/Qsim(t);
%         Bsim(t)=theta*Qsim(t)*Hbsim(t)*Rsim(t);
%     else
%         Hbsim(t)=FHb(wsim(t),Blag,Hblag);
%         Bsim(t)=(Cbsim(t)-Ybfun(Cbsim(t),wsim(t))+Blag+Qsim(t)*(Hbsim(t)-Hblag))*Rsim(t);
%     end
    
    multsim(t)=Fmult(wsim(t),Blag,Hblag);
    Hbsim(t)=FHb(wsim(t),Blag,Hblag);
    Bsim(t)=FB(wsim(t),Blag,Hblag);
    
    wlag=wsim(t);
    Blag=max(min(Bsim(t),Bmax),Bmin);
    Hblag=max(min(Hbsim(t),Hbmax),Hbmin);
end

figure;
subplot(2,4,1); plot(1:T,wsim); title('w (SS log dev)');
subplot(2,4,2); plot(1:T,log(Qsim)-SS.Q); title('Q (SS log dev)');
subplot(2,4,3); plot(1:T,log(Rsim)-log(1+SS.r)); title('R (SS log dev)');
subplot(2,4,4); plot(1:T,log(Bsim/SS.B)); title('B (SS log dev)');
subplot(2,4,5); plot(1:T,log(Hbsim)-SS.Hb); title('H_b (SS log dev)');
subplot(2,4,6); plot(1:T,log(Cbsim)-SS.Cb); title('C_b (SS log dev)');
subplot(2,4,7); plot(1:T,log(Cssim)-SS.Cs); title('C_s (SS log dev)');
subplot(2,4,8); plot(1:T,Bsim./(Qsim.*Hbsim.*Rsim),1:T,ones(1,T)*theta); title('debt-to-value ratio');

