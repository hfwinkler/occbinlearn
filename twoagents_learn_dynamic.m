function [residual, g1, g2, g3] = twoagents_learn_dynamic(y, x, params, steady_state, it_)
%
% Status : Computes dynamic model for Dynare
%
% Inputs :
%   y         [#dynamic variables by 1] double    vector of endogenous variables in the order stored
%                                                 in M_.lead_lag_incidence; see the Manual
%   x         [M_.exo_nbr by nperiods] double     matrix of exogenous variables (in declaration order)
%                                                 for all simulation periods
%   params    [M_.param_nbr by 1] double          vector of parameter values in declaration order
%   it_       scalar double                       time period for exogenous variables for which to evaluate the model
%
% Outputs:
%   residual  [M_.endo_nbr by 1] double    vector of residuals of the dynamic model equations in order of 
%                                          declaration of the equations
%   g1        [M_.endo_nbr by #dynamic variables] double    Jacobian matrix of the dynamic model equations;
%                                                           columns: equations in order of declaration
%                                                           rows: variables in order stored in M_.lead_lag_incidence
%   g2        [M_.endo_nbr by (#dynamic variables)^2] double   Hessian matrix of the dynamic model equations;
%                                                              columns: equations in order of declaration
%                                                              rows: variables in order stored in M_.lead_lag_incidence
%   g3        [M_.endo_nbr by (#dynamic variables)^3] double   Third order derivative matrix of the dynamic model equations;
%                                                              columns: equations in order of declaration
%                                                              rows: variables in order stored in M_.lead_lag_incidence
%
%
% Warning : this file is generated automatically by Dynare
%           from model file (.mod)

%
% Model equations
%

residual = zeros(14, 1);
lhs =exp(y(12));
rhs =exp(y(15)+y(14))+exp(y(16))*(exp(y(3))-exp(y(13)))-y(2)+y(11)/(1+y(17));
residual(1)= lhs-rhs;
lhs =y(11)*(1+y(17));
rhs =params(6)*exp(y(16)+y(13));
residual(2)= lhs-rhs;
lhs =log(params(10))+y(14)*params(11)+y(12)*params(8);
rhs =y(15);
residual(3)= lhs-rhs;
lhs =1-y(18);
rhs =(1+y(17))*params(7)*exp(params(8)*(y(12)-y(23)));
residual(4)= lhs-rhs;
lhs =exp(y(16)-y(12)*params(8))*(1-params(6)*y(18));
rhs =params(12)*exp(y(13)*(-params(9)))+params(7)*exp(y(23)*(-params(8))+y(24));
residual(5)= lhs-rhs;
lhs =exp(y(8));
rhs =y(2)+exp(y(15)+y(10))+exp(y(16))*(exp(y(1))-exp(y(9)))-y(11)/(1+y(17));
residual(6)= lhs-rhs;
lhs =log(params(4))+y(10)*params(5)+y(8)*params(2);
rhs =y(15);
residual(7)= lhs-rhs;
lhs =1;
rhs =(1+y(17))*params(1)*exp(params(2)*(y(8)-y(22)));
residual(8)= lhs-rhs;
lhs =exp(y(16)-y(8)*params(2));
rhs =exp(y(9)*(-params(3)))+params(1)*exp(y(24)+y(22)*(-params(2)));
residual(9)= lhs-rhs;
lhs =y(16);
rhs =y(5)+y(6)+params(17)*x(it_, 2)-0.5*(params(17)*y(25))^2;
residual(10)= lhs-rhs;
lhs =y(19);
rhs =y(6)+params(16)*(params(17)*x(it_, 3)-0.5*(params(17)*y(25))^2);
residual(11)= lhs-rhs;
lhs =y(21);
rhs =x(it_, 2);
residual(12)= lhs-rhs;
lhs =y(20);
rhs =y(7);
residual(13)= lhs-rhs;
lhs =y(15);
rhs =params(14)*y(4)+params(13)*x(it_, 1);
residual(14)= lhs-rhs;
if nargout >= 2,
  g1 = zeros(14, 28);

  %
  % Jacobian matrix
  %

  g1(1,2)=1;
  g1(1,11)=(-(1/(1+y(17))));
  g1(1,12)=exp(y(12));
  g1(1,3)=(-(exp(y(16))*exp(y(3))));
  g1(1,13)=(-(exp(y(16))*(-exp(y(13)))));
  g1(1,14)=(-exp(y(15)+y(14)));
  g1(1,15)=(-exp(y(15)+y(14)));
  g1(1,16)=(-(exp(y(16))*(exp(y(3))-exp(y(13)))));
  g1(1,17)=(-((-y(11))/((1+y(17))*(1+y(17)))));
  g1(2,11)=1+y(17);
  g1(2,13)=(-(params(6)*exp(y(16)+y(13))));
  g1(2,16)=(-(params(6)*exp(y(16)+y(13))));
  g1(2,17)=y(11);
  g1(3,12)=params(8);
  g1(3,14)=params(11);
  g1(3,15)=(-1);
  g1(4,12)=(-((1+y(17))*params(7)*params(8)*exp(params(8)*(y(12)-y(23)))));
  g1(4,23)=(-((1+y(17))*params(7)*exp(params(8)*(y(12)-y(23)))*(-params(8))));
  g1(4,17)=(-(params(7)*exp(params(8)*(y(12)-y(23)))));
  g1(4,18)=(-1);
  g1(5,12)=(1-params(6)*y(18))*exp(y(16)-y(12)*params(8))*(-params(8));
  g1(5,23)=(-(params(7)*(-params(8))*exp(y(23)*(-params(8))+y(24))));
  g1(5,13)=(-(params(12)*(-params(9))*exp(y(13)*(-params(9)))));
  g1(5,16)=exp(y(16)-y(12)*params(8))*(1-params(6)*y(18));
  g1(5,24)=(-(params(7)*exp(y(23)*(-params(8))+y(24))));
  g1(5,18)=exp(y(16)-y(12)*params(8))*(-params(6));
  g1(6,8)=exp(y(8));
  g1(6,1)=(-(exp(y(16))*exp(y(1))));
  g1(6,9)=(-(exp(y(16))*(-exp(y(9)))));
  g1(6,10)=(-exp(y(15)+y(10)));
  g1(6,2)=(-1);
  g1(6,11)=1/(1+y(17));
  g1(6,15)=(-exp(y(15)+y(10)));
  g1(6,16)=(-(exp(y(16))*(exp(y(1))-exp(y(9)))));
  g1(6,17)=(-y(11))/((1+y(17))*(1+y(17)));
  g1(7,8)=params(2);
  g1(7,10)=params(5);
  g1(7,15)=(-1);
  g1(8,8)=(-((1+y(17))*params(1)*params(2)*exp(params(2)*(y(8)-y(22)))));
  g1(8,22)=(-((1+y(17))*params(1)*exp(params(2)*(y(8)-y(22)))*(-params(2))));
  g1(8,17)=(-(params(1)*exp(params(2)*(y(8)-y(22)))));
  g1(9,8)=exp(y(16)-y(8)*params(2))*(-params(2));
  g1(9,22)=(-(params(1)*(-params(2))*exp(y(24)+y(22)*(-params(2)))));
  g1(9,9)=(-((-params(3))*exp(y(9)*(-params(3)))));
  g1(9,16)=exp(y(16)-y(8)*params(2));
  g1(9,24)=(-(params(1)*exp(y(24)+y(22)*(-params(2)))));
  g1(10,5)=(-1);
  g1(10,16)=1;
  g1(10,6)=(-1);
  g1(10,25)=0.5*params(17)*2*params(17)*y(25);
  g1(10,27)=(-params(17));
  g1(11,6)=(-1);
  g1(11,19)=1;
  g1(11,25)=(-(params(16)*(-(0.5*params(17)*2*params(17)*y(25)))));
  g1(11,28)=(-(params(17)*params(16)));
  g1(12,21)=1;
  g1(12,27)=(-1);
  g1(13,20)=1;
  g1(13,7)=(-1);
  g1(14,4)=(-params(14));
  g1(14,15)=1;
  g1(14,26)=(-params(13));
end
if nargout >= 3,
  %
  % Hessian matrix
  %

  g2 = sparse([],[],[],14,784);
end
if nargout >= 4,
  %
  % Third order derivatives
  %

  g3 = sparse([],[],[],14,21952);
end
end
