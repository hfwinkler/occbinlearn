% solve CCHH model with global projection method
% version with endogenous housing

%% parameters

%model parameters
betta=1.035^-0.25;
R=1.03^0.25;
Ybar=1;
rhoy=0.9;
sigy=1e-2;
theta=0.5;
delta=0.025;
psi=0.1;
chi=1;

%algorithm parameters
minC=1e-6;
convcrit=1e-4;
lambda=0.01;
plotfreq=1;
loadlast=1;

%% non-stochastic steady-state

Hss=chi*Ybar/(1-betta*(1-delta)-theta*(1-betta*R)+chi*delta+chi*theta*(R-1));
Css=Ybar-delta*Hss-(R-1)*theta*Hss;
Qss=1;
Bss=theta*Hss*Qss;

%% set up grid and nodes

%income grid
ymin=log(Ybar)-3*sigy/sqrt(1-rhoy^2);
ymax=log(Ybar)+3*sigy/sqrt(1-rhoy^2);
Ky=5;
grid.onlyy=linspace(ymin,ymax,Ky);

%debt grid
Bmin=Bss*0;
Bmax=Bss*1;
KB=21;
grid.onlyB=linspace(Bmin,Bmax,KB);

%housing grid
Hmin=Hss*0.5;
Hmax=Hss*1.3;
KH=21;
grid.onlyH=linspace(Hmin,Hmax,KH);

%overall grid
[grid.y, grid.B, grid.H]=ndgrid(grid.onlyy,grid.onlyB,grid.onlyH);
grid.length=Ky*KB*KH;
fprintf('Nodes: %i\n',grid.length);

%number of quadrature nodes
J=9; %J=5 to get 2 s.d., J=9 to get 3 s.d.
%calculate Gauss-Hermite nodes and weights
CM      = diag(sqrt((1:(J-1))/2),1) + diag(sqrt((1:(J-1))/2),-1);
[V, L]   = eig(CM);
[zeta, ind] = sort(diag(L));
V       = V(:,ind)';
w       = V(:,1).^2; %don't multiply by sqrt(pi) here

%for two shocks
%[zzeta1, zzeta2]=ndgrid(zeta,zeta);
%ww=w*w';

%% initial guess for policy function

%wild guess
Hold=Hss*ones(size(grid.H));
Cold=max(exp(grid.y)+Hss-(1-delta)*Hold-(R-1)*grid.B,10*minC);

%or load from file:
if loadlast
    load cchhtemp;
    Hold=interpn(gridold.y,gridold.B,gridold.H,Hold,grid.y,grid.B,grid.H,'spline');
    Cold=max(interpn(gridold.y,gridold.B,gridold.H,Cold,grid.y,grid.B,grid.H,'spline'),minC);
end
gridold=grid;

Hnew=Hold;
Cnew=Cold;

%auxiliaries:
Bnew=grid.B;
Qnew=Qss*ones(size(grid.y));
Xnew=ones(size(grid.y));

%% iterate

%load cchhtemp;
maxiter=1e4;
options=optimset('Display','off','MaxFunEvals',1e2,'TolFun',1e-5,'Algorithm','trust-region-dogleg');
Qfun=@(H) 1+psi*(H-Hss);
%Pifun=@(Q,H,Hminus) (Q-1)*(H-(1-delta)*Hminus)-psi/2*(H-Hss)^2;
Pifun=@(Q,H,Hminus) 0;

h=figure;
for n=1:maxiter
    %walk through grid
    Fc=griddedInterpolant(grid.y,grid.B,grid.H,log(Cold),'linear','linear');
    Fh=griddedInterpolant(grid.y,grid.B,grid.H,log(Hold),'linear','linear');
    parfor node=1:grid.length %can be replaced by parfor if grid is large
        %get grid points
        y=grid.y(node);
        Bminus=grid.B(node);
        Hminus=grid.H(node);
        %current period:
        Htemp=Hold(node);
        %prevent falling off the grid?
        Htemp=max(min(Htemp,Hmax),Hmin);
        Ctemp=Cold(node);
        Qtemp=Qfun(Htemp);
        Btemp=Ctemp-exp(y)+R*Bminus+Qtemp*(Htemp-(1-delta)*Hminus)-Pifun(Qtemp,Htemp,Hminus);
        %prevent falling off the grid?
        Btemp=max(min(Btemp,Bmax),Bmin);
        Htemp=Htemp*ones(J,1);
        Btemp=Btemp*ones(J,1);
        %next period:
        y1=rhoy*y+(1-rhoy)*log(Ybar)+sigy*zeta;
        H1=exp(Fh(y1,Btemp,Htemp)); %#ok<*PFBNS>
        C1=exp(Fc(y1,Btemp,Htemp)); %#ok<*PFBNS>
        C1=max(C1,minC); %if future consumption negative, set MU very high
        H1=max(H1,minC); %same for housing
        Q1=Qfun(H1);
        %take expectation with quadrature
        integrand1=1./C1;
        k1=sum(integrand1.*w);
        integrand2=Q1./C1;
        k2=sum(integrand2.*w);
        %get new solution: try unconstrained first
        C=1/(betta*R*k1);
        a1=psi;
        b1=1-psi*Hss-betta*(1-delta)*k2*C;
        c1=-chi*C;
        H=1/(2*a1)*(-b1+sqrt(b1^2-4*a1*c1));
        Q=Qfun(H);
        B=C-exp(y)+R*Bminus+Q*(H-(1-delta)*Hminus)-Pifun(Q,H,Hminus);
        if B>theta*Q*H %if borrowing constraint violated, try constrained
            fun=@(CH) [ -CH(1) + exp(y) - Qfun(CH(2))*((1-theta)*CH(2)-(1-delta)*Hminus)+Pifun(Qfun(CH(2)),CH(2),Hminus) - R*Bminus;
                        Qfun(CH(2))*(1-theta*(1-betta*CH(1)*k1*R)) - chi*CH(1)/CH(2) - betta*CH(1)*(1-delta)*k2 ];
            [CH,funval,exitflag]=fsolve(@(ch) fun(exp(ch)),log([Cold(node),Hold(node)]),options);
            C=exp(CH(1));
            H=exp(CH(2));
            Q=Qfun(H);
            B=theta*Q*H;
        end
        Cnew(node)=C;
        Hnew(node)=H;
        Bnew(node)=B;
        Qnew(node)=Q;
        Xnew(node)=B/(theta*Q*H);
    end
    
    %measure distance to old policy function and stop if it's small
    dist=max(abs(Cnew(:)-Cold(:)));
    if dist<convcrit
        fprintf('#%i: distance: %3.3g\n',n,dist);
        break; 
    end
    %update policy function with attenuation
    Hold=(1-lambda)*Hnew+lambda*Hold;
    Cold=(1-lambda)*Cnew+lambda*Cold;
    save cchhtemp Cold Hold gridold;
    
    if mod(n,plotfreq)==0 %plot policy function
        fprintf('#%i: distance: %3.3g\n',n,dist);
        subplot(1,2,1);
        surf(grid.onlyy,grid.onlyB/Bss,Cnew(:,:,round(KH/2))'/Css); 
        xlabel('y'); xlim([ymin ymax]);
        ylabel('B'); ylim([Bmin Bmax]/Bss);
        zlabel('H');
        subplot(1,2,2);
        surf(grid.onlyB/Bss,grid.onlyH/Hss,squeeze(Cnew(round(Ky/2),:,:))'/Css); 
        xlabel('B'); xlim([Bmin Bmax]/Bss);
        ylabel('H'); ylim([Hmin Hmax]/Hss);
        zlabel('H');
        drawnow;
    end
end
close(h);

%% plot

figure; 
subplot(1,2,1);
surf(grid.onlyy,grid.onlyB,Qnew(:,:,round(KH/2))'); 
xlabel('y'); xlim([ymin ymax]);
ylabel('B'); ylim([Bmin Bmax]);
zlabel('Q');
subplot(1,2,2);
surf(grid.onlyB,grid.onlyH,squeeze(Qnew(round(Ky/2),:,:))'); 
xlabel('B'); xlim([Bmin Bmax]);
ylabel('H'); ylim([Hmin Hmax]);
zlabel('Q');

figure; 
subplot(1,2,1);
surf(grid.onlyy,grid.onlyB,Hnew(:,:,round(KH/2))'); 
xlabel('y'); xlim([ymin ymax]);
ylabel('B'); ylim([Bmin Bmax]);
zlabel('H');
subplot(1,2,2);
surf(grid.onlyB,grid.onlyH,squeeze(Hnew(round(Ky/2),:,:))'); 
xlabel('B'); xlim([Bmin Bmax]);
ylabel('H'); ylim([Hmin Hmax]);
zlabel('H');

figure; 
subplot(1,2,1);
surf(grid.onlyy,grid.onlyB,Bnew(:,:,round(KH/2))'); 
xlabel('y'); xlim([ymin ymax]);
ylabel('B'); ylim([Bmin Bmax]);
zlabel('B');
subplot(1,2,2);
surf(grid.onlyB,grid.onlyH,squeeze(Bnew(round(Ky/2),:,:))'); 
xlabel('B'); xlim([Bmin Bmax]);
ylabel('H'); ylim([Hmin Hmax]);
zlabel('B');

figure; 
subplot(1,2,1);
surf(grid.onlyy,grid.onlyB,Cnew(:,:,round(KH/2))'); 
xlabel('y'); xlim([ymin ymax]);
ylabel('B'); ylim([Bmin Bmax]);
zlabel('C');
subplot(1,2,2);
surf(grid.onlyB,grid.onlyH,squeeze(Cnew(round(Ky/2),:,:))'); 
xlabel('B'); xlim([Bmin Bmax]);
ylabel('H'); ylim([Hmin Hmax]);
zlabel('C');

%% simulation

T=1000;
Tpre=0;
rng(10);
ysim=zeros(1,T);
Qsim=zeros(1,T);
Xsim=zeros(1,T);
Bsim=zeros(1,T);
Csim=zeros(1,T);
Hsim=zeros(1,T);

Blag=Bss*0.5;
Hlag=Hss*1;
ylag=log(Ybar);

FC=griddedInterpolant(grid.y,grid.B,grid.H,Cnew,'spline','linear');
FH=griddedInterpolant(grid.y,grid.B,grid.H,Hnew,'spline','linear');
FB=griddedInterpolant(grid.y,grid.B,grid.H,Bnew,'spline','linear');
 
for t=[ones(1,Tpre) 1:T]
    ysim(t)=rhoy*ylag+(1-rhoy)*log(Ybar)+sigy*randn(1)*(t>1);
    ysim(t)=max(min(ysim(t),ymax),ymin);
    %if t>=30 && t<=80; ysim(t)=ymin; end
    Csim(t)=FC(ysim(t),Blag,Hlag);
    Hsim(t)=FH(ysim(t),Blag,Hlag);
    Qsim(t)=Qfun(Hsim(t));
    Bsim(t)=Csim(t)-exp(ysim(t))+R*Blag+Qsim(t)*(Hsim(t)-(1-delta)*Hlag)-Pifun(Qsim(t),Hsim(t),Hlag);
    Xsim(t)=Bsim(t)/(theta*Qsim(t)*Hsim(t));
    if Xsim(t)>1
        Xsim(t)=1;
        Bsim(t)=Qsim(t)*theta*Hsim(t);
        Csim(t)=exp(ysim(t))-R*Blag+Bsim(t)-Qsim(t)*(Hsim(t)-(1-delta)*Hlag)+Pifun(Qsim(t),Hsim(t),Hlag);
        Qsim(t)=Qfun(Hsim(t));
        Bsim(t)=Qsim(t)*theta*Hsim(t);
        if Csim(t)<0
            %Csim(t)=nan;
            %Bsim(t)=nan;
        end
    end
    ylag=ysim(t);
    Blag=max(min(Bsim(t),Bmax),Bmin);
    Hlag=max(min(Hsim(t),Hmax),Hmin);
end

figure;
subplot(2,3,1); plot(1:T,ysim-log(Ybar)); title('income, log dev. from SS');
subplot(2,3,2); plot(1:T,log(Qsim/Qss)); title('house price, log dev from SS');
subplot(2,3,3); plot(1:T,Xsim,1:T,ones(1,T)); ylim([0 1.1]); title('debt as a fraction of debt limit');
subplot(2,3,4); plot(1:T,log(Bsim/Bss)); title('debt, log dev from SS');
subplot(2,3,5); plot(1:T,log(Csim/Css)); title('consumption, log dev from SS');
subplot(2,3,6); plot(1:T,log(Hsim/Hss)); title('housing, log dev from SS');

