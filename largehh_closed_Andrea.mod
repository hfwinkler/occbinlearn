% Dynare file to simulate large household model
% with the constraint always binding
% closed economy version

var C H N b B;
var W Q r;
var lambda;

varexo epsw;

parameters betta nu omega;
parameters chi theta;
parameters sigmaw rhow;

@#include "largehh_parameters_Andrea.m"

%shorthand notation:

model;

%lender BC
%exp(C)+b = (1-chi)*(exp(W+N)+exp(Q+H(-1))+B-(1+r)*B(-1));
%borrower BC
%exp(Q+H) = b + chi*(exp(W+N) + exp(Q+H(-1)) + (1+r)*B(-1) - (1+r)*B(-1));
exp(Q+H) = b + chi*(exp(W+N) + exp(Q+H(-1)) + (1+r(-1))*B(-1) - (1+r(-1))*B(-1));
%exp(C)   = exp(W+N)+B-(1+r)*B(-1);
exp(C)   = exp(W+N);
%borrowing constraint
b = theta*exp(Q+H); % eliminate chi
%chi*B+b = theta*exp(Q+H);
%borrower FOC for housing:
%exp(Q-C)*(1-theta*lambda) = exp(-H) + betta*exp(Q(+1)-C(+1))*(1+chi*lambda(+1));
exp(Q-C)*(1+(1-theta)*lambda) = exp(-H) + betta*exp(Q(+1)-C(+1))*(1+chi*lambda(+1));
%lender FOC for loans:
exp(-C) = betta*(1+r)*exp(-C(+1))*(1+chi*lambda(+1));
%labor FOC:
omega*exp(nu*N)=exp(W-C)*(1+chi*lambda);


%labor demand
W=rhow*W(-1)+sigmaw*epsw;
%housing supply
H=0;
%aggregate loan supply
B=b;

end;

steady_state_model;

%exogenous
W=0;
H=0;

%interest rate:
%rwedge1=(theta+betta*chi)/(2*chi+ theta + theta*chi - 1);
%rwedge1=(theta+betta*chi)/(2*chi+2*theta-1);

lambda = 1/(1 - theta - betta*chi) * ( - (1-betta) + (1 - theta - chi)/chi );

rwedge1 = 1/(1+chi*lambda);

r=rwedge1/betta-1;

% labor market
N=(1/(nu + 1))*(-log(omega*rwedge1));

% goods market
C = W + N;

% housing market
Q = C + log( chi / (1 - theta - chi) );

%shorthand notation:
%k1=1-betta-(1-rwedge1)/rwedge1*(theta/chi+betta);

%endogenous:
%C=W - 1/(nu+1)*log(omega*rwedge1);
%N=1/nu*(W-C-log(omega*rwedge1));
%Q=C-H-log(k1);
%lambda=(1-rwedge1)/(chi*rwedge1);
%b=theta*exp(Q+H);
b=theta*exp(Q+H);
B=b;

end;


shocks;
var epsw; stderr 1;
end;

steady;
check;

stoch_simul;