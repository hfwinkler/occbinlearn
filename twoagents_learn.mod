% Dynare file to simulate a model with two households
% with the constraint always binding
% learning version

var Cs Hs Ls B;
var Cb Hb Lb;
var W Q r;
var lambda;

varexo epsw;

parameters bettas gamas nus etas phis;
parameters theta betta gama nu eta phi chi;
parameters sigmaw rhow Hbar;

%additional learning:
var mu, lagv, auxv;
varexo v, v1;
parameters g, sigmav;

@#include "twoagents_parameters.m"

model;

%borrower
exp(Cb)=exp(W+Lb)+exp(Q)*(exp(Hb(-1))-exp(Hb))-B(-1)+B/(1+r);
B/(1+r)=theta*exp(Q+Hb);

log(eta)+phi*Lb+gama*Cb=W;
1-lambda=betta*exp(gama*(Cb-Cb(+1)))*(1+r);
exp(Q-gama*Cb)*(1-theta*lambda)=chi*exp(-nu*Hb)+betta*exp(-gama*Cb(+1)+Q(+1));

%saver
exp(Cs)=exp(W+Ls)+exp(Q)*(exp(Hs(-1))-exp(Hs))+B(-1)-B/(1+r);

log(etas)+phis*Ls+gamas*Cs=W;
1=bettas*exp(gamas*(Cs-Cs(+1)))*(1+r);
exp(Q-gamas*Cs)=exp(-nus*Hs)+bettas*exp(-gamas*Cs(+1)+Q(+1));

%housing supply -- eliminated
%exp(Hs)+exp(Hb)=Hbar;

%learning
Q=Q(-1)+mu(-1)+sigmav*v-0.5*(sigmav*auxv(+1))^2; 
mu=mu(-1)+g*(sigmav*v1-0.5*(sigmav*auxv(+1))^2);
auxv=v;
lagv=auxv(-1);

%exogenous wage
W=rhow*W(-1)+sigmaw*epsw;

end;

shocks;
var epsw; stderr 1;
var v; stderr 1;
var v1; stderr 1;
end;

%load steady state from RE (compute it first!!)
load twoagents_SS;
oo_.steady_state(1:length(ys_RE))=ys_RE;

options_.PLM=0;
stoch_simul(order=1,irf=100,nocorr,ar=0);
%stoch_simul(order=1,irf=100,nomoments,nograph);
