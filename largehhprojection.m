% solve large HH model with global projection method
% with housing production

clear;
%% parameters

%household
betta=0.99;
nu=2;
omega=2.3564;
phi=1/0.3;
chi=0.7;
theta=0.29;

%exogenous wage process
sigw=0.05;
rhow=0.9;

% for housing production
psi=10;
deltaH=0.025;

%algorithm parameters
minC=1e-6;
convcrit=1e-5;
lambdaAlgo=0.1;
plotfreq=1;
options=optimset('Display','off');

%% steady state guess

%exogenous
Wss=1;

%endogenous:
if 1-chi-theta<=0 %constraint likely won't bind
Qss=1;
lambdass=0;
Css = exp(-1/(1+nu)*log(omega) - nu/(1+nu)*log(1+deltaH/(1-betta*(1-deltaH))));
Hss = exp(log(Css) - log(1-betta*(1-deltaH)));
Nss = exp(-1/nu*(log(Css)+log(omega)));

else %constraint will likely bind
Qss=1;
lambdass = (1-theta-chi*(1+deltaH+(1-betta)*(1-deltaH))) / chi / (1 - theta - betta*chi*(1-deltaH));
Css = exp(1/(1+nu)*(-log(omega)+log(1+chi*lambdass)) - nu/(1+nu)*log(1+deltaH*chi/(1-theta-chi)));
Hss = exp(log(Css) + log(chi) - log(1-theta-chi));
Nss = exp(1/nu*(-log(omega)+log(1+chi*lambdass)) -1/nu*log(Css));

end

%% set up grid and nodes

%wage grid
wmin=log(Wss)-3*sigw/sqrt(1-rhow^2);
wmax=log(Wss)+3*sigw/sqrt(1-rhow^2);
Kw=11;
grid.onlyw=linspace(wmin,wmax,Kw);

%housing grid
Hmin=Hss*0.8;
Hmax=Hss*1.5;
KH=31;
grid.onlyH=linspace(Hmin,Hmax,KH);

%overall grid
[grid.w, grid.H]=ndgrid(grid.onlyw,grid.onlyH);
grid.length=Kw*KH;
fprintf('Nodes: %i\n',grid.length);

%number of quadrature nodes
J=5;
%calculate Gauss-Hermite nodes and weights
CM      = diag(sqrt((1:(J-1))/2),1) + diag(sqrt((1:(J-1))/2),-1);
[V, L]   = eig(CM);
[zeta, ind] = sort(diag(L));
V       = V(:,ind)';
weight  = V(:,1).^2; %don't multiply by sqrt(pi) here

%for two shocks
%[zzeta1, zzeta2]=ndgrid(zeta,zeta);
%ww=weight*weight';

%% initial guess for policy function

Cold=Css*ones(size(grid.w));
Hold=Hss*ones(size(grid.w));
lambdaold=lambdass*ones(size(grid.w));

Cnew=Cold;
Hnew=Hold;
lambdanew=lambdaold;

%% auxiliary functions

Qfun=@(H,Hminus) max(1+psi*(H./Hminus-1),0);  %NB: free disposal -- this is important
Yfun=@(C,w) exp(w)*(exp(w)*C^-phi/omega)^(1/nu); %output when lambda=0
Hcostfun=@(H,Hminus) H-(1-deltaH)*Hminus + psi/2*(H/Hminus-1)^2;
Cfun=@(H,Hminus) Qfun(H,Hminus)*H*(1-theta-chi)/chi; %consumption when lambda>0

%% iterate


maxiter=1e4;
h=figure;
for n=1:maxiter
    %walk through grid
    FC=griddedInterpolant(grid.w,grid.H,Cold,'linear','linear');
    FH=griddedInterpolant(grid.w,grid.H,Hold,'linear','linear');
    Flambda=griddedInterpolant(grid.w,grid.H,lambdaold,'linear','linear');
    for node=1:grid.length %can be replaced by parfor if grid is large
        %get grid points
        w=grid.w(node);
        Hminus=grid.H(node);
        %calculate old policy functions first
        Htemp=FH(w,Hminus); 
        Ctemp=FC(w,Hminus);
        %house price expectation:
        w1=rhow*w+(1-rhow)*log(Wss)+sigw*zeta;
        w1=min(max(w1,wmin),wmax);
        H1minus=ones(size(w1))*Htemp;
        H1=FH(w1,H1minus);
        H1=min(max(H1,Hmin),Hmax);
        C1=FC(w1,H1minus);
        lambda1=Flambda(w1,H1minus);
        Q1=Qfun(H1,Htemp);
        %take expectation with quadrature
        integrand1=C1.^-phi.*Q1.*(1+chi*lambda1);
        k1=sum(integrand1.*weight);       
        %try unconstrained first:
        lambda=0;
        fun=@(CH) [ Yfun(CH(1),w) - CH(1) - Hcostfun(CH(2),Hminus); ...
                    Qfun(CH(2),Hminus)*CH(1)^-phi - 1/CH(2) - betta*(1-deltaH)*k1];
        CH=fsolve(fun,[Ctemp, Htemp],options);
        C=CH(1); 
        H=CH(2);
        Q=Qfun(H,Hminus);
        %intra-household borrowing
        b = (1-chi)*Q*H - chi*(Yfun(C,w)-Hcostfun(H,Hminus));
        
        if b>theta*Q*H %if borrowing constraint violated, try constrained 
            fun=@(H) -1/H - betta*(1-deltaH)*k1 + Qfun(H,Hminus)*Cfun(H,Hminus)^phi*(1+(1-theta)/chi* max( ...
                     omega/exp(w)*Cfun(H,Hminus)^phi*(Cfun(H,Hminus)+Hcostfun(H,Hminus))^nu/exp(w)^nu-1 , 0));
                 %NB: the criterion imposes lambda>0, otherwise we have
                 %convergence problems
            [H,funval,exitflag]=fsolve(fun,Htemp,options);
            if abs(funval)>1e-5; keyboard; end
            C=Cfun(H,Hminus);  
            Q=Qfun(H,Hminus);
            lambda=omega/chi/exp(w)*C^phi*(C+Hcostfun(H,Hminus))^nu/exp(w)^nu-1/chi;
            if lambda<0; lambda=0; end %not sure this is kosher, but seems to work
        end
        %if H<3; keyboard; end
        Hnew(node)=H;
        Cnew(node)=C;
        lambdanew(node)=lambda;
    end
    
    %measure distance to old policy function and stop if it's small
    dist=max(abs(Cnew(:)-Cold(:)));
    if dist<convcrit
        fprintf('#%i: distance: %3.3g\n',n,dist);
        break; 
    end
    %update policy function with attenuation
    Hold=(1-lambdaAlgo)*Hnew+lambdaAlgo*Hold;
    Cold=(1-lambdaAlgo)*Cnew+lambdaAlgo*Cold;
    lambdaold=(1-lambdaAlgo)*lambdanew+lambdaAlgo*lambdaold;
    save largehhtemp Hold Cold lambdaold;
    
    if mod(n,plotfreq)==0 %plot policy function
        fprintf('#%i: distance: %3.3g\n',n,dist);
        surf(grid.onlyw,grid.onlyH,lambdanew'); 
        xlabel('w'); xlim([wmin wmax]);
        ylabel('H(-1)'); ylim([Hmin Hmax]);
        zlabel('multiplier');
        drawnow;
    end
end
close(h);

%% plot

figure; 
surf(grid.onlyw,grid.onlyH,Qfun(Hnew,grid.H)'); 
xlabel('w'); xlim([wmin wmax]);
ylabel('H(-1)'); ylim([Hmin Hmax]);
zlabel('Q');
title('house price');
figure;
surf(grid.onlyw,grid.onlyH,Hnew'); 
xlabel('w'); xlim([wmin wmax]);
ylabel('H(-1)'); ylim([Hmin Hmax]);
zlabel('H');
title('housing stock');
figure;
surf(grid.onlyw,grid.onlyH,lambdanew'); 
xlabel('w'); xlim([wmin wmax]);
ylabel('H(-1)'); ylim([Hmin Hmax]);
zlabel('X');
title('multiplier');
figure;
surf(grid.onlyw,grid.onlyH,Cnew'); 
xlabel('w'); xlim([wmin wmax]);
ylabel('H(-1)'); ylim([Hmin Hmax]);
zlabel('C');
title('consumption');

%% simulation

T=1E3;
Tpre=0;
rng(10);
wsim=zeros(1,T);
Lsim=zeros(1,T);
Qsim=zeros(1,T);
Hsim=zeros(1,T);
Csim=zeros(1,T);
bsim=zeros(1,T);
Xsim=zeros(1,T);

wlag=log(Wss);
Hlag=Hss*1;

FC=griddedInterpolant(grid.w,grid.H,Cold,'linear','linear');
FH=griddedInterpolant(grid.w,grid.H,Hold,'linear','linear');
   
for t=[ones(1,Tpre) 1:T]
    wsim(t)=rhow*wlag+(1-rhow)*log(Wss)+sigw*randn(1)*(t>1);
    Csim(t)=FC(wsim(t),Hlag);
    Hsim(t)=FH(wsim(t),Hlag);
    Qsim(t)=Qfun(Hsim(t),Hlag);
    Lsim(t)=(Csim(t) + Hcostfun(Hsim(t),Hlag))/exp(wsim(t));
    bsim(t)=(1-chi)*Qsim(t)*Hsim(t) - chi*Csim(t);
    Xsim(t)=bsim(t)/(theta*Qsim(t)*Hsim(t));
    wlag=wsim(t);
end

figure;
subplot(2,3,1); plot(1:T,exp(wsim)/Wss); title('wage relative to SS');
subplot(2,3,3); plot(1:T,Csim/Css); title('consumption relative to SS');
subplot(2,3,4); plot(1:T,Qsim/Qss); title('house price relative to SS');
subplot(2,3,6); plot(1:T,Xsim,1:T,ones(1,T)); title('debt as a fraction of debt limit');


fprintf('Average debt level:\t%3.3g \nAverage house price\t%3.3g\n',mean(bsim(T-100:T)),mean(Qsim(T-100:T)));
