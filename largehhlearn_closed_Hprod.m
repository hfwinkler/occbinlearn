%
% Status : main Dynare file 
%
% Warning : this file is generated automatically by Dynare
%           from model file (.mod)

clear all
tic;
global M_ oo_ options_ ys0_ ex0_ estimation_info
options_ = [];
M_.fname = 'largehhlearn_closed_Hprod';
%
% Some global variables initialization
%
global_initialization;
diary off;
diary('largehhlearn_closed_Hprod.log');
M_.exo_names = 'epsw';
M_.exo_names_tex = 'epsw';
M_.exo_names_long = 'epsw';
M_.exo_names = char(M_.exo_names, 'v');
M_.exo_names_tex = char(M_.exo_names_tex, 'v');
M_.exo_names_long = char(M_.exo_names_long, 'v');
M_.exo_names = char(M_.exo_names, 'v1');
M_.exo_names_tex = char(M_.exo_names_tex, 'v1');
M_.exo_names_long = char(M_.exo_names_long, 'v1');
M_.endo_names = 'C';
M_.endo_names_tex = 'C';
M_.endo_names_long = 'C';
M_.endo_names = char(M_.endo_names, 'H');
M_.endo_names_tex = char(M_.endo_names_tex, 'H');
M_.endo_names_long = char(M_.endo_names_long, 'H');
M_.endo_names = char(M_.endo_names, 'N');
M_.endo_names_tex = char(M_.endo_names_tex, 'N');
M_.endo_names_long = char(M_.endo_names_long, 'N');
M_.endo_names = char(M_.endo_names, 'b');
M_.endo_names_tex = char(M_.endo_names_tex, 'b');
M_.endo_names_long = char(M_.endo_names_long, 'b');
M_.endo_names = char(M_.endo_names, 'B');
M_.endo_names_tex = char(M_.endo_names_tex, 'B');
M_.endo_names_long = char(M_.endo_names_long, 'B');
M_.endo_names = char(M_.endo_names, 'W');
M_.endo_names_tex = char(M_.endo_names_tex, 'W');
M_.endo_names_long = char(M_.endo_names_long, 'W');
M_.endo_names = char(M_.endo_names, 'Q');
M_.endo_names_tex = char(M_.endo_names_tex, 'Q');
M_.endo_names_long = char(M_.endo_names_long, 'Q');
M_.endo_names = char(M_.endo_names, 'r');
M_.endo_names_tex = char(M_.endo_names_tex, 'r');
M_.endo_names_long = char(M_.endo_names_long, 'r');
M_.endo_names = char(M_.endo_names, 'lambda');
M_.endo_names_tex = char(M_.endo_names_tex, 'lambda');
M_.endo_names_long = char(M_.endo_names_long, 'lambda');
M_.endo_names = char(M_.endo_names, 'disttoconstraint');
M_.endo_names_tex = char(M_.endo_names_tex, 'disttoconstraint');
M_.endo_names_long = char(M_.endo_names_long, 'disttoconstraint');
M_.endo_names = char(M_.endo_names, 'dH');
M_.endo_names_tex = char(M_.endo_names_tex, 'dH');
M_.endo_names_long = char(M_.endo_names_long, 'dH');
M_.endo_names = char(M_.endo_names, 'Pi');
M_.endo_names_tex = char(M_.endo_names_tex, 'Pi');
M_.endo_names_long = char(M_.endo_names_long, 'Pi');
M_.endo_names = char(M_.endo_names, 'Hs');
M_.endo_names_tex = char(M_.endo_names_tex, 'Hs');
M_.endo_names_long = char(M_.endo_names_long, 'Hs');
M_.endo_names = char(M_.endo_names, 'mu');
M_.endo_names_tex = char(M_.endo_names_tex, 'mu');
M_.endo_names_long = char(M_.endo_names_long, 'mu');
M_.endo_names = char(M_.endo_names, 'lagv');
M_.endo_names_tex = char(M_.endo_names_tex, 'lagv');
M_.endo_names_long = char(M_.endo_names_long, 'lagv');
M_.endo_names = char(M_.endo_names, 'auxv');
M_.endo_names_tex = char(M_.endo_names_tex, 'auxv');
M_.endo_names_long = char(M_.endo_names_long, 'auxv');
M_.param_names = 'betta';
M_.param_names_tex = 'betta';
M_.param_names_long = 'betta';
M_.param_names = char(M_.param_names, 'nu');
M_.param_names_tex = char(M_.param_names_tex, 'nu');
M_.param_names_long = char(M_.param_names_long, 'nu');
M_.param_names = char(M_.param_names, 'omega');
M_.param_names_tex = char(M_.param_names_tex, 'omega');
M_.param_names_long = char(M_.param_names_long, 'omega');
M_.param_names = char(M_.param_names, 'phi');
M_.param_names_tex = char(M_.param_names_tex, 'phi');
M_.param_names_long = char(M_.param_names_long, 'phi');
M_.param_names = char(M_.param_names, 'chi');
M_.param_names_tex = char(M_.param_names_tex, 'chi');
M_.param_names_long = char(M_.param_names_long, 'chi');
M_.param_names = char(M_.param_names, 'theta');
M_.param_names_tex = char(M_.param_names_tex, 'theta');
M_.param_names_long = char(M_.param_names_long, 'theta');
M_.param_names = char(M_.param_names, 'sigmaw');
M_.param_names_tex = char(M_.param_names_tex, 'sigmaw');
M_.param_names_long = char(M_.param_names_long, 'sigmaw');
M_.param_names = char(M_.param_names, 'rhow');
M_.param_names_tex = char(M_.param_names_tex, 'rhow');
M_.param_names_long = char(M_.param_names_long, 'rhow');
M_.param_names = char(M_.param_names, 'psi');
M_.param_names_tex = char(M_.param_names_tex, 'psi');
M_.param_names_long = char(M_.param_names_long, 'psi');
M_.param_names = char(M_.param_names, 'deltaH');
M_.param_names_tex = char(M_.param_names_tex, 'deltaH');
M_.param_names_long = char(M_.param_names_long, 'deltaH');
M_.param_names = char(M_.param_names, 'g');
M_.param_names_tex = char(M_.param_names_tex, 'g');
M_.param_names_long = char(M_.param_names_long, 'g');
M_.param_names = char(M_.param_names, 'sigmav');
M_.param_names_tex = char(M_.param_names_tex, 'sigmav');
M_.param_names_long = char(M_.param_names_long, 'sigmav');
M_.exo_det_nbr = 0;
M_.exo_nbr = 3;
M_.endo_nbr = 16;
M_.param_nbr = 12;
M_.orig_endo_nbr = 16;
M_.aux_vars = [];
M_.Sigma_e = zeros(3, 3);
M_.Correlation_matrix = eye(3, 3);
M_.H = 0;
M_.Correlation_matrix_ME = 1;
options_.block=0;
options_.bytecode=0;
options_.use_dll=0;
erase_compiled_function('largehhlearn_closed_Hprod_static');
erase_compiled_function('largehhlearn_closed_Hprod_dynamic');
M_.lead_lag_incidence = [
 0 8 24;
 1 9 0;
 0 10 0;
 0 11 0;
 2 12 0;
 3 13 0;
 4 14 25;
 5 15 0;
 0 16 26;
 0 17 0;
 0 18 0;
 0 19 0;
 0 20 0;
 6 21 0;
 0 22 0;
 7 23 27;]';
M_.nstatic = 7;
M_.nfwrd   = 2;
M_.npred   = 5;
M_.nboth   = 2;
M_.nsfwrd   = 4;
M_.nspred   = 7;
M_.ndynamic   = 9;
M_.equations_tags = {
};
M_.static_and_dynamic_models_differ = 0;
M_.exo_names_orig_ord = [1:3];
M_.maximum_lag = 1;
M_.maximum_lead = 1;
M_.maximum_endo_lag = 1;
M_.maximum_endo_lead = 1;
oo_.steady_state = zeros(16, 1);
M_.maximum_exo_lag = 0;
M_.maximum_exo_lead = 0;
oo_.exo_steady_state = zeros(3, 1);
M_.params = NaN(12, 1);
M_.NNZDerivatives = zeros(3, 1);
M_.NNZDerivatives(1) = 68;
M_.NNZDerivatives(2) = -1;
M_.NNZDerivatives(3) = -1;
M_.params( 1 ) = 0.99;
betta = M_.params( 1 );
M_.params( 2 ) = .33;
nu = M_.params( 2 );
M_.params( 3 ) = 1;
omega = M_.params( 3 );
M_.params( 4 ) = 2;
phi = M_.params( 4 );
M_.params( 5 ) = 0.5;
chi = M_.params( 5 );
M_.params( 6 ) = 0.3;
theta = M_.params( 6 );
M_.params( 7 ) = 0.01;
sigmaw = M_.params( 7 );
M_.params( 8 ) = 0.9;
rhow = M_.params( 8 );
rwedge=0.995;
M_.params( 9 ) = 10;
psi = M_.params( 9 );
M_.params( 10 ) = 0.025;
deltaH = M_.params( 10 );
M_.params( 11 ) = 0.01;
g = M_.params( 11 );
M_.params( 12 ) = 0.01;
sigmav = M_.params( 12 );
%
% SHOCKS instructions
%
make_ex_;
M_.exo_det_length = 0;
M_.Sigma_e(1, 1) = (1)^2;
M_.sigma_e_is_diagonal = 1;
load largehh_closed_Hprod_SS;
oo_.steady_state(1:length(ys_RE))=ys_RE;
options_.irf = 100;
options_.nograph = 1;
options_.nomoments = 1;
options_.order = 1;
var_list_=[];
info = stoch_simul(var_list_);
cleanup_dyn(M_.fname);
save('largehhlearn_closed_Hprod_results.mat', 'oo_', 'M_', 'options_');
if exist('estim_params_', 'var') == 1
  save('largehhlearn_closed_Hprod_results.mat', 'estim_params_', '-append');
end
if exist('bayestopt_', 'var') == 1
  save('largehhlearn_closed_Hprod_results.mat', 'bayestopt_', '-append');
end
if exist('dataset_', 'var') == 1
  save('largehhlearn_closed_Hprod_results.mat', 'dataset_', '-append');
end
if exist('estimation_info', 'var') == 1
  save('largehhlearn_closed_Hprod_results.mat', 'estimation_info', '-append');
end


disp(['Total computing time : ' dynsec2hms(toc) ]);
if ~isempty(lastwarn)
  disp('Note: warning(s) encountered in MATLAB/Octave code')
end
diary off
