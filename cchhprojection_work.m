% solve CCHH model with global projection method
%version with work

clear;
%% parameters
% The assumption here is that labor supply is governed by a quadratic term
% -1/2*h^2 in the utility function. Then labor supply in equilibrium is
% equal to h = w/C, and Y = w^2/h, so that C = Y + B - R*B(-1) = 
%model parameters
betta=1.05^-0.25;
R=1.03^0.25;
theta=0.5;

Ybar=1;
rhow=0.9;
sigw=5e-2;

%algorithm parameters
minC=1e-6;
convcrit=1e-5;
lambda=0.5;
plotfreq=10;

%% non-stochastic steady-state and output function

Css=1/(1-theta*(1-R)/(1-(1-betta*R)*theta-betta))*Ybar;
Qss=1/(1-(1-betta*R)*theta-betta)*Css;
Bss=theta*Qss;
Wbar=sqrt(Ybar*Css);

Yfun=@(X,W) 0.5*(X+sqrt(X.^2+4*W.^2)); %??

%% set up grid and nodes

%income grid
wmin=log(Wbar)-3*sigw/sqrt(1-rhow^2);
wmax=log(Wbar)+3*sigw/sqrt(1-rhow^2);
Kw=10;
grid.onlyw=linspace(wmin,wmax,Kw);

%debt grid
Bmin=Bss*0.5;
Bmax=Bss*1.1;
KB=50;
grid.onlyB=linspace(Bmin,Bmax,KB);

%overall grid
[grid.w, grid.B]=ndgrid(grid.onlyw,grid.onlyB);
grid.length=Kw*KB;
fprintf('Nodes: %i\n',grid.length);

%number of quadrature nodes
J=3;
%calculate Gauss-Hermite nodes and weights
CM      = diag(sqrt((1:(J-1))/2),1) + diag(sqrt((1:(J-1))/2),-1);
[V, L]   = eig(CM);
[zeta, ind] = sort(diag(L));
V       = V(:,ind)';
weight  = V(:,1).^2; %don't multiply by sqrt(pi) here

%for two shocks
%[zzeta1, zzeta2]=ndgrid(zeta,zeta);
%ww=weight*weight';

%% initial guess for policy function

Cold=arrayfun(@(X) Yfun(X,Wbar)-X, (R-1)*grid.B);
Qold=ones(size(grid.w))*Qss;
Qold=max(Qold,(minC+R*grid.B-exp(grid.w))/theta);

Qnew=Qold;
Cnew=Cold;
Bnew=Cold;

%test:
Xnew=ones(size(grid.w));
Bflag=zeros(size(grid.w));

%% iterate

maxiter=1e4;
h=figure;
for n=1:maxiter
    %walk through grid
    FC=griddedInterpolant(grid.w,grid.B,Cold,'linear','linear');
    FQ=griddedInterpolant(grid.w,grid.B,Qold,'linear','linear');
    for node=1:grid.length %can be replaced by parfor if grid is large
        %get grid points
        w=grid.w(node);
        Bminus=grid.B(node);
        %current period:
        Ctemp=Cold(node);
        Ytemp=exp(2*w)/Ctemp;
        Qtemp=Qold(node);
        Btemp=Ctemp-Ytemp+R*Bminus;
        if Btemp>theta*Qtemp;
            Btemp=theta*Qtemp;
        end
        Btemp=Btemp*ones(J,1);
        %next period:
        w1=rhow*w+(1-rhow)*log(Wbar)+sigw*zeta;
        C1=FC(w1,Btemp); %#ok<*PFBNS>
        %C1=max(C1,minC); %if future consumption negative, set MU very high
        Q1=FQ(w1,Btemp);
        %take expectation with quadrature
        integrand1=1./C1;
        k1=integrand1'*weight;
        integrand2=Q1./C1;
        k2=integrand2'*weight;
        %get new solution: try unconstrained first
        C=1/(betta*R*k1);
        Y=w^2/C;
        Q=C*(1+betta*k2);
        B=C-Y+R*Bminus;
        if B>theta*Qtemp %if borrowing constraint violated, try constrained
            B=theta*Qtemp; %take old policy for Q to compute B and C
            % If the constraint binds, then recompute the expectations:
            C1=FC(w1,B*ones(J,1)); %#ok<*PFBNS>
            %C1=max(C1,minC); %if future consumption negative, set MU very high
            Q1=FQ(w1,B*ones(J,1));
            %take expectation with quadrature
            integrand1=1./C1;
            k1=integrand1'*weight;
            integrand2=Q1./C1;
            k2=integrand2'*weight;
            Bflag(node)=1;
            Y=Yfun(-B+R*Bminus,exp(w));
            C=Y+B-R*Bminus;
            lamC=1-betta*R*k1*C;
            Q=C*(1+betta*k2)/(1-lamC*theta); %then update Q
            Q=max(Q,0);
        else
            Bflag(node)=0;
        end
        if Q<0 || isnan(Q) || isinf(Q)
            error('Something wrong with Q');
        end
        Qnew(node)=Q;
        Xnew(node)=B/(theta*Qtemp);
        Cnew(node)=C;
        Bnew(node)=B;
    end
    
    %measure distance to old policy function and stop if it's small
    dist=max(abs(Qnew(:)-Qold(:)));
    if dist<convcrit
        fprintf('#%i: distance: %3.3g\n',n,dist);
        break; 
    end
    %update policy function with attenuation
    Qold=(1-lambda)*Qnew+lambda*Qold;
    Cold=(1-lambda)*Cnew+lambda*Cold;
    %save cchhtemp;
    
    if mod(n,plotfreq)==0 %plot policy function
        fprintf('#%i: distance: %3.3g\n',n,dist);
        surf(grid.onlyw,grid.onlyB,Qnew'); 
        xlabel('w'); xlim([wmin wmax]);
        ylabel('B'); ylim([Bmin Bmax]);
        zlabel('Q');
        drawnow;
    end
end
close(h);

FC=griddedInterpolant(grid.w,grid.B,Cnew,'linear','linear');
FQ=griddedInterpolant(grid.w,grid.B,Qnew,'linear','linear');
FB= griddedInterpolant(grid.w,grid.B,Bnew,'linear','linear');

%% plot

figure; 
surf(grid.onlyw,grid.onlyB/Bss,Qnew'); 
xlabel('w'); xlim([wmin wmax]);
ylabel('B/Bss'); ylim([Bmin Bmax]/Bss);
zlabel('Q');
title('house price');
figure;
surf(grid.onlyw,grid.onlyB/Bss,(theta*Xnew.*Qnew)'/Bss); 
xlabel('w'); xlim([wmin wmax]);
ylabel('B/Bss'); ylim([Bmin Bmax]/Bss);
zlabel('B/Bss');
title('debt');
figure;
surf(grid.onlyw,grid.onlyB/Bss,Xnew'); 
xlabel('w'); xlim([wmin wmax]);
ylabel('B/Bss'); ylim([Bmin Bmax]/Bss);
zlabel('X');
title('debt as percent of constraint');
figure;
surf(grid.onlyw,grid.onlyB/Bss,Cnew'); 
xlabel('w'); xlim([wmin wmax]);
ylabel('B/Bss'); ylim([Bmin Bmax]/Bss);
zlabel('C');
title('consumption');

%% simulation

T=1e3;
Tpre=0;
rng(1);
wsim=zeros(1,T);
Ysim=zeros(1,T);
Qsim=zeros(1,T);
Xsim=zeros(1,T);
Bsim=zeros(1,T);
Csim=zeros(1,T);

Blag=Bss*0.7;
wlag=log(Wbar);
    
for t=[ones(1,Tpre) 1:T]
    wsim(t)=rhow*wlag+(1-rhow)*log(Wbar)+sigw*randn(1)*(t>1);
    %if t>=100&&t<=300; wsim(t)=wmin; end
    Csim(t)=FC(wsim(t),Blag);
    Qsim(t)=FQ(wsim(t),Blag);
    Ysim(t)=exp(2*wsim(t))/Csim(t);
    %Bsim(t)=FB(wsim(t),Blag);
    Bsim(t)=Csim(t)-Ysim(t)+R*Blag;
    Xsim(t)=Bsim(t)/(theta*Qsim(t));
    if Xsim(t)>1
        Xsim(t)=1;
        Bsim(t)=Qsim(t)*theta;
        Ysim(t)=Yfun(R*Blag-Bsim(t),exp(wsim(t)));
        Csim(t)=Ysim(t)-R*Blag+Bsim(t);
        if Csim(t)<0
            Csim(t)=nan;
            Bsim(t)=nan;
        end
    end
    wlag=wsim(t);
    Blag=Bsim(t);
end

figure;
subplot(2,3,1); plot(1:T,exp(wsim)/Wbar); title('wage relative to SS');
subplot(2,3,2); plot(1:T,Ysim/Ybar); title('income relative to SS');
subplot(2,3,3); plot(1:T,Csim/Css); title('consumption relative to SS');
subplot(2,3,4); plot(1:T,Qsim/Qss); title('house pricerelative to SS');
subplot(2,3,5); plot(1:T,Bsim/Bss); title('debt relative to SS');
subplot(2,3,6); plot(1:T,Xsim,1:T,ones(1,T)); title('debt as a fraction of debt limit');


fprintf('Average debt level:\t%3.3g \nAverage house price\t%3.3g\n',mean(Bsim(T-100:T)),mean(Qsim(T-100:T)));
