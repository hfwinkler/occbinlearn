% solve CCHH model with global projection method
% learning version with work

clear;
%% parameters

%model parameters
betta=1.032^-0.25;
R=1.03^0.25;
chi=0.01;

theta=0.1;
c=0;
psi=0;

Ybar=1;
rhow=0.9;
sigw=1e-2;

Hbar=1;

g=0.004;
sigq=1e-2;
rhomu=0.99;

%algorithm parameters
minC=1e-4;
convcrit=1e-4;
lambda=0.1;
plotfreq=5;
maxiter=1e4;
loadlast=0;

%% non-stochastic steady-state and output function

Css=(1-(R-1)*theta*chi/(1-betta-theta*(1-betta*R-chi*R-1)))*(Ybar-(R-1)*c);
wbar=log(sqrt(Ybar*Css));
Qss=chi/(1-(1-betta*R)*theta-betta)*Css/Hbar;
Bss=theta*Qss*Hbar+c;
Wss=Qss*Hbar-R*Bss;


%income Y=w*L as a function of w (wage) and X=B-R*Bminus+Q*(Hminus-H) (financial cash flow)
Yfun=@(X,w) 0.5*(-X+sqrt(X.^2+4*exp(2*w)));

%% set up grid and nodes

%income grid
wmin=wbar-3*sigw/sqrt(1-rhow^2);
wmax=wbar+3*sigw/sqrt(1-rhow^2);
Kw=7;
grid.onlyw=linspace(wmin,wmax,Kw);

%price grid
qmin=log(Qss*0.01);
qmax=log(Qss*2);
Kq=51;
grid.onlyq=linspace(qmin,qmax,Kq);
%grid.onlyq=log(linspace(exp(qmin),exp(qmax),Kq));

%belief grid
mumin=-.003;
mumax=.003;
Kmu=7;
grid.onlymu=linspace(mumin,mumax,Kmu);

%debt grid
Bmin=Bss*0.2;
Bmax=Bss*1.5;
KB=101;
grid.onlyB=linspace(Bmin,Bmax,KB);

%housing grid
Hmin=0.9*Hbar;
Hmax=1.1*Hbar;
KH=20;
grid.onlyH=linspace(0.9,1.1,KH);

%wealth grid (for PLM)
Wmin=exp(qmin)*Hbar-R*Bmax;
Wmax=exp(qmax)*Hbar-R*Bmin;
KW=101;
%option one:
[tempgrid.q, tempgrid.B]=ndgrid(grid.onlyq,grid.onlyB);
tempgrid.W=exp(tempgrid.q)*Hbar-R*tempgrid.B;
tempgrid.W=sort(tempgrid.W(:));
grid.onlyW=tempgrid.W(round(linspace(1,KB*Kq,KW)));
%option two:
%logWtoW=@(logW) Wmin-1*Wss+exp(logW);
%WtologW=@(W) log(W-Wmin+1*Wss);
%grid.onlyW=logWtoW(linspace(WtologW(Wmin),WtologW(Wmax),KW));
%grid.onlyW=linspace(Wmin,Wmax,KW);

%overall grid
[grid.w, grid.q, grid.mu, grid.W]=ndgrid(grid.onlyw,grid.onlyq,grid.onlymu,grid.onlyW);
grid.length=Kw*Kq*Kmu*KW;
fprintf('Nodes: %i\n',grid.length);

%number of quadrature nodes
J=5;
%calculate Gauss-Hermite nodes and weights
CM          = diag(sqrt((1:(J-1))/2),1) + diag(sqrt((1:(J-1))/2),-1);
[V, L]      = eig(CM);
[zeta, ind] = sort(diag(L));
V           = V(:,ind)';
weight      = V(:,1).^2; %don't multiply by sqrt(pi) here

%for two shocks
[zzeta1, zzeta2]=ndgrid(zeta,zeta);
ww=weight*weight';

%for three shocks
[zzzeta1, zzzeta2, zzzeta3]=ndgrid(zeta,zeta,zeta);
www=reshape(kron(weight,ww),[J J J]);

%% initial guess for PLM

%wild guess:
Hold=Hbar*ones(size(grid.W)); 
Cold=Yfun((1-R)*(exp(grid.q)*Hbar-grid.W),grid.w)+(1-R)*(exp(grid.q)*Hbar-grid.W);

%or load from file:
if loadlast
    load PLMsave;
    Hold=exp(interpn(gridold.w,gridold.q,gridold.mu,gridold.W,log(Hold),grid.w,grid.q,grid.mu,grid.W,'linear'));
    Cold=exp(interpn(gridold.w,gridold.q,gridold.mu,gridold.W,log(Cold),grid.w,grid.q,grid.mu,grid.W,'linear'));
end
gridold=grid;

Cnew=Cold;
Hnew=Hold;

Bnew=ones(size(grid.q));
Xnew=ones(size(grid.q));
Ynew=ones(size(grid.q));
troubleshooter=zeros(size(grid.q));

%% iterate

dist=Inf;
distmean=Inf;
h=figure;
for n=1:maxiter
    
    %plot policy function
    if mod(n,plotfreq)==0
        fprintf('#%i: distance: %3.3g, mean: %3.3g\n',n,dist,distmean);
        subplot(1,2,1);
        surf(grid.onlyq,grid.onlymu,squeeze(Cold(round(Kw/2),:,:,round(KW/2)))'); 
        xlabel('q'); xlim([qmin qmax]);
        ylabel('\mu'); ylim([mumin mumax]);
        zlabel('C');
        subplot(1,2,2);
        surf(grid.onlyw,grid.onlyW,squeeze(Cold(:,round(Kq/2),round(Kmu/2),:))'); 
        xlabel('w'); xlim([wmin wmax]);
        ylabel('W'); ylim([Wmin Wmax]);
        zlabel('C');
        drawnow;
    end
    
    %walk through grid
    Fc=griddedInterpolant(grid.w,grid.q,grid.mu,grid.W,log(Cold),'linear','linear');
    parfor node=1:grid.length %can be replaced by parfor if grid is large
        %get grid points
        Q=exp(grid.q(node));
        mu=grid.mu(node);
        W=grid.W(node);
        w=grid.w(node);
        %current period:
        Ctemp=Cold(node); 
        Ytemp=exp(2*w)/Ctemp;
        Htemp=Hold(node);
        Btemp=min(theta*Q*Htemp, Ctemp-Ytemp-W+Q*Htemp);
        %next period:
        w1=(1-rhow)*wbar+rhow*w+sigw*zzzeta1;
        q1=log(Q)+rhomu*mu+sigq*zzzeta2;
        mu1=rhomu*mu+g*sigq*zzzeta3;
        W1=exp(q1)*Htemp-R*Btemp;
        %prevent falling off the grid
        %w1=min(max(w1,wmin),wmax);
        %q1=min(max(q1,qmin),qmax);
        %mu1=min(max(mu1,mumin),mumax);
        W1=min(max(W1,Wmin),Wmax);
        %interpolate in t+1
        C1=exp(Fc(w1,q1,mu1,W1)); %#ok<*PFBNS>
        C1=max(C1,minC); %if future consumption negative, set MU very high
        C1=min(C1,1/minC); %to prevent divergence
        %take expectation with quadrature
        integrand1=1./C1;
        k1=integrand1(:)'*www(:);
        integrand2=exp(q1)./C1;
        k2=integrand2(:)'*www(:);
        %get new solution: try unconstrained first
        C=max(1/(betta*R*k1),minC);
        Y=exp(2*w)/C;
        H=chi/(Q/C-betta*k2);
        B=C-Y-W+Q*H;
        troubleshooter(node)=0;
        if B>theta*Q*H+c || H<0  
            if W+Ytemp+c>minC
                %solve for H, C, but not Y - slightly faster but doesn't converge for low W
                Y=Ytemp;
                k3=Y+W+c;
                k4=betta/(1-theta)*(theta*R*k1-k2/Q);
                Croots=roots([k4,1+chi-k3*k4,-k3]);
                C=Croots(2);
                if C<0; disp(Croots); end
            else    
                %solve for everything - it's a cubic equation in C
                k3=exp(2*w);
                k4=betta/(1-theta)*(theta*R*k1-k2/Q);
                Croots=roots([k4,1+chi-k4*(W+c),-k3*k4-W-c,-k3]);
                %selection of C is actually tricky! I'm not sure about it.
                C=min(Croots(Croots>0));
                %disp(find(Croots>0));
            end
            if sum(Croots>0)>1; 
                %disp(Croots); 
                troubleshooter(node)=1;
            end
            Y=exp(2*w)/C;
            C=max(C,minC);
            H=(Y+W+c-C)/Q/(1-theta);            

            if H<minC
                H=minC;
                C=Y+W+c-(1-theta)*Q*H;
                troubleshooter(node)=2;
            end
            B=theta*Q*H+c;      
        end
 
        if ~isreal(C)
            error('Something wrong with C');
        end
        if isnan(H) || isinf(H)
            error('Something wrong with H');
        end
        Cnew(node)=max(C,minC);
        Hnew(node)=max(H,minC);
        Bnew(node)=B;
        Xnew(node)=B/(theta*Q*H+c);
        Ynew(node)=Y;
    end
    
    %measure distance to old policy function and stop if it's small
    dist=max(abs([Hnew(:); Cnew(:)]-[Hold(:); Cold(:)]));
    distmean=mean(abs([Hnew(:); Cnew(:)]-[Hold(:); Cold(:)]));
    %dist=max(abs(Cnew(:)-Cold(:)));
    if dist<convcrit
        fprintf('#%i: distance: %3.3g, mean: %3.3g\n',n,dist,distmean);
        break; 
    end
    %update policy function with attenuation
    Cold=(1-lambda)*Cnew+lambda*Cold;
    Hold=(1-lambda)*Hnew+lambda*Hold;
    save PLMsave Cold Hold gridold;
    
end
close(h);

%% plot PLM

figure; 
subplot(1,2,1);
surf(grid.onlyq,grid.onlymu,squeeze(log(Hnew(round(Kw/2),:,:,round(KW/2))))'); 
xlabel('q'); xlim([qmin qmax]);
ylabel('\mu'); ylim([mumin mumax]);
zlabel('log(H)');
subplot(1,2,2);
surf(grid.onlyw,grid.onlyW,squeeze(log(Hnew(:,round(Kq/2),round(Kmu/2),:)))'); 
xlabel('w'); xlim([wmin wmax]);
ylabel('W'); ylim([Wmin Wmax]);
zlabel('log(H)');

figure; 
subplot(1,2,1);
surf(grid.onlyq,grid.onlymu,squeeze(log(Cnew(round(Kw/2),:,:,round(KW/2))))'); 
xlabel('q'); xlim([qmin qmax]);
ylabel('\mu'); ylim([mumin mumax]);
zlabel('log(C)');
subplot(1,2,2);
surf(grid.onlyw,grid.onlyW,squeeze(log(Cnew(:,round(Kq/2),round(Kmu/2),:)))'); 
xlabel('w'); xlim([wmin wmax]);
ylabel('W'); ylim([Wmin Wmax]);
zlabel('log(C)');

figure; 
subplot(1,2,1);
surf(grid.onlyq,grid.onlymu,squeeze(Bnew(round(Kw/2),:,:,round(KW/2)))'); 
xlabel('q'); xlim([qmin qmax]);
ylabel('\mu'); ylim([mumin mumax]);
zlabel('B');
subplot(1,2,2);
surf(grid.onlyw,grid.onlyW,squeeze(Bnew(:,round(Kq/2),round(Kmu/2),:))'); 
xlabel('w'); xlim([wmin wmax]);
ylabel('W'); ylim([Wmin Wmax]);
zlabel('B');

figure; 
subplot(1,2,1);
surf(grid.onlyq,grid.onlymu,squeeze(Xnew(round(Kw/2),:,:,round(KW/2)))'); 
xlabel('q'); xlim([qmin qmax]);
ylabel('\mu'); ylim([mumin mumax]);
zlabel('debt, fraction of constraint');
subplot(1,2,2);
surf(grid.onlyw,grid.onlyW,squeeze(Xnew(:,round(Kq/2),round(Kmu/2),:))'); 
xlabel('w'); xlim([wmin wmax]);
ylabel('W'); ylim([Wmin Wmax]);
zlabel('debt, fraction of constraint');

%% simulate PLM (just checking...) 

T=100;
Tpre=0;
wsim=zeros(1,T);
Qsim=zeros(1,T);
musim=zeros(1,T);
Xsim=zeros(1,T);
Wsim=zeros(1,T);
Bsim=zeros(1,T);
Hsim=zeros(1,T);
Csim=zeros(1,T);
Ysim=zeros(1,T);


FC=griddedInterpolant(grid.w,grid.q,grid.mu,grid.W,Cnew,'linear','none');
FH=griddedInterpolant(grid.w,grid.q,grid.mu,grid.W,Hnew,'linear','none'); 
FX=griddedInterpolant(grid.w,grid.q,grid.mu,grid.W,Xnew,'linear','none');

wlag=wbar;
Blag=1*Bss;
Hlag=Hbar;
mulag=0;
qlag=log(0.9*Qss);

%rng(0);

for t=[ones(1,Tpre) 1:T]
    wsim(t)=(1-rhow)*wbar+rhow*wlag+sigw*randn(1)*0;
    Qsim(t)=exp(qlag+rhomu*mulag+sigq*randn(1))*0;
    Wsim(t)=Qsim(t)*Hlag-R*Blag;
    Wsim(t)=min(max(Wsim(t),Wmin),Wmax);
    musim(t)=rhomu*mulag+g*sigq*randn(1)*0;
    %don't fall off the grid for exogenous variables
    wsim(t)=min(max(wsim(t),wmin),wmax);
    Qsim(t)=max(min(Qsim(t),exp(qmax)),exp(qmin));
    musim(t)=min(max(musim(t),mumin),mumax);
    
    Hsim(t)=FH(wsim(t),log(Qsim(t)),musim(t),Wsim(t));
    
   %use C policy function
%     Csim(t)=FC(wsim(t),log(Qsim(t)),musim(t),Wsim(t));
%     Ysim(t)=exp(wsim(t))^2/Csim(t);
%     Bsim(t)=Csim(t)-Ysim(t)+Qsim(t)*Hsim(t)-Wsim(t);
%     Xsim(t)=Bsim(t)/(theta*Qsim(t)*Hsim(t)+c);

    %use X policy function
    Xsim(t)=FX(wsim(t),log(Qsim(t)),musim(t),Wsim(t));
    Bsim(t)=(theta*Hsim(t)*Qsim(t)+c)*Xsim(t);
    Ysim(t)=Yfun(Bsim(t)+Wsim(t)-Qsim(t)*Hsim(t),wsim(t));
    Csim(t)=Ysim(t)+Bsim(t)+Wsim(t)-Qsim(t)*Hsim(t);

    Blag=Bsim(t);
    Hlag=Hsim(t);
    wlag=wsim(t);
    mulag=musim(t);
    qlag=log(Qsim(t));
end

figure;
subplot(2,4,1); plot(1:T,exp(wsim-wbar)); title('wage, fraction of SS');
subplot(2,4,2); plot(1:T,Qsim/Qss); title('house price, fraction of SS');
subplot(2,4,3); plot(1:T,Bsim/Bss); title('debt, fraction of SS');
subplot(2,4,4); plot(1:T,Xsim); title('debt as percent of constraint');
subplot(2,4,5); plot(1:T,musim); title('belief');
subplot(2,4,6); plot(1:T,Csim/Css); title('consumption, fraction of SS');
subplot(2,4,7); plot(1:T,Hsim/Hbar); title('housing, fraction of SS');
subplot(2,4,8); plot(1:T,Ysim/Ybar); title('income, fraction of SS');

%% solve for ALM

%store PLM
CPLM=Cnew;
HPLM=Hnew;
XPLM=Xnew;
%set up grid:

%switch to a debt grid, because W is endogenous now
gridALM.onlyw=grid.onlyw;
gridALM.onlymu=grid.onlymu;
gridALM.onlyB=grid.onlyB;
gridALM.onlyH=grid.onlyH;
[gridALM.w, gridALM.mu, gridALM.B, gridALM.H]=ndgrid(gridALM.onlyw,gridALM.onlymu,gridALM.onlyB,gridALM.onlyH);
gridALM.length=Kw*Kmu*KB*KH;

qALM=zeros(size(gridALM.mu));
CALM=zeros(size(gridALM.mu));
BALM=zeros(size(gridALM.mu));
HALM=zeros(size(gridALM.mu));
XALM=zeros(size(gridALM.mu));

Fc=griddedInterpolant(grid.w,grid.q,grid.mu,grid.W,log(CPLM),'linear','none');
Fh=griddedInterpolant(grid.w,grid.q,grid.mu,grid.W,log(HPLM),'linear','none');
FX=griddedInterpolant(grid.w,grid.q,grid.mu,grid.W,XPLM,'linear','none');
Fnode=griddedInterpolant(grid.w,grid.q,grid.mu,grid.W,reshape(1:grid.length,size(grid.w)),'nearest','nearest');

troubleshooter=zeros(size(gridALM.mu));
options=optimset('Display','off');
parfor node=1:gridALM.length
    w=gridALM.w(node);
    mu=gridALM.mu(node);
    Bminus=gridALM.B(node);
    Hminus=gridALM.H(node);
    fun=@(q) exp(Fh(w,q,mu,exp(q)*Hminus-R*Bminus))-Hbar*exp(psi*(q-log(Qss)));
    tryfuns=arrayfun(fun,grid.onlyq);
    signchangeidx=find(abs(diff(sign(tryfuns)))==2);
    if isempty(signchangeidx)
        q0=grid.onlyq(abs(tryfuns)==min(abs(tryfuns)));
        %q0=qmin;
        troubleshooter(node)=0.5*sign(tryfuns(1));    
        [q,fval,exitflag]=fsolve(fun,q0,optimset('Display','off'));   
        if exitflag~=1; 
            troubleshooter(node)=0.75*sign(tryfuns(1)); 
%             plot(grid.onlyq,tryfuns);
%             hold on;
%             plot(grid.onlyq,arrayfun(@(q) FX(w,q,mu,exp(q)*Hbar-R*Bminus),grid.onlyq),'r')
%             hold off;
%             keyboard;
            %q=qmin;
        end
        q=max(min(q,qmax),qmin);
    elseif length(signchangeidx)==1
        q0=grid.onlyq(signchangeidx);
        troubleshooter(node)=0;
        [q,fval,exitflag]=fzero(fun,grid.onlyq(signchangeidx+[0 1]));
    else
        idxchoice=signchangeidx(end);
        q0=grid.onlyq(idxchoice);
        troubleshooter(node)=-length(signchangeidx);
        [q,fval,exitflag]=fzero(fun,grid.onlyq(idxchoice+[0 1]));
        %plot(grid.onlyq,tryfuns);
        %hold on;
        %plot(grid.onlyq,arrayfun(@(q) FX(w,q,mu,exp(q)*Hbar-R*Bminus)),'r')
        %keyboard;
    end

    H=fval+Hbar*exp(psi*(q-log(Qss)));
    Q=exp(q);
    W=Q*Hminus-R*Bminus;
    %use C policy function
%     C=exp(Fc(w,log(Q),mu,W));
%     Y=exp(w)^2/C;
%     B=C-Y+R*Bminus+Q*(H-Hbar);
%     X=B/(theta*Q*H+c);

    %use X policy function
    X=FX(w,q,mu,W);
    B=(theta*Q*H+c)*X;
    Y=Yfun(B-Q*H+W,w);
    C=Y+B-Q*H+W;
    %troubleshooter(node)=(C-exp(Fc(w,q,mu,W)))/Css;

    %get nearest node index
    %troubleshooter(node)=Fnode(w,q,mu,W);
    %get deviation of housing demand from supply
    %troubleshooter(node)=fval;
    
    qALM(node)=log(Q);
    CALM(node)=max(C,minC);
    BALM(node)=B;
    HALM(node)=H;
    XALM(node)=X;
end

%display outcomes
figure;
hist(troubleshooter(:));

%% plot ALM

figure; 
subplot(1,2,1);
surf(gridALM.onlyw,gridALM.onlymu,qALM(:,:,round(KB/2),round(KH/2))'); 
xlabel('w'); xlim([wmin wmax]);
ylabel('\mu'); ylim([mumin mumax]);
zlabel('log(Q)');
subplot(1,2,2);
surf(gridALM.onlyB/Bss,gridALM.onlyH/Hbar,squeeze(qALM(round(Kw/2),round(Kmu/2),:,:))'); 
xlabel('B/Bss'); ylim([Bmin/Bss Bmax/Bss]);
ylabel('H/Hbar'); ylim([Hmin/Hbar Hmax/Hbar]);
zlabel('log(Q)');

figure; 
subplot(1,2,1);
surf(gridALM.onlyw,gridALM.onlymu,BALM(:,:,round(KB/2),round(KH/2))'-gridALM.B(:,:,round(KB/2),round(KH/2))'); 
xlabel('w'); xlim([wmin wmax]);
ylabel('\mu'); ylim([mumin mumax]);
zlabel('\Delta B');
subplot(1,2,2);
surf(gridALM.onlyB/Bss,gridALM.onlyH/Hbar,squeeze(BALM(round(Kw/2),round(Kmu/2),:,:)-gridALM.B(round(Kw/2),round(Kmu/2),:,:))'); 
xlabel('B/Bss'); ylim([Bmin/Bss Bmax/Bss]);
ylabel('H/Hbar'); ylim([Hmin/Hbar Hmax/Hbar]);
zlabel('\Delta B');

figure; 
subplot(1,2,1);
surf(gridALM.onlyw,gridALM.onlymu,XALM(:,:,round(KB/2),round(KH/2))'); 
xlabel('w'); xlim([wmin wmax]);
ylabel('\mu'); ylim([mumin mumax]);
zlabel('X');
subplot(1,2,2);
surf(gridALM.onlyB/Bss,gridALM.onlyH/Hbar,squeeze(XALM(round(Kw/2),round(Kmu/2),:,:))'); 
xlabel('B/Bss'); ylim([Bmin/Bss Bmax/Bss]);
ylabel('H/Hbar'); ylim([Hmin/Hbar Hmax/Hbar]);
zlabel('X');

figure; 
subplot(1,2,1);
surf(gridALM.onlyw,gridALM.onlymu,log(CALM(:,:,round(KB/2),round(KH/2)))'); 
xlabel('w'); xlim([wmin wmax]);
ylabel('\mu'); ylim([mumin mumax]);
zlabel('log(C)');
subplot(1,2,2);
surf(gridALM.onlyB/Bss,gridALM.onlyH/Hbar,squeeze(log(CALM(round(Kw/2),round(Kmu/2),:,:)))'); 
xlabel('B/Bss'); ylim([Bmin/Bss Bmax/Bss]);
ylabel('H/Hbar'); ylim([Hmin/Hbar Hmax/Hbar]);
zlabel('log(C)');

figure; 
subplot(1,2,1);
surf(gridALM.onlyw,gridALM.onlymu,troubleshooter(:,:,round(KB/2),round(KH/2))'); 
xlabel('w'); xlim([wmin wmax]);
ylabel('\mu'); ylim([mumin mumax]);
zlabel('trrrouble');
subplot(1,2,2);
surf(gridALM.onlyB/Bss,gridALM.onlyH/Hbar,squeeze(troubleshooter(round(Kw/2),round(Kmu/2),:,:))'); 
xlabel('B/Bss'); ylim([Bmin/Bss Bmax/Bss]);
ylabel('H/Hbar'); ylim([Hmin/Hbar Hmax/Hbar]);
zlabel('trrrouble');

%% simulate ALM

T=100;
Tpre=100;
wsim=zeros(1,T);
Qsim=zeros(1,T);
musim=zeros(1,T);
Xsim=zeros(1,T);
Wsim=zeros(1,T);
Bsim=zeros(1,T);
Csim=zeros(1,T);
Ysim=zeros(1,T);
Hsim=zeros(1,T);
troublesim=zeros(1,T);
Hdiagsim=zeros(Kq,T);

Fc=griddedInterpolant(grid.w,grid.q,grid.mu,grid.W,log(CPLM),'linear','none');
Fh=griddedInterpolant(grid.w,grid.q,grid.mu,grid.W,log(HPLM),'linear','none');
FX=griddedInterpolant(grid.w,grid.q,grid.mu,grid.W,XPLM,'linear','none');
Fnode=griddedInterpolant(grid.w,grid.q,grid.mu,grid.W,reshape(1:grid.length,size(grid.w)),'nearest','nearest');

wlag=wbar;
Blag=0.7*Bss;
Hlag=Hbar;
mulag=0;
qlag=interpn(gridALM.w,gridALM.mu,gridALM.B,gridALM.H,qALM,wlag,mulag,Blag,Hlag);


rng(0);

for t=[ones(1,Tpre) 1:T]
    %solve for the price    
    wsim(t)=(1-rhow)*wbar+rhow*wlag+sigw*randn(1)*(t>1);
    wsim(t)=max(min(wsim(t),wmax),wmin);
    %if t>=50 && t<=60; wsim(t)=wmin; end
    fun=@(q) exp(Fh(wsim(t),q,mulag,exp(q)*Hlag-R*Blag))-Hbar*exp(psi*(q-log(Qss)));
    tryfuns=arrayfun(fun,grid.onlyq);
    Hdiagsim(:,t)=tryfuns;
    signchangeidx=find(abs(diff(sign(tryfuns)))==2);
    if isempty(signchangeidx)
        troublesim(t)=.5*sign(tryfuns(1));    
        q0=grid.onlyq(abs(tryfuns)==min(abs(tryfuns)));
        %q0=log(Qss);
        [q,fval,exitflag]=fsolve(fun,q0,optimset('Display','off'));   
        if exitflag~=1; 
            troublesim(t)=0.75*sign(tryfuns(1)); 
%             plot(grid.onlyq,tryfuns);
%             hold on;
%             plot(grid.onlyq,arrayfun(@(q) FX(w,q,mu,exp(q)*Hbar-R*Bminus),grid.onlyq),'r')
%             hold off;
%             keyboard;
            %q=qmin; 
        end
    elseif length(signchangeidx)==1
        q0=grid.onlyq(signchangeidx);
        troublesim(t)=0;
        [q,fval,exitflag]=fzero(fun,grid.onlyq(signchangeidx+[0 1]));
    else
        idxchoice=signchangeidx(end);
        q0=grid.onlyq(idxchoice);
        troublesim(t)=-length(signchangeidx);
        [q,fval,exitflag]=fzero(fun,grid.onlyq(idxchoice+[0 1]));
    end
    
    Qsim(t)=exp(q);
    Wsim(t)=Qsim(t)*Hlag-R*Blag;
    Hsim(t)=Hbar*exp(psi*(q-log(Qss)))+fval;
    
    %use C policy function
%     Csim(t)=exp(Fc(wsim(t),q,mulag,Wsim(t)));
%     Ysim(t)=exp(2*wsim(t))/Csim(t);
%     Bsim(t)=Csim(t)-Ysim(t)+R*Blag+Qsim(t)*(Hsim(t)-Hbar);
%     Xsim(t)=Bsim(t)/(theta*Qsim(t)*Hsim(t)+c);
%     %troublesim(t)=Xsim(t)-FX(wsim(t),q,mulag,Wsim(t));

    %use X policy function
    Xsim(t)=FX(wsim(t),q,mulag,Wsim(t));
    Bsim(t)=(theta*Qsim(t)*Hsim(t)+c)*Xsim(t);
    Ysim(t)=Yfun(Bsim(t)-Qsim(t)*Hsim(t)+Wsim(t),wsim(t));
    Csim(t)=Ysim(t)+Bsim(t)-Qsim(t)*Hsim(t)+Wsim(t);
    %troublesim(t)=(Csim(t)-exp(Fc(wsim(t),q,mulag,Wsim(t))))/Css;

    %get nearest node number
    %troublesim(t)=Fnode(wsim(t),q,mulag,Wsim(t));
    %compare with price computed previously
    %troublesim(t)=q-interpn(gridALM.w,gridALM.mu,gridALM.B,qALM,wsim(t),mulag,Blag);
    %get deviation from housing market clearing
    %troublesim(t)=fval;
    
    musim(t)=(rhomu-g)*mulag+g*(q-qlag)*(t>1);
    musim(t)=max(min(musim(t),mumax),mumin);

    
    wlag=wsim(t);
    Blag=Bsim(t); 
    Hlag=Hsim(t);
    %Blag=min(max(Blag,Bmin),Bmax);
    mulag=musim(t);
    qlag=q;
    
end

figure;
subplot(2,4,1); plot(1:T,exp(wsim-wbar)); title('wage, fraction of SS');
subplot(2,4,2); plot(1:T,Qsim/Qss); title('house price, fraction of SS');
subplot(2,4,3); plot(1:T,Bsim/Bss); title('debt, fraction of SS');
subplot(2,4,4); plot(1:T,Xsim); title('debt, fraction of debt limit');
subplot(2,4,5); plot(1:T,musim); title('belief');
subplot(2,4,6); plot(1:T,Csim/Css); title('consumption, fraction of SS');
subplot(2,4,7); plot(1:T,Ysim/Ybar); title('income, fraction of SS');
subplot(2,4,8); plot(1:T,troublesim); title('trouble');

figure;
plot(1:T,Hsim/Hbar); title('housing, fraction of SS');
figure;
surf(1:T,exp(grid.onlyq),min(max(Hdiagsim,-1),1)); 
hold on;
plot3(1:T,Qsim,zeros(1,T),'LineWidth',3,'Color','red');
