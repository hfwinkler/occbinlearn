% solve twoagents model with global projection method


%% parameters

%model parameters from file
twoagents_parameters;

rhoq=0.9;
sigmaq=1e-2;

%algorithm parameters
minC=1e-6;
convcrit=1e-4;
lambda=0;
plotfreq=1;
loadprevious=0;
n_stable=Inf; %after these many iterations, fix order of trying constrained/unconstrained

%% steady state from Dynare calculation (run it first!)
load twoagents_SS;
for i=1:M_RE.endo_nbr
    SS.(deblank(M_RE.endo_names(i,:)))=oo_RE.dr.ys(i);
    jdx.(deblank(M_RE.endo_names(i,:)))=oo_RE.dr.inv_order_var(i);
end

%% set up grid and nodes

%house price grid
qbar=SS.Q;
qmin=qbar-3*sigmaq/sqrt(1-rhoq^2);
qmax=qbar+3*sigmaq/sqrt(1-rhoq^2);
Kq=5;
grid.onlyq=linspace(qmin,qmax,Kq);

%debt grid
Bmin=SS.B*0.5;
Bmax=SS.B*1.2;
KB=11;
grid.onlyB=linspace(Bmin,Bmax,KB);

%borrower housing grid
Hbmin=max(exp(SS.Hb)-0.2,.01*Hbar);
Hbmax=min(exp(SS.Hb)+0.2,.99*Hbar);
KHb=11;
grid.onlyHb=linspace(Hbmin,Hbmax,KHb);

%saver housing grid
Hsmin=max(exp(SS.Hs)-0.2,.01*Hbar);
Hsmax=min(exp(SS.Hs)+0.2,.99*Hbar);
KHs=11;
grid.onlyHs=linspace(Hsmin,Hsmax,KHs);

%overall grid
[grid.q, grid.B, grid.Hb, grid.Hs]=ndgrid(grid.onlyq,grid.onlyB,grid.onlyHb,grid.onlyHs);
grid.length=Kq*KB*KHb*KHs;
fprintf('Nodes: %i\n',grid.length);

%number of quadrature nodes
J=3;
%calculate Gauss-Hermite nodes and weights
CM      = diag(sqrt((1:(J-1))/2),1) + diag(sqrt((1:(J-1))/2),-1);
[V, L]   = eig(CM);
[zeta, ind] = sort(diag(L));
V       = V(:,ind)';
weights = V(:,1).^2; %don't multiply by sqrt(pi) here

%for two shocks
%[zzeta1, zzeta2]=ndgrid(zeta,zeta);
%ww=w*w';

%% initial guess for policy function:

%simple guess
Hb0=exp(SS.Hb)*ones(size(grid.q));
Hs0=Hbar-Hb0;
B0=SS.B*ones(size(grid.q));
Cb0=exp(SS.Cb)*ones(size(grid.q));
Cs0=exp(SS.Cs)*ones(size(grid.q));

Csnew=Cs0;
Cbnew=Cb0;
Hbnew=Hb0;
Hsnew=Hs0;
Bnew=B0;

%load previous solution?
if loadprevious
    load twoagents_polfuns;
    %interpolate to new grid
    Csnew=interpn(savedgrid.q,savedgrid.B,savedgrid.Hb,savedgrid.Hs,Csnew,grid.q,grid.B,grid.Hb,grid.Hs);
    Cbnew=interpn(savedgrid.q,savedgrid.B,savedgrid.Hb,savedgrid.Hs,Cbnew,grid.q,grid.B,grid.Hb,grid.Hs);
    Hbnew=interpn(savedgrid.q,savedgrid.B,savedgrid.Hb,savedgrid.Hs,Hbnew,grid.q,grid.B,grid.Hb,grid.Hs);
    Hsnew=interpn(savedgrid.q,savedgrid.B,savedgrid.Hb,savedgrid.Hs,Hsnew,grid.q,grid.B,grid.Hb,grid.Hs);
    Bnew=interpn(savedgrid.q,savedgrid.B,savedgrid.Hb,savedgrid.Hs,Bnew,grid.q,grid.B,grid.Hb,grid.Hs);
end

Csold=Csnew;
Cbold=Cbnew;
Hbold=Hbnew;
Hsold=Hsnew;
Bold=Bnew;

multiplier=zeros(size(grid.q));
Rnew=zeros(size(grid.q));
tryunconfirst=zeros(size(grid.q));

%load intermittent results from previous run?
%load twoagentstemp;

%% iterate

%helper functions
Ybfun=@(Cb,w) exp(w)*(exp(w)/eta*Cb^-gama)^(1/phi);
Ysfun=@(Cs,w) exp(w)*(exp(w)/etas*Cs^-gamas)^(1/phis);
%for use with fsolve objective:
clear twoagents_qexog_fsolvefun
params.gama=gama;
params.gamas=gamas;
params.betta=betta;
params.bettas=bettas;
params.nu=nu;
params.nus=nus;
params.eta=eta;
params.etas=etas;
params.phi=phi;
params.phis=phis;
params.theta=theta;
params.chi=chi;
params.minC=minC;

maxiter=1e3;
options=optimset('Display','off','MaxFunEvals',1000,'TolFun',1e-5,'Algorithm','trust-region-dogleg','Jacobian','on','DerivativeCheck','off');
h=figure;

for n=1:maxiter
    %walk through grid
    FCs=griddedInterpolant(grid.q,grid.B,grid.Hb,grid.Hs,Csold,'linear','linear');
    FCb=griddedInterpolant(grid.q,grid.B,grid.Hb,grid.Hs,Cbold,'linear','linear');
    parfor node=1:grid.length %can be replaced by parfor if grid is large
        %get grid points
        Q=exp(grid.q(node));
        Bminus=grid.B(node);
        Hbminus=grid.Hb(node);
        Hsminus=grid.Hs(node);
        %current period:
        Btemp=Bold(node);
        Hbtemp=Hbold(node);
        Hstemp=Hsold(node);
        %prevent falling off the grid?
        Btemp=max(min(Btemp,Bmax),Bmin);
        Hbtemp=max(min(Hbtemp,Hbmax),Hbmin);
        Hstemp=max(min(Hstemp,Hsmax),Hsmin);
        %next period:
        q1=rhoq*log(Q)+(1-rhoq)*qbar+sigmaq*zeta;
        Cs1=FCs(q1,Btemp*ones(J,1),Hbtemp*ones(J,1),Hstemp*ones(J,1)); %#ok<*PFBNS>
        Cb1=FCb(q1,Btemp*ones(J,1),Hbtemp*ones(J,1),Hstemp*ones(J,1)); %#ok<*PFBNS>
        Cs1=max(Cs1,minC); %if future consumption negative, set MU very high
        Cb1=max(Cb1,minC); %if future consumption negative, set MU very high
        %take expectation with quadrature
        k=zeros(4,1);
        integrand1=Cs1.^-gamas;
        k(1)=sum(integrand1.*weights);
        integrand2=exp(q1).*Cs1.^-gamas;
        k(2)=sum(integrand2.*weights);
        integrand3=Cb1.^-gama;
        k(3)=sum(integrand3.*weights);
        integrand4=exp(q1).*Cb1.^-gama;
        k(4)=sum(integrand4.*weights);
        %pack states for objective function
        states=struct;
        states.Hbminus=Hbminus;
        states.Hsminus=Hsminus;
        states.Bminus=Bminus;
        states.Q=Q;
        states.k=k;
        confun=@(cscb) twoagents_qexog_fsolvefun(cscb,states,params,1);
        unconfun=@(cscb) twoagents_qexog_fsolvefun(cscb,states,params,0);
        if tryunconfirst(node)
            [solution,funval]=fsolve(unconfun,log([Csold(node), Cbold(node)]),options); %try unconstrained first
%             if max(abs(funval))>convcrit %sometimes the below helps
%                 solution=fsolve(confun,solution,options); 
%                 [solution,funval]=fsolve(unconfun,solution,options);
%                 if max(abs(funval))>convcrit; error('Cannot find unconstrained solution'); end
%             end
            Cs=exp(solution(1)); Cb=exp(solution(2));
            [~,~,Hs,Hb]=unconfun(solution);
            R=1/(bettas*Cs^gamas*k(1));
            B=(Cb-Ybfun(Cb,0)+Q*(Hb-Hbminus)+Bminus)*R;
            multiplier(node)=0;
            if B>theta*Q*Hb*R %then try constrained
                [solution,funval]=fsolve(confun,log([Csold(node), Cbold(node)]),options);
%                 if max(abs(funval))>convcrit %maybe this helps?
%                     [solution,funval]=fsolve(confun,log([Csold(node), Cbold(node)]),options); 
%                     if max(abs(funval))>convcrit; error('Unconstrained solution violates borrowing constraint, constrained could not be found'); end
%                 end
                Cs=exp(solution(1)); Cb=exp(solution(2));
                [~,~,Hs,Hb]=confun(solution);
                R=1/(bettas*Cs^gamas*k(1));
                Q=Cs^gamas*(Hs^-nus + bettas*k(2));
                B=theta*Q*Hb*R;
                multiplier(node)=1-betta*Cb^gama*k(3)*R;
                %this condition is too much to ask, because due to
                %numerical error it can be violated along the way
                %if multiplier(node)<0; disp(multiplier(node)); error('Neither constrained nor unconstrained works'); end
            end
        else
            [solution,funval]=fsolve(confun,log([Csold(node), Cbold(node)]),options); %try constrained first
%             if max(abs(funval))>convcrit %sometimes the below helps
%                 solution=fsolve(unconfun,solution,options); 
%                 [solution,funval]=fsolve(confun,solution,options);
%                 if max(abs(funval))>convcrit; error('Cannot find constrained solution'); end
%             end
            Cs=exp(solution(1)); Cb=exp(solution(2));
            [~,~,Hs,Hb]=confun(solution);
            R=1/(bettas*Cs^gamas*k(1));
            Q=Cs^gamas*(Hs^-nus + bettas*k(2));
            B=theta*Q*Hb*R;
            multiplier(node)=1-betta*Cb^gama*k(3)*R;
            if multiplier(node)<0 %then try unconstrained
                [solution,funval]=fsolve(unconfun,log([Csold(node), Cbold(node)]),options);
%                 if max(abs(funval))>convcrit %maybe this helps?
%                     [solution,funval]=fsolve(unconfun,log([Csold(node), Cbold(node)]),options); 
%                     if max(abs(funval))>convcrit; error('Constrained solution violates borrowing constraint, unconstrained could not be found'); end
%                 end
                Cs=exp(solution(1)); Cb=exp(solution(2));
                [~,~,Hs,Hb]=unconfun(solution);
                R=1/(bettas*Cs^gamas*k(1));
                B=(Cb-Ybfun(Cb,0)+Q*(Hb-Hbminus)+Bminus)*R;
                multiplier(node)=0;
                %this condition is too much to ask, because due to
                %numerical error it can be violated along the way
                %if B>theta*Q*Hb*R; error('Neither constrained nor unconstrained works'); end
            end
        end
        %update policy functions
        Csnew(node)=Cs;
        Cbnew(node)=Cb;
        Hbnew(node)=Hb;
        Hsnew(node)=Hs;
        Bnew(node)=B;
        Rnew(node)=R;
    end
    
    %measure distance to old policy function and stop if it's small
    maxdist=max(abs(Cbnew(:)-Cbold(:)));
    meandist=mean(abs(Cbnew(:)-Cbold(:)));
    if maxdist<convcrit
        fprintf('#%i: max distance: %3.3g, mean distance: %3.3g\n',n,maxdist,meandist);
        savedgrid=grid;
        save twoagents_polfuns Csnew Cbnew Hbnew Hsnew Bnew Rnew multiplier savedgrid;
        break; 
    end
    %update policy functions with attenuation
    Csold=(1-lambda)*Csnew+lambda*Csold;
    Cbold=(1-lambda)*Cbnew+lambda*Cbold;
    Hbold=(1-lambda)*Hbnew+lambda*Hbold;
    Hsold=(1-lambda)*Hsnew+lambda*Hsold;
    Bold=(1-lambda)*Bnew+lambda*Bold;
    disp(sum((multiplier(:)==0)~=tryunconfirst(:)));
    %disp(find(abs(Cbnew(:)-Cbold(:))==max(abs(Cbnew(:)-Cbold(:)))));
    if n<n_stable
        tryunconfirst=(multiplier==0);
    end
    save twoagentstemp Csold Cbold Hbold Hsold Bold multiplier tryunconfirst savedgrid;
    
    if mod(n,plotfreq)==0 %plot policy function
        fprintf('#%i: max distance: %3.3g, mean distance: %3.3g\n',n,maxdist,meandist);
        subplot(1,2,1);
        surf(grid.onlyq,grid.onlyB,Cbnew(:,:,round(KHb/2),round(KHs/2))'); 
        zlabel('C_b');
        xlabel('log(Q)'); xlim([qmin qmax]);
        ylabel('B'); ylim([Bmin Bmax]);

        subplot(1,2,2);
        surf(grid.onlyHb,grid.onlyHs,squeeze(Cbnew(round(Kq/2),round(KB/2),:,:))'); 
        zlabel('C_b');
        xlabel('H_b'); xlim([Hbmin Hbmax]);
        ylabel('H_s'); ylim([Hsmin Hsmax]);
        
        drawnow;
    end
end
close(h);

%% plot

figure; 
subplot(1,2,1);
surf(grid.onlyq,grid.onlyB,Cbnew(:,:,round(KHb/2),round(KHs/2))'); 
zlabel('C_b');
xlabel('log(Q)'); xlim([qmin qmax]);
ylabel('B'); ylim([Bmin Bmax]);
title('borrower consumption');
subplot(1,2,2);
surf(grid.onlyHb,grid.onlyHs,squeeze(Cbnew(round(Kq/2),round(KB/2),:,:))'); 
zlabel('C_b');
xlabel('H_b'); xlim([Hbmin Hbmax]);
ylabel('H_s'); ylim([Hsmin Hsmax]);
title('borrower consumption');

figure; 
subplot(1,2,1);
surf(grid.onlyq,grid.onlyB,Csnew(:,:,round(KHb/2),round(KHs/2))'); 
zlabel('C_s');
xlabel('log(Q)'); xlim([qmin qmax]);
ylabel('B'); ylim([Bmin Bmax]);
title('saver consumption');
subplot(1,2,2);
surf(grid.onlyHb,grid.onlyHs,squeeze(Csnew(round(Kq/2),round(KB/2),:,:))'); 
zlabel('C_s');
xlabel('H_b'); xlim([Hbmin Hbmax]);
ylabel('H_s'); ylim([Hsmin Hsmax]);
title('saver consumption');

figure; 
subplot(1,2,1);
surf(grid.onlyq,grid.onlyB,Hbnew(:,:,round(KHb/2),round(KHs/2))'); 
zlabel('H_b');
xlabel('log(Q)'); xlim([qmin qmax]);
ylabel('B'); ylim([Bmin Bmax]);
title('borrower housing');
subplot(1,2,2);
surf(grid.onlyHb,grid.onlyHs,squeeze(Hbnew(round(Kq/2),round(KB/2),:,:))'); 
zlabel('H_b');
xlabel('H_b'); xlim([Hbmin Hbmax]);
ylabel('H_s'); ylim([Hsmin Hsmax]);
title('borrower housing');

figure; 
subplot(1,2,1);
surf(grid.onlyq,grid.onlyB,Hsnew(:,:,round(KHb/2),round(KHs/2))'); 
zlabel('H_s');
xlabel('log(Q)'); xlim([qmin qmax]);
ylabel('B'); ylim([Bmin Bmax]);
title('saver housing');
subplot(1,2,2);
surf(grid.onlyHb,grid.onlyHs,squeeze(Hsnew(round(Kq/2),round(KB/2),:,:))'); 
zlabel('H_s');
xlabel('H_b'); xlim([Hbmin Hbmax]);
ylabel('H_s'); ylim([Hsmin Hsmax]);
title('saver housing');

figure; 
subplot(1,2,1);
surf(grid.onlyq,grid.onlyB,Rnew(:,:,round(KHb/2),round(KHs/2))'); 
zlabel('R');
xlabel('log(Q)'); xlim([qmin qmax]);
ylabel('B'); ylim([Bmin Bmax]);
title('interest rate');
subplot(1,2,2);
surf(grid.onlyHb,grid.onlyHs,squeeze(Rnew(round(Kq/2),round(KB/2),:,:))'); 
zlabel('R');
xlabel('H_b'); xlim([Hbmin Hbmax]);
ylabel('H_s'); ylim([Hsmin Hsmax]);
title('interest rate');

figure; 
subplot(1,2,1);
surf(grid.onlyq,grid.onlyB,Bnew(:,:,round(KHb/2),round(KHs/2))'); 
zlabel('B');
xlabel('log(Q)'); xlim([qmin qmax]);
ylabel('B'); ylim([Bmin Bmax]);
title('debt');
subplot(1,2,2);
surf(grid.onlyHb,grid.onlyHs,squeeze(Bnew(round(Kq/2),round(KB/2),:,:))'); 
zlabel('B');
xlabel('H_b'); xlim([Hbmin Hbmax]);
ylabel('H_s'); ylim([Hsmin Hsmax]);
title('debt');

figure; 
subplot(1,2,1);
surf(grid.onlyq,grid.onlyB,multiplier(:,:,round(KHb/2),round(KHs/2))'); 
zlabel('\lambda');
xlabel('log(Q)'); xlim([qmin qmax]);
ylabel('B'); ylim([Bmin Bmax]);
title('multiplier');
subplot(1,2,2);
surf(grid.onlyHb,grid.onlyHs,squeeze(multiplier(round(Kq/2),round(KB/2),:,:))'); 
zlabel('\lambda');
xlabel('H_b'); xlim([Hbmin Hbmax]);
ylabel('H_s'); ylim([Hsmin Hsmax]);
title('multiplier');

%% simulation

T=1e3;
Tpre=1;
rng(10);

qsim=zeros(1,T);
Bsim=zeros(1,T);
Cbsim=zeros(1,T);
Cssim=zeros(1,T);
Hbsim=zeros(1,T);
Hssim=zeros(1,T);
Rsim=zeros(1,T);

multsim=zeros(1,T);

Blag=Bmax;
Hblag=exp(SS.Hb);
Hslag=exp(SS.Hs);
qlag=SS.Q;

FCs=griddedInterpolant(grid.q,grid.B,grid.Hb,grid.Hs,Csnew,'linear','linear');
FCb=griddedInterpolant(grid.q,grid.B,grid.Hb,grid.Hs,Cbnew,'linear','linear');
FHb=griddedInterpolant(grid.q,grid.B,grid.Hb,grid.Hs,Hbnew,'linear','linear');
FHs=griddedInterpolant(grid.q,grid.B,grid.Hb,grid.Hs,Hsnew,'linear','linear');
FR= griddedInterpolant(grid.q,grid.B,grid.Hb,grid.Hs,Rnew,'linear','linear');

Fmult=griddedInterpolant(grid.q,grid.B,grid.Hb,grid.Hs,multiplier,'linear','linear');

for t=[ones(1,Tpre) 1:T]
    qsim(t)=rhoq*qlag+(1-rhoq)*qbar+1*sigmaq*randn(1)*(t>1);
    Cssim(t)=FCs(qsim(t),Blag,Hblag,Hslag);
    Cbsim(t)=FCb(qsim(t),Blag,Hblag,Hslag);
    Hbsim(t)=FHb(qsim(t),Blag,Hblag,Hslag);
    Hssim(t)=FHs(qsim(t),Blag,Hblag,Hslag);
    Rsim(t)=FR(qsim(t),Blag,Hblag,Hslag);
    Bsim1=(Cbsim(t)-Ybfun(Cbsim(t),0)+Blag+exp(qsim(t))*(Hbsim(t)-Hblag))*Rsim(t);
    Bsim2=-(Cssim(t)-Ysfun(Cssim(t),0)-Blag+exp(qsim(t))*(Hssim(t)-Hslag))*Rsim(t);
    Bsim(t)=Bsim1;
    multsim(t)=Fmult(qsim(t),Blag,Hblag,Hslag);

    qlag=qsim(t);
    Blag=min(max(Bsim(t),Bmin),Bmax);
    Hblag=min(max(Hbsim(t),Hbmin),Hbmax);
    Hslag=min(max(Hssim(t),Hsmin),Hsmax);
end

figure;
subplot(2,4,1); plot(1:T,multsim); title('multiplier');
subplot(2,4,2); plot(1:T,qsim-qbar); title('Q (SS log dev)');
subplot(2,4,3); plot(1:T,log(Rsim)-log(1+SS.r)); title('R (SS log dev)');
subplot(2,4,4); plot(1:T,log(Bsim/SS.B)); title('B (SS log dev)');
subplot(2,4,5); plot(1:T,log(Hbsim)-SS.Hb); title('H_b (SS log dev)');
subplot(2,4,6); plot(1:T,log(Cbsim)-SS.Cb); title('C_b (SS log dev)');
subplot(2,4,7); plot(1:T,log(Cssim)-SS.Cs); title('C_s (SS log dev)');
subplot(2,4,8); plot(1:T,Bsim./(exp(qsim).*Hbsim.*Rsim),1:T,ones(1,T)*theta); title('debt-to-value ratio');


