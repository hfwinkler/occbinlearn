% solve CCHH model with global projection method
% learning version with work, PLM only, unconstrained!

clear;
%% parameters

%model parameters
betta=1.03^-0.25;
R=1.03^0.25;
theta=0.5;

Ybar=1;
Hbar=1;

sigq=0;

%algorithm parameters
minC=1e-6;
minH=1e-6;
convcrit=1e-5;
lambda=0;
plotfreq=10;

%% non-stochastic steady-state and output function

Css=1/(1-theta*(1-R)/(1-(1-betta*R)*theta-betta))*Ybar;
Qss=1/(1-(1-betta*R)*theta-betta)*Css;
Bss=theta*Qss;
Wss=Qss*Hbar-R*Bss;
wbar=sqrt(Ybar*Css);

%income Y=w*L as a function of w (wage) and X=B-R*Bminus+Q*(Hminus-H) (financial cash flow)
Yfun=@(X,w) 2*w.^2./(X+sqrt(X.^2+4*w.^2));

%% set up grid and nodes

%price grid
qmin=log(Qss*0.01);
qmax=log(Qss*100);
Kq=20;
grid.onlyq=linspace(qmin,qmax,Kq);

%debt grid (for PLM we use wealth, but for ALM we use debt as the state)
Bmin=Bss*0;
Bmax=Bss*1;
KB=20;
grid.onlyB=linspace(Bmin,Bmax,KB);

%wealth grid set to encompass debt grid with H=Hbar
Wmin=exp(qmin)*Hbar-R*Bmax-1*Wss;
Wmax=exp(qmax)*Hbar-R*Bmin+1*Wss;
KW=KB;
logWtoW=@(logW) Wmin-1*Wss+exp(logW);
WtologW=@(W) log(W-Wmin+1*Bss);
grid.onlyW=logWtoW(linspace(WtologW(Wmin),WtologW(Wmax),KW));

%overall grid
[grid.q, grid.W]=ndgrid(grid.onlyq,grid.onlyW);
grid.length=Kq*KW;
fprintf('Nodes: %i\n',grid.length);

%number of quadrature nodes
J=5;
%calculate Gauss-Hermite nodes and weights
CM      = diag(sqrt((1:(J-1))/2),1) + diag(sqrt((1:(J-1))/2),-1);
[V, L]   = eig(CM);
[zeta, ind] = sort(diag(L));
V       = V(:,ind)';
weight  = V(:,1).^2; %don't multiply by sqrt(pi) here

%% initial guess for PLM

%wild guess:
Hold=Hbar*ones(size(grid.W)); 
Cold=Yfun((1-R)*(exp(grid.q)*Hbar-grid.W),wbar)+(1-R)*(exp(grid.q)*Hbar-grid.W);

%or load from file:
% load PLMsave;
% Hold=interpn(gridold.q,gridold.W,Hold,grid.q,grid.W,'linear');
% Cold=interpn(gridold.q,gridold.W,Cold,grid.q,grid.W,'linear');

Cnew=Cold;
Hnew=Hold;

Bnew=ones(size(grid.q));
Ynew=ones(size(grid.q));

%% iterate

maxiter=1e4;
dist=nan;
h=figure;
for n=1:maxiter
    
    %plot policy function
    if mod(n,plotfreq)==1
        fprintf('#%i: distance: %3.3g\n',n,dist);
        surf(grid.onlyq,grid.onlyW,log(Hold')); 
        xlabel('q'); xlim([qmin qmax]);
        ylabel('W'); ylim([Wmin Wmax]);
        zlabel('log(H)');
        drawnow;
    end
    
    %walk through grid
    Fc=griddedInterpolant(grid.q,grid.W,log(Cold),'linear','linear');
    for node=1:grid.length %can be replaced by parfor if grid is large
        %get grid points
        Q=exp(grid.q(node));
        W=grid.W(node);
        %current period:
        Ctemp=Cold(node); 
        Ytemp=wbar^2/Ctemp;
        Htemp=Hold(node);
        Btemp=Ctemp-Ytemp-W+Q*Htemp;
        %next period:
        q1=log(Q)+sigq*zeta;
        W1=exp(q1)*Htemp-R*Btemp;
        C1=exp(Fc(q1,W1)); %#ok<*PFBNS>
        C1=max(C1,minC); %if future consumption negative, set MU very high
        %C1=min(C1,1/minC); %to prevent divergence
        %take expectation with quadrature
        integrand1=1./C1;
        k1=sum(integrand1.*weight);
        integrand2=exp(q1)./C1;
        k2=sum(integrand2.*weight);
        C=1/(betta*R*k1);
        Y=wbar^2/C;
        H=1/(Q/C-betta*k2);
        B=C-Y-W+Q*H;
        if ~isreal(C)
            error('Something wrong with C');
        end
        if isnan(H) || isinf(H)
            error('Something wrong with H');
        end
        Cnew(node)=max(C,minC);
        Hnew(node)=max(H,minH);
        Bnew(node)=B;
        Ynew(node)=Y;
    end
    
    %measure distance to old policy function and stop if it's small
    dist=max(abs([Hnew(:); Cnew(:)]-[Hold(:); Cold(:)]));
    if dist<convcrit
        fprintf('#%i: distance: %3.3g\n',n,dist);
        break; 
    end
    %update policy function with attenuation
    Cold=(1-lambda)*Cnew+lambda*Cold;
    Hold=(1-lambda)*Hnew+lambda*Hold;
    %save cchhtemp;
    
end
close(h);

gridold=grid;
save PLMsave Hold Cold gridold;

%% plot PLM

figure; 
surf(grid.onlyq,grid.onlyW,Hnew'); 
hold on;
surf(grid.onlyq,grid.onlyW,Hbar*ones(KW,Kq),'FaceAlpha',0.33);
xlabel('q'); xlim([qmin qmax]);
ylabel('W'); ylim([Wmin Wmax]);
zlabel('H');
title('housing (PLM)');

figure; 
surf(grid.onlyq,grid.onlyW,Cnew'); 
xlabel('q'); xlim([qmin qmax]);
ylabel('W'); ylim([Wmin Wmax]);
zlabel('C');
title('consumption (PLM)');

figure; 
surf(grid.onlyq,grid.onlyW,Bnew'); 
xlabel('q'); xlim([qmin qmax]);
ylabel('W'); ylim([Wmin Wmax]);
zlabel('B');
title('debt (PLM)');


%% simulate PLM (just checking...) 

T=5e3;
Tpre=0;
Qsim=zeros(1,T);
Wsim=zeros(1,T);
Bsim=zeros(1,T);
Hsim=zeros(1,T);
Csim=zeros(1,T);
Ysim=zeros(1,T);
diagsim=zeros(1,T);

%THIS IS IMPORTANT - not logging the policy functions is highly inaccurate,
Fc=griddedInterpolant(grid.q,grid.W,log(Cnew),'linear','linear');
Fh=griddedInterpolant(grid.q,grid.W,log(Hnew),'linear','linear');
FB=griddedInterpolant(grid.q,grid.W,Bnew,'spline','spline');

Blag=0.5*Bss;
Hlag=Hbar;
qlag=log(1.5*Qss);
    
%rng(0);

for t=[ones(1,Tpre) 1:T]
    Qsim(t)=exp(qlag+sigq*randn(1));
    Wsim(t)=Qsim(t)*Hlag-R*Blag;
    Hsim(t)=exp(Fh(log(Qsim(t)),Wsim(t)));
   %use C policy function
    Csim(t)=exp(Fc(log(Qsim(t)),Wsim(t)));
    Ysim(t)=wbar^2/Csim(t);
    Bsim(t)=Csim(t)-Ysim(t)+Qsim(t)*Hsim(t)-Wsim(t);
%     %use B policy function
%     Bsim(t)=FB(log(Qsim(t)),Wsim(t));
%     Ysim(t)=Yfun(Bsim(t)+Wsim(t)-Qsim(t)*Hsim(t),wbar);
%     Csim(t)=Ysim(t)+Bsim(t)+Wsim(t)-Qsim(t)*Hsim(t);
    diagsim(t)=Bsim(t)/(theta*Qsim(t)*Hsim(t));
    Blag=Bsim(t);
    Hlag=Hsim(t);
    qlag=log(Qsim(t));
end
disp(mean(diff(log(Csim))));
disp(mean(diagsim));

figure;
subplot(2,3,1); plot(1:T,Qsim/Qss); title('house price, fraction of SS');
subplot(2,3,2); plot(1:T,Wsim/Wss); title('wealth, fraction of SS');
subplot(2,3,3); plot(1:T,diagsim); title('DIAGNOSTICS');
subplot(2,3,4); plot(1:T,Bsim/Bss); title('debt, fraction of SS');
subplot(2,3,5); plot(1:T,Hsim/Hbar); title('housing, fraction of SS');
subplot(2,3,6); plot(1:T,Csim/Css); title('consumption, fraction of SS');

