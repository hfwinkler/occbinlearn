
clear all
close all

global n_poly

%[ Parameters
bet    =            .99; % consistent with 2% annual real rate
R_ss   =   1/bet - 0.00001;
theta  =            0.5;
H_ss   =              1;
Y_ss   =            0.1;
B_ss   =            1.5;

RE_sw  =              1;


% State Space and Expectations
n        = 10; % # of Chebyschev's nodes
n_poly   = 5;

% Convergence criteria parameters
diff     = 10;
max_iter = 10000;
tol      = 10^-8;
dW       = 1;

z = NaN(n,1);

for i = 1:n
    
    z(i) = -cos(pi*(2*i - 1)/(2*n));
    
end

Y_t       = Y_ss;
q_ss      = Y_t/(1+(R_ss-1)*theta-(1-bet*R_ss)*theta-bet);
B_ss      = theta*q_ss;
C_ss      = Y_t + (1-R_ss)*B_ss;

% C_ss          = 1/(1-theta*(1-R_ss)/(1-(1-bet*R_ss)*theta - bet))*Y_ss;
% q_ss          = 1/(1-(1-bet*R_ss)*theta-bet)*C_ss;
% B_ss          = theta*q_ss;

B_min         = B_ss-4;
B_max         = B_ss+4;
B_grid        = (z + 1) * (B_max - B_min)/2 + B_min;
B_grid        = B_grid;

n_L           = length(B_grid);  % # of L_0 grid points

C_t_grid      = repmat(C_ss,n_L,1);
q_t_grid      = repmat(q_ss,n_L,1);
B_t_grid      = B_grid;
Y_t_grid      = 0.1*ones(n_L,1);
lambda_t      = zeros(n_L,1);
R_t_grid      = repmat(R_ss,n_L,1);
V_t_grid      = repmat(log(0.04)/(1-bet),n_L,1);

T = NaN(n_poly,n);

for i=1:n_poly
    if i == 1 || i == 2
        T(i,:) = z'.^(i-1);
    else
        T(i,:) = 2*z'.*T(i-1,:) - T(i-2,:);
    end
end

Cheb_V    = (T*T')\T*V_t_grid;
Cheb_q    = (T*T')\T*q_t_grid;
Cheb_C    = (T*T')\T*C_t_grid;
Cheb_B    = (T*T')\T*B_t_grid;

ii = 1;

while ii < max_iter && dW > tol;
        
    for jj  = 1:n_L
        
        B_t_m1 = B_grid(jj);

            options = optimset('Display', 'off', 'Algorithm', 'interior-point','TolFun',10e-9,'TolX',10e-9); 

            [C_t] = fminsearch(@(C_t) Value_function(C_t, B_t_m1, bet, theta, H_ss, Y_ss, R_ss, B_min, B_max, Cheb_V, Cheb_q, Cheb_C), C_ss, options);
            
            [V_t, C_t, q_t, B_t, lambda_t, Y_t, R_t] = Value_function(C_t, B_t_m1, bet, theta, H_ss, Y_ss, R_ss, B_min, B_max, Cheb_V, Cheb_q, Cheb_C);
            
            V_t_grid(jj)          = -V_t;
            C_t_grid(jj)          =  C_t;
            q_t_grid(jj)          =  q_t;
            B_t_grid(jj)          =  B_t;
            Y_t_grid(jj)          =  Y_t;
            R_t_grid(jj)          =  R_t;
           
    end
    
    plot(B_grid, q_t_grid)
    
    Cheb_V_p1    = (T*T')\T*V_t_grid;
    Cheb_C_p1    = (T*T')\T*C_t_grid;
    Cheb_q_p1    = (T*T')\T*q_t_grid;    
    Cheb_B_p1    = (T*T')\T*B_t_grid;
    %Cheb_Y_p1    = (T*T')\T*Y_t_grid;
    %Cheb_R_p1    = (T*T')\T*R_t_grid;

    dW = max(max(norm(Cheb_V_p1 - Cheb_V), norm(Cheb_q_p1 - Cheb_q)), norm(Cheb_C_p1 - Cheb_C))
    
    Cheb_V    = Cheb_V_p1;
    Cheb_q    = Cheb_q_p1;
    Cheb_C    = Cheb_C_p1;
    
    ii           = ii + 1;
    
    if dW< tol
        iConvergence = 1;
        disp('converged!')
    end
    
    
end

return