
clear all
close all

global n_poly B_ss lambda Cheb_sw n_q nodes weights

%[ Parameters
bet    =            .99; % consistent with 2% annual real rate
R_ss   =  1/bet - 0.001;
theta  =            0.7;
H_ss   =              1;
Y_ss   =              1;
rho_y  =           0.95;
mu_y   =           Y_ss;
sig_y  =           0.01;
rho_q  =            0.9;
mu_q   =              0;
sig_q  =           0.02;

RE_sw   =             1;

% dampening parameter

lambda   = 0.7;

% State Space and Expectations
n_L      = 50; % # of nodes
n_poly   = 5;

% Convergence criteria parameters
diff     = 10;
max_iter = 10000;
tol      = 10^-8;
dW       = 1;

z = NaN(n_L,1);

for i = 1:n_L
    
    z(i) = -cos(pi*(2*i - 1)/(2*n_L));
    
end

Y_t           = Y_ss;
q_ss          = Y_t/(1+(R_ss-1)*theta-(1-bet*R_ss)*theta-bet);
B_ss          = theta*q_ss;
C_ss          = Y_t + (1-R_ss)*B_ss;

B_min         = B_ss-.1*B_ss;
B_max         = B_ss+.1*B_ss;
B_grid        = [B_min:(B_max - B_min)/(n_L-1):B_max]';%(z + 1) * (B_max - B_min)/2 + B_min;


n_q           = 5;
[nodes, weights] = gauss_hermite_weights_and_nodes(n_q);
% Scale GH nodes
q_nodes = sig_q*nodes' + mu_q;

w_tilde_q = zeros(n_q);

for j = 1:n_q
    
    w_tilde_q(j,:) = 1/sqrt(pi) * weights' .* normpdf((q_nodes - ((1-rho_q)*mu_q + rho_q*q_nodes(j)))/sig_q)./normpdf((q_nodes - ((1-rho_q)*mu_q + rho_q*mu_q))/sig_q);
    
end

q_grid        = q_nodes;

Trans_q = w_tilde_q./repmat(sum(w_tilde_q,2),1,n_q);


n_Y           = 3;
[nodes, weights] = gauss_hermite_weights_and_nodes(n_Y);
% Scale GH nodes
y_nodes = sig_y*nodes' + mu_y;

w_tilde_y = zeros(n_Y);

for j = 1:n_Y
    
    w_tilde_y(j,:) = 1/sqrt(pi) * weights' .* normpdf((y_nodes - ((1-rho_y)*mu_y + rho_y*y_nodes(j)))/sig_y)./normpdf((y_nodes - ((1-rho_y)*mu_y + rho_y*mu_y))/sig_y);
    
end

Trans_y = w_tilde_y./repmat(sum(w_tilde_y,2),1,n_Y);

C_init = C_ss*y_nodes;
q_init = q_ss*y_nodes;

C_t_grid      = repmat(C_init,n_L,1,n_q).*repmat(reshape(exp(q_grid),1,1,n_q),n_L,n_Y,1);
q_t_grid      = repmat(q_init,n_L,1,n_q).*repmat(reshape(exp(q_grid),1,1,n_q),n_L,n_Y,1);
B_t_grid      = repmat(B_grid,1,n_Y,n_q).*repmat(reshape(exp(q_grid),1,1,n_q),n_L,n_Y,1);
C_t_grid_p1   = C_t_grid;
q_t_grid_p1   = q_t_grid;
B_t_grid_p1   = B_t_grid;

ii = 1;

while ii < max_iter && dW > tol;
    
    for yy = 1:n_Y
        
        Y_t_m1 = y_nodes(yy);
        
        for qq = 1:n_q
            
            eps_q  = q_grid(qq);
            
            for jj  = 1:n_L
                
                B_t_m1 = B_grid(jj);
                C_t_m1 = C_t_grid(jj,yy,qq);
                q_t_m1 = q_ss*exp(eps_q);%q_t_grid(jj,yy,qq);
                
                options = optimset('Display', 'off', 'Algorithm', 'interior-point','TolFun',10e-9,'TolX',10e-9);
                
                [C_t, q_t, B_t, lambda_t] = FOCS_q(B_t_m1, Y_t_m1, eps_q, yy, qq, Trans_y, Trans_q, bet, theta, H_ss, R_ss, C_t_m1, q_t_m1,B_grid, C_t_grid, q_t_grid);
                
                C_t_grid_p1(jj,yy,qq)     =  C_t;
                q_t_grid_p1(jj,yy,qq)     =  q_t;
                B_t_grid_p1(jj,yy,qq)     =  B_t;
                
            end
        end
    end
    
    dW = max( max(max( reshape(max(abs(C_t_grid_p1 - C_t_grid)),n_Y,n_q), reshape(max(abs(q_t_grid_p1 - q_t_grid)),n_Y,n_q,1))) )
    
    %dV =  max( max(max(reshape(max(abs(B_t_grid_p1 - B_t_grid)),n_Y,n_q) )));
    
    %dW = max(dW,dV)
    
    C_t_grid = C_t_grid_p1;
    q_t_grid = q_t_grid_p1;
    B_t_grid = B_t_grid_p1;
    
    ii           = ii + 1;
    
    if dW< tol
        iConvergence = 1;
        disp('converged!')
    end
    
end

figure;mesh(B_grid,log(q_ss*exp(q_nodes)),squeeze(B_t_grid(:,1,:)./(theta*q_ss*q_t_grid(:,1,:)))')
title('Debt as a percentage of Constraint')
xlabel('Debt')
ylabel('Asset Price')

figure;mesh(B_grid,y_nodes,C_t_grid(:,:,1)')
title('Consumption')
xlabel('Debt')
ylabel('Output')

figure;mesh(B_grid,y_nodes,B_t_grid(:,:,2)')
title('Debt')
xlabel('Debt')
ylabel('Output')

figure;mesh(B_grid,q_nodes,squeeze(B_t_grid(:,1,:))')
title('Debt')
xlabel('Debt')
ylabel('q')

figure;mesh(B_grid,q_nodes,squeeze(C_t_grid(:,1,:))')
title('Consumption')
xlabel('Debt')
ylabel('q')

% Simulate the model
% Need to add q shocks

simul_sw = 0;

if simul_sw == 1
    
    n_simul = 1000;
    
    C_t_simul = zeros(n_simul,1);
    B_t_simul = zeros(n_simul,1);
    q_t_simul = zeros(n_simul,1);
    Y_t_simul = zeros(n_simul,1);
    
    [seq, states] = hmmgenerate(n_simul, Trans_y, [1,0,0]');
    
    B_t_m1 = B_grid(round(n_L/2));
    
    for ii = 1:n_simul
        
        Y_t_simul(ii) = y_nodes(states(ii));
        
        if Cheb_sw == 1
            
            T_B_p1    = NaN(n_poly,1);
            
            z_B       = 2*(B_t_m1 - B_min)/(B_max - B_min) - 1 ;
            
            for i=1:n_poly
                if i == 1 || i == 2
                    T_B_p1(i,:) = z_B'.^(i-1);
                else
                    T_B_p1(i,:) = 2*z_B'.*T_B_p1(i-1,:) - T_B_p1(i-2,:);
                end
            end
            
            C_t_simul(ii) = T_B_p1'*Cheb_C(:,states(ii));
            q_t_simul(ii) = T_B_p1'*Cheb_q(:,states(ii));
            B_t_simul(ii) = T_B_p1'*Cheb_B(:,states(ii));
            
            B_t_m1        = B_t_simul(ii);
            
        else
            
            B_idx  = find(B_t_m1 >= B_grid(:,1), 1,'last');
            
            if isempty(B_idx)
                
                B_idx = 1;
                
            elseif B_idx >= length(B_grid(:,1))
                
                B_idx = length(B_grid(:,1)) - 1;
                
            end
            
            C_t = ((C_t_grid(B_idx+1,states(ii)) - C_t_grid(B_idx,states(ii)))/(B_grid(B_idx+1) - B_grid(B_idx)) * (B_t_m1 - B_grid(B_idx)) + C_t_grid(B_idx,states(ii)));
            q_t = ((q_t_grid(B_idx+1,states(ii)) - q_t_grid(B_idx,states(ii)))/(B_grid(B_idx+1) - B_grid(B_idx)) * (B_t_m1 - B_grid(B_idx)) + q_t_grid(B_idx,states(ii)));
            B_t = ((B_t_grid(B_idx+1,states(ii)) - B_t_grid(B_idx,states(ii)))/(B_grid(B_idx+1) - B_grid(B_idx)) * (B_t_m1 - B_grid(B_idx)) + B_t_grid(B_idx,states(ii)));
            
            C_t_simul(ii) = C_t;
            q_t_simul(ii) = q_t;
            B_t_simul(ii) = B_t;
            
            B_t_m1        = B_t_simul(ii);
            
        end
        
    end
    
    prob_binding_constr = sum(B_t_simul > theta*q_t_simul*H_ss)/n_simul
    
end

return