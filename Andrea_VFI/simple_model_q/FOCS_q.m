function [C_t, q_t, B_t, lambda_t] = FOCS_q(B_t_m1, Y_t_m1, eps_q, yy, qq, Trans_y, Trans_q, bet, theta, H_ss, R_ss, C_t_m1, q_t_m1,B_grid, C_t_grid, q_t_grid)

global n_q

C_t = C_t_m1;

q_t = q_t_m1;

R   = R_ss;

Y_t = Y_t_m1;

H_t = H_ss;

B_t = C_t - Y_t + R*B_t_m1;

if B_t < theta*q_t*H_t
    
    lambda_t = 0;
    
    %interpolate expectations
    
    B_idx  = find(B_t >= B_grid(:,1), 1,'last');
    
    if isempty(B_idx)
        
        B_idx = 1;
        
    elseif B_idx >= length(B_grid(:,1))
        
        B_idx = length(B_grid(:,1)) - 1;
        
    end
    
    C_t_p1 = zeros(n_q,1);
    q_t_p1 = zeros(n_q,1);
    
    for q_idx = 1:n_q
        
        C_t_p1(q_idx) = Trans_y(yy,:)*((C_t_grid(B_idx+1,:,q_idx) - C_t_grid(B_idx,:,q_idx))/(B_grid(B_idx+1,:) - B_grid(B_idx,:)) * (B_t - B_grid(B_idx,:)) + C_t_grid(B_idx,:,q_idx))';
        
        q_t_p1(q_idx) = Trans_y(yy,:)*((q_t_grid(B_idx+1,:,q_idx) - q_t_grid(B_idx,:,q_idx))/(B_grid(B_idx+1,:) - B_grid(B_idx,:)) * (B_t - B_grid(B_idx,:)) + q_t_grid(B_idx,:,q_idx))';
        
    end
    
    C_t_p1 = Trans_q(qq,:)*C_t_p1;
    q_t_p1 = Trans_q(qq,:)*q_t_p1;
    
    C_t = 1/(lambda_t + bet*R/C_t_p1);
    
    %q_t = 1/(1 - lambda_t*C_t*theta) * (exp(eps_q)*C_t/H_t  + bet*(C_t/C_t_p1)*q_t_p1) ;
    
else
    
    B_t = theta*q_t*H_t;

    C_t = max(10^-10, B_t + Y_t - R*B_t_m1);

% if C_t < 0
% 
%     C_t = 10^-10;
%     q_t = 10^-10;
%     B_t = 0;
%     lambda_t = 10^10;
% 
% else
    
    %C_t = B_t + Y_t - R*B_t_m1;
    
    %interpolate expectations
    
    B_idx  = find(B_t >= B_grid(:,1), 1,'last');
    
    if isempty(B_idx)
        
        B_idx = 1;
        
    elseif B_idx >= length(B_grid(:,1))
        
        B_idx = length(B_grid(:,1)) - 1;
        
    end
    
    C_t_p1 = zeros(n_q,1);
    q_t_p1 = zeros(n_q,1);
    
    for q_idx = 1:n_q
        
        C_t_p1(q_idx) = Trans_y(yy,:)*((C_t_grid(B_idx+1,:,q_idx) - C_t_grid(B_idx,:,q_idx))/(B_grid(B_idx+1,:) - B_grid(B_idx,:)) * (B_t - B_grid(B_idx,:)) + C_t_grid(B_idx,:,q_idx))';
        
        q_t_p1(q_idx) = Trans_y(yy,:)*((q_t_grid(B_idx+1,:,q_idx) - q_t_grid(B_idx,:,q_idx))/(B_grid(B_idx+1,:) - B_grid(B_idx,:)) * (B_t - B_grid(B_idx,:)) + q_t_grid(B_idx,:,q_idx))';
        
    end
    
    C_t_p1 = Trans_q(qq,:)*C_t_p1;
    q_t_p1 = Trans_q(qq,:)*q_t_p1;
    
    lambda_t = (1 - bet*(C_t/C_t_p1)*R)/C_t;
    
    %q_t = 1/(1 - lambda_t*C_t*theta) * ( exp(eps_q)*C_t/H_t + bet*(C_t/C_t_p1)*q_t_p1 );

end

q_t = q_t_m1;
    
end



