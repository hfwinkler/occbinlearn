function [C_t, B_t, H_t, q_t, mu_t, lambda_t] = FOCS_l(C_t, H_t, yy, W_t_m1, q_t_m1, mu_t_m1, Y_t, z_t, C_t_grid, W_t_grid, q_t_grid, mu_t_grid, bet, theta, R_ss, sig_z, g, y_grid_exp, z_grid_exp, Trans_y, Trans_z, W_0_grid, q_0_grid, mu_0_grid, y_0_grid, z_0_grid, n_Y, n_Z)

% Update State Variables

mu_t    = mu_t_m1 + g*z_t;
lq_t_m1 = log(q_t_m1);

lq_t    = lq_t_m1 + mu_t_m1 + z_t;

q_t     = exp(lq_t);

q_t_p1  = q_t*exp(mu_t + 0.5*sig_z^2);

R       = R_ss;
B_t     = C_t + q_t*(H_t - H_t_m1) - Y_t + R*B_t_m1;

%(B_grid,H_grid,q_grid,mu_grid,y_grid,z_grid);

% Unconstrained solution

lambda_t = 0;

iC_t_p1        = interpn(B_0_grid,H_0_grid,q_0_grid,mu_0_grid,y_0_grid,z_0_grid,1./C_t_grid,B_t*ones(n_Y,n_z),H_t*ones(n_Y,n_z),q_t*ones(n_Y,n_z),mu_t*ones(n_Y,n_z), y_grid_exp, z_grid_exp,'linear');

q_t_p1_iC_t_p1 = interpn(B_0_grid,H_0_grid,q_0_grid,mu_0_grid,y_0_grid,z_0_grid,q_t_p1./C_t_grid,B_t*ones(n_Y,n_z),H_t*ones(n_Y,n_z),q_t*ones(n_Y,n_z),mu_t*ones(n_Y,n_z), y_grid_exp, z_grid_exp,'linear');

iC_t_p1        = Trans_y(yy,:)*iC_t_p1*Trans_z;

q_t_p1_iC_t_p1 = Trans_y(yy,:)*q_t_p1_iC_t_p1*Trans_z;

% Update C_t and H_t

C_t = max(10^-10,1/(lambda_t + bet*R*iC_t_p1));

H_t = C_t/(q_t - lambda_t*C_t*theta*q_t - bet*(C_t*q_t_p1_iC_t_p1));

%q_t = 1/(1 - lambda_t*C_t*theta - bet*(C_t*iC_t_p1)*exp(mu_t + 0.5*sig_z^2)) * C_t/H_t;

%z_t = (log(q_t)-log(q_t_m1) - mu_t_m1);

%mu_t = mu_t_m1 + g*z_t;

if  B_t > theta*q_t*H_t
    
    B_t = theta*q_t*H_t;
    
    % Update C_t and H_t
    
    C_t = max(10^-10, B_t - q_t*(H_t - H_t_m1) + Y_t - R*B_t_m1);
    
    iC_t_p1 = interpn(B_0_grid,H_0_grid,q_0_grid,mu_0_grid,y_0_grid,z_0_grid,1./C_t_grid,B_t*ones(n_Y,n_z),H_t*ones(n_Y,n_z),q_t*ones(n_Y,n_z),mu_t*ones(n_Y,n_z), y_grid_exp, z_grid_exp,'linear');
    
    q_t_p1_iC_t_p1 = interpn(B_0_grid,H_0_grid,q_0_grid,mu_0_grid,y_0_grid,z_0_grid,q_t_p1./C_t_grid,B_t*ones(n_Y,n_z),H_t*ones(n_Y,n_z),q_t*ones(n_Y,n_z),mu_t*ones(n_Y,n_z), y_grid_exp, z_grid_exp,'linear');
    
    iC_t_p1 = Trans_y(yy,:)*iC_t_p1*Trans_z;
    
    q_t_p1_iC_t_p1 = Trans_y(yy,:)*q_t_p1_iC_t_p1*Trans_z;
    
    lambda_t = (1 - bet*(C_t*iC_t_p1)*R)/C_t;
    
    H_t = C_t/(q_t - lambda_t*C_t*theta*q_t - bet*(C_t*q_t_p1_iC_t_p1));
    
    %q_t = max(10^-10,1/(1 - lambda_t*C_t*theta - bet*(C_t*iC_t_p1)*exp(mu_t + 0.5*sig_z^2)) * C_t/H_t);
    
    %z_t = (log(q_t)-log(q_t_m1) - mu_t_m1);
    
    %mu_t = mu_t_m1 + g*z_t;
    
end

end



