clear all
close all

global n_q nodes weights n_Y y_grid n_z z_grid Trans_z B_0_grid H_0_grid q_0_grid mu_0_grid y_0_grid z_0_grid

%[ Parameters
bet    =            .99; % consistent with 2% annual real rate
R_ss   =  1/bet - 0.001;
theta  =            0.7;
H_ss   =              1;
Y_ss   =              1;
rho_y  =           0.95;
mu_y   =           Y_ss;
sig_y  =           0.01;
rho_q  =            0.9;
mu_q   =              0;
sig_q  =           0.02;
mu_ss  =              0;
sig_z  =          0.001;
g      =          10^-4;

RE_sw   =             1;

% dampening parameter

lambda   = 1;

% State Space and Expectations
Y_t           = Y_ss;
q_ss          = Y_t/(1+(R_ss-1)*theta-(1-bet*R_ss)*theta-bet);
B_ss          = theta*q_ss;
C_ss          = Y_t + (1-R_ss)*B_ss;

n_L           = 20; % # of nodes for B
B_min         = B_ss-.1*B_ss;
B_max         = B_ss+.05*B_ss;
B_grid        = [B_min:(B_max - B_min)/(n_L-1):B_max]';

n_H           = 20; % # of nodes for H
H_min         = H_ss-.1*H_ss;
H_max         = H_ss+.1*H_ss;
H_grid        = [H_min:(H_max - H_min)/(n_H-1):H_max]';

n_q           = 20;
q_min         = q_ss-.02*q_ss;
q_max         = q_ss+.02*q_ss;
q_grid        = [q_min:(q_max - q_min)/(n_q-1):q_max]';

n_mu          = 20;
mu_min        = -10^-3;
mu_max        =  10^-3;
mu_grid       = [mu_min:(mu_max - mu_min)/(n_mu-1):mu_max]';

n_W           = 20;
W_grid        = q_grid.*H_grid - R_ss*B_grid;

n_Y           = 2;

[nodes, weights] = gauss_hermite_weights_and_nodes(n_Y);

% Scale GH nodes
y_nodes = sig_y*nodes' + mu_y;
y_grid  = y_nodes;

% Find transition matrix for y state
w_tilde_y = zeros(n_Y);
for j = 1:n_Y
    
    w_tilde_y(j,:) = 1/sqrt(pi) * weights' .* normpdf((y_nodes - ((1-rho_y)*mu_y + rho_y*y_nodes(j)))/sig_y)./normpdf((y_nodes - ((1-rho_y)*mu_y + rho_y*mu_y))/sig_y);
    
end

Trans_y = w_tilde_y./repmat(sum(w_tilde_y,2),1,n_Y);

n_z           = 3;

[nodes, weights] = gauss_hermite_weights_and_nodes(n_z);

% Scale GH nodes
z_nodes = sig_z*nodes';
z_grid  = z_nodes;

% Find transition matrix for y state
Trans_z = weights;

[B_0_grid,H_0_grid,q_0_grid,mu_0_grid,y_0_grid,z_0_grid] = ndgrid(B_grid,H_grid,q_grid,mu_grid,y_grid,z_grid);

NN = numel(B_0_grid);

% Definte initial conditions
%C_init        = C_ss*y_nodes;
%q_init        = q_ss*y_nodes;
C_t_grid      = C_ss*y_0_grid;
B_t_grid      = B_0_grid;
H_t_grid      = H_0_grid;
q_t_grid      = q_0_grid;
mu_t_grid     = mu_0_grid;
y_t_grid      = y_0_grid;


y_grid_exp = repmat(y_grid',1,n_z);
z_grid_exp = repmat(z_grid,n_Y,1);

C_t_grid_p1   = C_t_grid;
q_t_grid_p1   = q_0_grid;
B_t_grid_p1   = B_0_grid;
H_t_grid_p1   = H_0_grid;
mu_t_grid_p1  = mu_0_grid;
lambda_t_grid_p1 = mu_0_grid;

% Convergence criteria parameters
max_iter = 10000;
tol      = 10^-8;
dW       = 1;

ii = 1;

try
    parpool
catch me
end

while ii < max_iter && dW > tol;
    
    for nn  = 1:NN
        
        [bb,hh,qq,mm,yy,zz]  = ind2sub(size(B_0_grid),nn);
        
        B_t_m1    = B_0_grid(nn);
        H_t_m1    = H_0_grid(nn);
        q_t_m1    = q_0_grid(nn);
        mu_t_m1   = mu_0_grid(nn);
        y_t       = y_0_grid(nn);
        z_t       = z_0_grid(nn);
        
        
        % Use guess from previous iteration for C_t and H_t
        C_t_guess     = C_t_grid(nn);
        H_t_guess     = H_t_grid(nn);
        
        
        [C_t, B_t, H_t, q_t, mu_t, lambda_t] = FOCS_l(C_t_guess, H_t_guess ,yy,B_t_m1, H_t_m1, q_t_m1, mu_t_m1, y_t, z_t, C_t_grid, B_t_grid, H_t_grid, q_t_grid, mu_t_grid, Trans_y, bet, theta, R_ss, sig_z, g, y_grid_exp, z_grid_exp);
        
        C_t_grid_p1(nn)      =  C_t;
        B_t_grid_p1(nn)      =  B_t;
        H_t_grid_p1(nn)      =  H_t;
        q_t_grid_p1(nn)      =  q_t;
        mu_t_grid_p1(nn)     =  mu_t;
        lambda_t_grid_p1(nn) =  lambda_t;
        
        
    end
    
    dW = max(max( max( reshape(abs(C_t_grid_p1 - C_t_grid),n_Y*n_L,n_q*n_mu), reshape(abs(q_t_grid_p1 - q_t_grid),n_Y*n_L,n_q*n_mu) )));
    
    dV =  max( max(max(reshape(abs(mu_t_grid_p1 - mu_t_grid),n_Y*n_L,n_q*n_mu))));
    
    dW = max(dW,dV)
    
    C_t_grid  = C_t_grid_p1;
    q_t_grid  = q_t_grid_p1;
    B_t_grid  = B_t_grid_p1;
    mu_t_grid = mu_t_grid_p1;
    
    ii           = ii + 1;
    
    if dW< tol
        iConvergence = 1;
        disp('converged!')
    end
    
end

figure;mesh(B_grid,log(q_ss*exp(q_nodes)),squeeze(B_t_grid(:,1,:)./(theta*q_ss*q_t_grid(:,1,:)))')
title('Debt as a percentage of Constraint')
xlabel('Debt')
ylabel('Asset Price')

figure;mesh(B_grid,y_nodes,C_t_grid(:,:,1)')
title('Consumption')
xlabel('Debt')
ylabel('Output')

figure;mesh(B_grid,y_nodes,B_t_grid(:,:,2)')
title('Debt')
xlabel('Debt')
ylabel('Output')

figure;mesh(B_grid,q_nodes,squeeze(B_t_grid(:,1,:))')
title('Debt')
xlabel('Debt')
ylabel('q')

figure;mesh(B_grid,q_nodes,squeeze(C_t_grid(:,1,:))')
title('Consumption')
xlabel('Debt')
ylabel('q')

% Simulate the model
% Need to add q shocks

simul_sw = 0;

if simul_sw == 1
    
    n_simul = 1000;
    
    C_t_simul = zeros(n_simul,1);
    B_t_simul = zeros(n_simul,1);
    q_t_simul = zeros(n_simul,1);
    Y_t_simul = zeros(n_simul,1);
    
    [seq, states] = hmmgenerate(n_simul, Trans_y, [1,0,0]');
    
    B_t_m1 = B_grid(round(n_L/2));
    
    for ii = 1:n_simul
        
        Y_t_simul(ii) = y_nodes(states(ii));
        
        if Cheb_sw == 1
            
            T_B_p1    = NaN(n_poly,1);
            
            z_B       = 2*(B_t_m1 - B_min)/(B_max - B_min) - 1 ;
            
            for i=1:n_poly
                if i == 1 || i == 2
                    T_B_p1(i,:) = z_B'.^(i-1);
                else
                    T_B_p1(i,:) = 2*z_B'.*T_B_p1(i-1,:) - T_B_p1(i-2,:);
                end
            end
            
            C_t_simul(ii) = T_B_p1'*Cheb_C(:,states(ii));
            q_t_simul(ii) = T_B_p1'*Cheb_q(:,states(ii));
            B_t_simul(ii) = T_B_p1'*Cheb_B(:,states(ii));
            
            B_t_m1        = B_t_simul(ii);
            
        else
            
            B_idx  = find(B_t_m1 >= B_grid(:,1), 1,'last');
            
            if isempty(B_idx)
                
                B_idx = 1;
                
            elseif B_idx >= length(B_grid(:,1))
                
                B_idx = length(B_grid(:,1)) - 1;
                
            end
            
            C_t = ((C_t_grid(B_idx+1,states(ii)) - C_t_grid(B_idx,states(ii)))/(B_grid(B_idx+1) - B_grid(B_idx)) * (B_t_m1 - B_grid(B_idx)) + C_t_grid(B_idx,states(ii)));
            q_t = ((q_t_grid(B_idx+1,states(ii)) - q_t_grid(B_idx,states(ii)))/(B_grid(B_idx+1) - B_grid(B_idx)) * (B_t_m1 - B_grid(B_idx)) + q_t_grid(B_idx,states(ii)));
            B_t = ((B_t_grid(B_idx+1,states(ii)) - B_t_grid(B_idx,states(ii)))/(B_grid(B_idx+1) - B_grid(B_idx)) * (B_t_m1 - B_grid(B_idx)) + B_t_grid(B_idx,states(ii)));
            
            C_t_simul(ii) = C_t;
            q_t_simul(ii) = q_t;
            B_t_simul(ii) = B_t;
            
            B_t_m1        = B_t_simul(ii);
            
        end
        
    end
    
    prob_binding_constr = sum(B_t_simul > theta*q_t_simul*H_ss)/n_simul
    
end

return