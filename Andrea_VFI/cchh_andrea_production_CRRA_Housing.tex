\documentclass[12pt]{article}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage[margin=1in]{geometry}
\usepackage{helvet}

\begin{document}

\section*{The Household}

The economy is populated by a large household that in each period $t$ consumes non-durable goods $C_t$ and housing $H_t$ and supplies hours worded $N_t$ to maximize the utility function:

\begin{equation} \label{EQ1}
\max_{\left(C_{t}, H_{t}\right)_{t=0}^{\infty}}\mathbb{E}\sum_{t=0}\beta^{t} \left(\frac{C_{t}^{1-\varphi}}{1-\varphi}+\log H_{t} - \omega \frac{N_t^{1+\nu}}{1+\nu} \right)
\end{equation}


The household is composed by a measure 1 of members. Each period is composed of two sub-periods. At the start of the first subperiod the head of the household entrusts each member with the same endowment of wealth, equal to a fraction of the household stock of housing $q_th_{t-1}$ (with $\int h_{i,t-1} di = H_{t-1}$, $q_t$ being the unit price of real estate) plus an equal fraction of nominal bonds issued in period $t-1$ that will come to maturity in period $t$, $b_{t-1}^{\$+}$, minus a fraction of the stock of nominal debt issued by household members in period $t-1$, $b_{t-1}^{\$-}$. In the first sub-period household members also take part in the labor market and supply hours worked $N_t$. Labor is employed by the production side of the economy and remunerated with a nominal hourly wage $W_t$, equally across household members. 

In the second sub-period, after receiving their endowment and their labor income, members leave the household and a fraction $\chi$ of them (indexed by 'b', as in borrower) is presented with a purchase opportunity on the real estate market, $q_th_{b,t}$, where $h_{b,t}$ is the purchase of member $b$. These members can issue bonds, $b_{b,t}^{\$-}$, on financial markets to finance the real estate purchase.

The budget constraint of members with a real estate opportunity will then be:
\begin{equation} \label{EQ3}
P_t c_{b,t} + Q_t (h_{b,t} - (1-\delta_H) h_{t-1}) + R_{t-1}b_{t-1}^{\$-} \leq W_t N_{b,t} + b_{b,t}^{\$-} + R_{t-1}b_{t-1}^{\$+} + \Pi_{b,t}.
\end{equation}
where $P_t$ is the nominal price of a unit of consumption good, and $Q_t$ is the nominal price of housing. Note that $b_{b,t}^{-}$ are nominal bonds and enter the budget constraint as a source of funds. We will show that in equilibrium borrowers will optimally issue bonds to fund the purchase of housing stock. $\Pi_{t}$ are profits from housing and goods production and that are distributed lump-sum to household members. We assume that borrowers need to satisfy the borrowing constraint:
\begin{equation} \label{EQ4}
b_{b,t}^{\$-} \leq \theta Q_t h_{b,t}
\end{equation}
so that the amount of bonds issued cannot be higher than a fraction $\theta$ of the value of their intended housing stock

Household members without a real estate opportunity will face a budget constraint equal to:

\begin{equation} \label{EQ5}
P_t c_{l,t} + R_{t-1}b_{t-1}^{\$-} + b_{l,t}^{\$+} \leq W_t N_{l,t} + R_{t-1}b_{t-1}^{\$+} + \Pi_{l,t} + Q_t (1-\delta_H) h_{t-1} 
\end{equation}
Note that $b_{l,t}^{+}$ enters the budget constraint as a use of funds. I will show that in equilibrium lenders will optimally purchase bonds issued by borrowers to fund the purchase of housing stock.

Note also that on aggregate, in a closed economy, bonds issued in each period are in zero net supply and do not enter the definition of wealth of the household, so that $\int b_{l,t}^{\$+} di - \int b_{b,t}^{\$-} di = 0$. Similarly for each member $R_{t-1}b_{t-1}^{\$-} = R_{t-1}b_{t-1}^{\$+}$, since they all receive the same fraction of assets and liabilities. Note that we are assuming that bonds are default-free and that no intermediation costs affect the spread between interest raid paid to/by household members.

On aggregate, the budget constraint of the household is then equal to:

\begin{equation} \label{EQ6}
P_t C_t + Q_t (H_t  - (1-\delta_H) H_{t-1}) \leq W_t N_t + \Pi_t.
\end{equation}
with $C_t = \int c_{i,t} di$ and $H_t = \int h_{i,t} di$. See below for the goods market clearing condition, that aggregates firms profits in the household budget constraint.

\subsection*{The Maximization Problem}

Before uncertainty over real estate opportunity is resolved, the head of the household will choose members' contingent plans for the purchase of consumption and housing goods, to maximize aggregate utility:
\begin{equation} \label{EQ7}
\max_{\left(c_{i,t}, h_{i,t}\right)_{t=0}^{\infty}}\mathbb{E}\sum_{t=0}\beta^{t}\left( \frac{1}{1-\varphi}\left(\int c_{i,t} di\right)^{1-\varphi}+\log \int h_{i,t} di - \omega \frac{\left(\int n_{i,t}di\right)^{(1+\nu)}}{1+\nu}  \right)
\end{equation}
subject to the constraints from (\ref{EQ8}) - (\ref{EQ11}). Here we express all constraints in real terms (dividing both right- and left-hand sides by $P_t$):

Budget and borrowing constraints for members $i = b \in [0, \chi]$:
\begin{equation} \label{EQ8}
 c_{b,t} + q_t(h_{b,t} - H_{t-1}) + \frac{R_{t-1}}{\pi_t}b_{t-1}^{-} \leq W_t N_{b,t} + b_{b,t}^{-} + \frac{R_{t-1}}{\pi_t}b_{t-1}^{+}.
\end{equation}
where the value of real bonds $b_{t-1}$ is defined as $\frac{b_{t-1}^{\$}}{P_{t-1}}$ and $\pi_t$ is the inflation rate defined as the ratio of the price index $P_t$ in time $t$ with respect of $t-1$: $\pi_t = \frac{P_t}{P_{t-1}}$.
\begin{equation} \label{EQ9}
b_{b,t}^{-} \leq \theta q_t h_{b,t}
\end{equation}
with $q_t = \frac{Q_t}{P_t}$ is the real price of housing.

Budget constraint for members $i = l \in (\chi, 1]$:
\begin{equation} \label{EQ10}
  c_{l,t} + R_{t-1}b_{t-1}^{-} + b_{l,t}^{+} \leq w_tN_{l,t} + R_{t-1}b_{t-1}^{+} + q_t H_{t-1}
\end{equation}
where $w_t = \frac{W_t}{P_t}$ is the real wage.
Definition of the aggregate housing stock:
\begin{equation} \label{EQ11}
H_t = \int h_{i,t} di
\end{equation}
%The head of the household will also take into account the aggregate process for output:
%\begin{equation} \label{EQ12}
% Y_t = (1-\rho_y)Y_{ss} + \rho_y Y_{t-1} + \epsilon_t^y.
%\end{equation}
We also impose that consumption of members needs to be non-negative:
\begin{equation} \label{EQ12}
c_{b,t} \geq 0
\end{equation}
\begin{equation} \label{EQ13}
c_{l,t} \geq 0
\end{equation}
We attach multipliers $\mu_t^b$, $\lambda_t^b$, $\mu_t^l$, $\mu_t^{\Sigma h}$  to equations (\ref{EQ8}) to (\ref{EQ11}), and multipliers $\mu_t^{c_b +}$ and $\mu_t^{c_l +}$  to the non-negativity constraints (\ref{EQ12}) and (\ref{EQ13}). The aggregate resource constraint in (\ref{EQ6}) is equal to the sum of (\ref{EQ8}) and (\ref{EQ10}) over the two sets of household members and is therefore redundant.

\subsection*{The First Order Conditions for the Household}

\begin{equation}\label{EQ14}
c_{b,t}: \mu_t^b - \mu_t^{c_b +} = \frac{1}{C_t^\varphi}
\end{equation}

\begin{equation}\label{EQ15}
h_{b,t}: q_t =  \frac{1}{\mu_t^b - \lambda_{t}^b \theta} \left( \frac{1}{H_t} + \mu_t^{\Sigma h} \right)
\end{equation}

\begin{equation}\label{EQ16}
n_{l,t}: (\chi \mu_t^b + (1-\chi) \mu_t^l) w_t = \omega N_t^\nu
\end{equation}

\begin{equation}\label{EQ17}
c_{l,t}: \mu_t^l - \mu_t^{c_l +} = \frac{1}{C_t^\varphi}
\end{equation}

\begin{equation}\label{EQ18}
H_{t}: \mu_t^{\Sigma h} = \beta E_t\left[ (\chi \mu_{b,t+1} q_{t+1} + (1-\chi) \mu_{l,t+1} q_{t+1} ) \right] 
\end{equation}

\begin{equation}\label{EQ19}
b_{b,t}^-: \mu_{b,t} - \lambda_{t}^b = \beta E_t\left[ \frac{R_t}{\pi_{t+1}} (\chi \mu_{b,t+1} + (1-\chi) \mu_{l,t+1}) \right] 
\end{equation}

\begin{equation}\label{EQ20}
b_{b,t}^+: \mu_{l,t} = \beta E_t\left[ \frac{R_t}{\pi_{t+1}} (\chi \mu_{b,t+1} + (1-\chi) \mu_{l,t+1}) \right] 
\end{equation}

Complementary slackness conditions:
\begin{equation}
\lambda_{t}^b(b_{b,t}^{-} - \theta q_t h_{b,t}) = 0
\end{equation}

\begin{equation}
\mu_t^{c_b +}c_{b,t} = 0
\end{equation}

\begin{equation}
\mu_t^{c_l +}c_{l,t} = 0
\end{equation}
I omit the constraints (reported above) and the transversality condition.

We can characterize the equilibrium conditions over the state space depending on whether the borrowing constraint binds or not.

First, consider the case in which the borrowing constraint is not binding. That means that the multiplier:
\begin{equation}
\lambda_{t}^b = 0
\end{equation}
and that the amount of borrowing will be equal to:
\begin{equation}
b_{b,t}^{-} \leq \theta q_t h_{b,t}
\end{equation}
The right-hand sides of equations (\ref{EQ19}) and (\ref{EQ20}) are the same, and since $\lambda_{t}^b = 0$, it follows that the multipliers:
\begin{equation}
\mu_t^b = \mu_t^l
\end{equation}
From (\ref{EQ15}) and (\ref{EQ16}), this equality implies that:
\begin{equation}
\mu_t^{c_b +}  = \mu_t^{c_l +} = 0
\end{equation}
since  aggregate consumption $C_t$ would be zero if both $c_{l,t}$ and $c_{b,t}$ were zero ($\mu_t^{c_b +}  = \mu_t^{c_l +} > 0$).
To sum up, if the borrowing constraint does not bind, the household's Euler equations for housing and nominal bonds become:
\begin{equation}
	\frac{q_t}{C_t^\varphi} =   \left( \frac{1}{H_t} + \beta E_t\left[\chi \mu_{b,t+1} q_{t+1} + (1-\chi) \mu_{l,t+1} q_{t+1} \right]  \right)
\end{equation}

\begin{equation}
	\frac{1}{C_t^\varphi} =  \beta E_t\left[ \frac{R_{t}}{\pi_{t+1}} (\chi \mu_{b,t+1} + (1-\chi) \mu_{l,t+1}) \right] 
\end{equation}
where the expected values of the multipliers $\mu_{b,t+1}$ and $\mu_{l,t+1}$ will reflect the likelihood that the borrowing constraint could be binding in period $t+1$. When the constraint is not binding, $\mu_{b,t} = \mu_{l,t} = \frac{1}{C_t^\varphi}$. 

Second, consider the case in which the borrowing constraint is binding. This implies that the multiplier:
\begin{equation}
\lambda_{t}^b > 0
\end{equation}
and that borrowing will be constrained at:
\begin{equation}
b_{b,t}^{-} = \theta q_t h_{b,t}
\end{equation}
The fact that $\lambda_{t}^b > 0$ implies, from equations (\ref{EQ19}) and (\ref{EQ20}) that:
\begin{equation}
\mu_t^b > \mu_t^l.
\end{equation}
Equation \ref{EQ15} and the non-negativity requirement for aggregate consumption $C_t$ imply that:
\begin{equation}
\mu_t^{c_b +} = \lambda_{t}^b >0
\end{equation}
which means that borrowers' purchase of consumption goods is zero $c_{b,t} = 0$ when the borrowing constraint binds.
To conclude, if the borrowing constraint binds, borrowers borrow up to their limit, reduce consumption to zero and purchase as much real estate as their income, wealth and debt position allows them to do:
\begin{equation}
q_t(h_{b,t} - \theta h_{b,t}) = \left( q_t H_{t-1} - R_{t-1}b_{t-1}^{-} + R_{t-1}b_{t-1}^{+} \right).
\end{equation}
Aggregating over borrowers, one can find an expression for $H_t$:
\begin{equation}
H_{t} = \frac{\chi}{q_t(1-\theta)} \left( q_t H_{t-1}  - \frac{R_{t-1}}{\pi_t}b_{t-1}^{-} + \frac{R_{t-1}}{\pi_t}b_{t-1}^{+} \right).
\end{equation}
On the other hand, lenders purchase consumption goods and bonds. The Euler equations for housing then becomes:
\begin{equation}
		q_t =  \frac{C_t^{\varphi}}{1 + C_t^{\varphi} (1-\theta) \lambda_{t}^b} \left( \frac{1}{H_t} + \beta E_t\left[\chi \mu_{b,t+1} q_{t+1} + (1-\chi) \mu_{l,t+1} q_{t+1} \right] \right)
\end{equation}
which can be solved for $\lambda_{t}^b$ for each value of $q_t$. The Euler equation for bonds is instead:
\begin{equation}
\frac{1}{C_t^{\varphi}} = \beta E_t\left[ \frac{R_{t}}{\pi_{t+1}} (\chi \mu_{b,t+1} + (1-\chi) \mu_{l,t+1}) \right].
\end{equation}
The expected values of the multipliers $\mu_{b,t+1}$ and $\mu_{l,t+1}$ will reflect the likelihood that the borrowing constraint could be binding in period $t+1$. When the constraint is binding, $\mu_{l,t} = \frac{1}{C_t^{\varphi}}$, while $\mu_{b,t} = \frac{1}{C_t^{\varphi}} + \lambda_{t}^b$. 
Note that the household stochastic discount factor is equal to:
\begin{equation}\label{SSF}
\beta \frac{\Lambda_{t+1}}{\Lambda_{t}} = \beta E_t\left[ C_t^{\varphi} (\chi \mu_{b,t+1} + (1-\chi) \mu_{l,t+1}) \right].
\end{equation}

\section*{The Productive Sector}

The household owns the productive sector of the economy, composed by a housing producer, a measure of intermediate-good producers and a final good producer. Intermediate goods are indexed by $i\in[0,1]$, $y_t(i)$, and act as imperfect substitutes in the production of final goods, $y_t$. Profits from these production units are rebated lump-sum to the household.

\subsection*{Housing Producer}

There is a measure of housing producers that choose a real estate investment $dH_t$ and a housing stock $H_t$ to maximize the expected discounted value of their flow of profits from real estate sales at price $Q_t$, net of convex housing investment adjustment costs $\frac{\psi_H}{2} \left( \frac{dH_t}{H_{t-1}^*} - \delta_H \right)^2 H_{t-1}$, where $H_{t-1}^*$ is the aggregate housing stock, which in a symmetric equilibrium is equal to the single producers' housing stock $H_{t-1}$ in any period $t$. 

At the end of each period, accrued profits are rebated to the household:

\begin{equation}
	\max_{dH_t, H_t} \sum_{s=0}^\infty \beta^{t+s} \frac{\Lambda_{t+s+1}}{\Lambda_{t+s}} \left( 
	Q_{t+s} H_{t+s} - dH_{t+s} - \frac{\psi_H}{2} \left( \frac{dH_{t+s}}{H_{t+s-1}^*} - \delta_H \right)^2 H_{t+s-1} - 	Q_{t+s} (1-\delta_H) H_{t+s-1} \right)
\end{equation}
s.t.
\begin{equation}\label{Hevol}
H_{t+s} = dH_{t+s} + (1-\delta_H)H_{t+s-1}	
\end{equation}
Using the constraint, we can substitute out $H_{t+s}$ in the profit function and maximize with respect of $dH_{t+s}$ alone, given the assumption of external adjustment costs. Consequently, the first order conditions of the real estate producer for a generic time $t$ are equal to:
\begin{equation} \label{QH_ext}
Q_t = 1 + \psi_H \left( \frac{dH_t}{H_{t-1}} - \delta_H \right)
\end{equation}
together with the constraint:
\begin{equation}\label{Hevol}
H_{t} = dH_{t} + (1-\delta_H)H_{t-1}	
\end{equation}

If we wanted to introduce internal adjustment costs, then \eqref{QH_ext} would become:

\begin{eqnarray}
\label{QH_int}
Q_t = &1& + \psi_H \left( \frac{dH_t}{H_{t-1}} - \delta_H \right) + \\ \nonumber
&\beta & \frac{\Lambda_{t+1}}{\Lambda_{t}}  \left[ 
\psi_H \left( \frac{dH_{t+1}}{H_{t}} - \delta_H  \right) \frac{H_{t+1}}{H_{t}} - \frac{\psi_H}{2} \left( \frac{dH_{t+1}}{H_{t}} - \delta_H \right)^2
- (Q_{t+1} - 1)(1-\delta_H)
\right]
\end{eqnarray}
Note that if $\psi_H = 0$, the only solution to both \eqref{QH_ext} and \eqref{QH_int} is $Q_t = 1$, $\forall t$.


\subsection*{Final Good Producers}

Final goods producers adopt a CES production function: 
\begin{equation} 
y_t = \left(\int_0^1y_t(i)^\frac{\eta-1}{\eta} di\right)^\frac{\eta}{\eta-1}\label{EQ37}
\end{equation}
The final good producer operates in perfect competition, and seeks to maximize profits, $\Pi_t$:
\begin{equation} 
\max_{y_t, y_t(i)\forall i}\Pi_t = P_ty_t - p_t(i)y_t(i), \label{fgmV}
\end{equation}
subject to the production function (\ref{EQ37}), where $P_t$ is the price of the final goods and $p_t(i)$ is the per-unit price of intermediate goods $y_t(i)$. 



\begin{equation}\label{EQ39}
 y_t(i) = \left(\frac{p_t(i)}{P_t}\right)^{-\eta}y_t, 
\end{equation}
which is the demand for good $y_t(i)$ conditional on aggregate production of final goods $y_t$. The usual price aggregator is derived by plugging the demand function (\ref{EQ39}) into the production function (\ref{EQ37}):
\begin{equation}
P_t^{-\eta} = \left(\int_0^1p_t(i)^{1-\eta}di\right)^\frac{\eta}{\eta-1}	
\end{equation}

\subsection*{Intermediate Goods Producers} 

Each intermediate-goods producer employs hours worked, $h_t(i)$, to produce goods $y_t(i)$ according to the linear production function: 
\begin{equation}\label{EQ41}
y_t(i) = \exp(a_t) h_t(i).
\end{equation}
where $a_t$ is aggregate TFP and can follow an AR(1) process:
\[
a_t = (1-\rho_a)a_{ss} + \rho_a a_{t-1} + \sigma_a \epsilon_t^a
\]
and $\epsilon_t^a \sim N(0,1)$.

Producers pay workers a real nominal hourly wage, $w_t = \frac{W_t}{P_t}$. 
Their goods are partial substitutes and intermediate good producers act in regime of monopolistic competition. In every period
$t$, they observe the demand for their good, \eqref{EQ39}, and select $p_t(i)$ that maximizes their profits under minimum costs. 
Producer $i^\text{th}$ wishes to minimize total real costs, $TC_t(i)$:
\begin{equation}\label{EQ42}
TC_t(i) = w_t h_t(i) ,\;\;\;\forall t	
\end{equation}
subject to the production function \eqref{EQ41}, to which we assign multiplier $\lambda_t(i)$. The FOCs are: 
%\begin{align}
%h_t(i): w_t &= \lambda_t(i)(1-\alpha)\exp(a_t)^{1-\alpha}k_{t-1}(i)^\alpha h_t(i)^{-\alpha}\label{FOChV}\\
%k_{t-1}(i): r_t^k &= \lambda_t(i)\alpha \exp(a_t)^{1-\alpha}k_{t-1}(i)^{\alpha-1} h_t(i)^{1-\alpha}\label{FOCkV}
%\end{align}
\begin{equation} \label{EQ43}
h_t(i): w_t = \lambda_t(i)
\end{equation}
%and \eqref{pfigV}. Taking the ratio of \eqref{FOChV} to \eqref{FOCkV}, we get 
%\begin{equation}\frac{w_t}{r_t^k(i)} = 
%\frac{(1-\alpha)k_{t-1}(i)}{\alpha h_t(i)},\label{FOCkhV}\end{equation} which pins down the ratio of 
%inputs (capital and hours). 

Multiplying \eqref{EQ43} by $h_t(i)$ and 
substituting in \eqref{EQ42}, we attain
\begin{equation}\label{EQ44}
w_th_t(i) = TC_t(i) = \lambda_t(i) y_t(i)
\end{equation}	
where $\lambda_t(i)$ is the real marginal cost. Note that the marginal cost $\lambda_t(i)$ is the same across producers $i$ so that we can drop the index and rename $\lambda_t = mc_t$). The total cost function can thus be rewritten: $TC_t(i) = P_t mc_t y_t(i)$. Following Rotemberg (1992), we now assume that when a firm is free to change its nominal price, $p_t(i)$, in every period, but it incurs a real cost:
\begin{equation}\label{EQ46} 
\frac{\psi}{2}\left(\frac{p_t(i)}{p_{t-1}(i)\pi}-1\right)^2,
\end{equation}
which is quadratic in the deviation from $1$ of the ratio of producer's $i$ inflation rate, $\frac{p_t(i)}{p_{t-1}(i)}$, from steady state inflation, $\pi$.

The intermediate firm wishes to maximize the present discounted value of real profits at time $t$: 
\begin{equation}\label{EQ48}
\max_{p_t(i)} \sum_{t = 0}^\infty \beta^t E_t\left[\Lambda_t\left(\left(\frac{p_t(i)}{P_t} - mc_t \right) y_t(i) - \frac{\psi}{2}\left(\frac{p_t(i)}{p_{t-1}(i)\pi}-1 \right)^2 \right) \right]
\end{equation}

In a symmetric equilibrium $p_t(i) = p_t(j)\;\;\forall i,j\in[0,1]$, the first order condition of the producers' problem will give rise to the Phillips curve:

\begin{equation} \label{EQ48}
(1-\eta)+ \eta mc_t - \psi\left(\frac{\pi_t}{\pi} - 1\right)\frac{\pi_t}{\pi} + \beta E_t \frac{\Lambda_{t+1}}{\Lambda_t}\psi\left(\frac{\pi_{t+1}}{\pi}-1\right) \frac{\pi_{t+1}}{\pi}\frac{y_{t+1}}{y_t}
\end{equation}
where $\beta\frac{\Lambda_{t+1}}{\Lambda_t}$ is the household stochastic discount factor in \eqref{SSF} and inflation is defined as $\pi_t = \frac{P_t}{P_{t-1}}$

\section*{The Monetary Policy Authority}

To close a baseline version of the model, we assume that the monetary policy authority sets the nominal rate of interest, $R_t$, by means of the Taylor-type rule:

\begin{equation} 
R_t = r_{ss} + \pi_{ss} + \phi_{\pi}(\pi_t - \pi_ss) + \phi_y(Y_t - Y_{ss});
\end{equation}

\section*{Market Clearing}

By aggregating the budget constraints of the household members and substituting the expressions of time $t$ profits of housing producers, intermediate goods producers and final goods producers, we obtain:
\begin{equation}
	C_t + dH_t + \frac{\psi_H}{2} \left( \frac{dH_t}{H_{t-1}} - \delta_H \right)^2 H_{t-1}  + \frac{\psi}{2}\left(\frac{p_t(i)}{p_{t-1}(i)\pi}-1\right)^2 = Y_t
\end{equation}

\end{document}