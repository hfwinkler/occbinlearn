function [C_t, q_t, B_t, lambda_t] = FOCS(B_t_m1, Y_t_m1, yy, Trans, Cheb_C, Cheb_q, T_B_j, bet, theta, H_ss, R_ss, B_min, B_max, C_t_m1, q_t_m1, B_grid, C_t_grid, q_t_grid)

global n_poly Cheb_sw

if Cheb_sw == 1

    C_t = T_B_j'*Cheb_C(:,yy);

    q_t = T_B_j'*Cheb_q(:,yy);

else
    
    C_t = C_t_m1(yy);
    
    q_t = q_t_m1(yy);
    
end

R   = R_ss;

Y_t = Y_t_m1;

H_t = H_ss;

B_t = C_t - Y_t + R*B_t_m1;

if Cheb_sw ==1
    
    T_B_p1    = NaN(n_poly,1);
    
    z_B       = 2*(B_t - B_min)/(B_max - B_min) - 1 ;
    
    for i=1:n_poly
        if i == 1 || i == 2
            T_B_p1(i,:) = z_B'.^(i-1);
        else
            T_B_p1(i,:) = 2*z_B'.*T_B_p1(i-1,:) - T_B_p1(i-2,:);
        end
    end
    
    C_t_p1 = Trans(yy,:)*(T_B_p1'*Cheb_C)';
    
    q_t_p1 = Trans(yy,:)*(T_B_p1'*Cheb_q)';

else
    
    %interpolate expectations
    
    B_idx  = find(B_t >= B_grid(:,1), 1,'last');
    
    if isempty(B_idx)
        
        B_idx = 1;
        
    elseif B_idx >= length(B_grid(:,1))
        
        B_idx = length(B_grid(:,1)) - 1;
        
    end
    
    C_t_p1 = Trans(yy,:)*((C_t_grid(B_idx+1,:) - C_t_grid(B_idx,:))/(B_grid(B_idx+1,:) - B_grid(B_idx,:)) * (B_t - B_grid(B_idx,:)) + C_t_grid(B_idx,:))';  
    
    q_t_p1 = Trans(yy,:)*((q_t_grid(B_idx+1,:) - q_t_grid(B_idx,:))/(B_grid(B_idx+1,:) - B_grid(B_idx,:)) * (B_t - B_grid(B_idx,:)) + q_t_grid(B_idx,:))';
    
end

if B_t < theta*q_t*H_t

    lambda_t = 0;
    
    C_t = 1/(lambda_t + bet*R/C_t_p1);

    q_t = 1/(1 - lambda_t*C_t*theta) * (C_t/H_t  + bet*(C_t/C_t_p1)*q_t_p1);

else
    
    B_t = theta*q_t*H_t;
    
    C_t = max(0.000000000001,B_t + Y_t - R*B_t_m1);
    
    %C_t = B_t + Y_t - R*B_t_m1;
    
    if Cheb_sw ==1
        
        T_B_p1    = NaN(n_poly,1);
        
        z_B       = 2*(B_t - B_min)/(B_max - B_min) - 1 ;
        
        for i=1:n_poly
            if i == 1 || i == 2
                T_B_p1(i,:) = z_B'.^(i-1);
            else
                T_B_p1(i,:) = 2*z_B'.*T_B_p1(i-1,:) - T_B_p1(i-2,:);
            end
        end
        
        C_t_p1 = Trans(yy,:)*(T_B_p1'*Cheb_C)';
        
        q_t_p1 = Trans(yy,:)*(T_B_p1'*Cheb_q)';
        
    else
        
    %interpolate expectations
    
    B_idx  = find(B_t >= B_grid(:,1), 1,'last');
    
    if isempty(B_idx)
        
        B_idx = 1;
        
    elseif B_idx >= length(B_grid(:,1))
        
        B_idx = length(B_grid(:,1)) - 1;
        
    end
    
    C_t_p1 = Trans(yy,:)*((C_t_grid(B_idx+1,:) - C_t_grid(B_idx,:))/(B_grid(B_idx+1,:) - B_grid(B_idx,:)) * (B_t - B_grid(B_idx,:)) + C_t_grid(B_idx,:))';  
    
    q_t_p1 = Trans(yy,:)*((q_t_grid(B_idx+1,:) - q_t_grid(B_idx,:))/(B_grid(B_idx+1,:) - B_grid(B_idx,:)) * (B_t - B_grid(B_idx,:)) + q_t_grid(B_idx,:))';
        
    end

    lambda_t = (1 - bet*(C_t/C_t_p1)*R)/C_t;

    q_t = 1/(1 - lambda_t*C_t*theta) * ( C_t/H_t + bet*(C_t/C_t_p1)*q_t_p1 );

end



