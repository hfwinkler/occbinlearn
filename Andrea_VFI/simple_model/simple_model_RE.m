
clear all

global n_poly B_ss lambda Cheb_sw

%% Parameters
bet    =     1.04^-0.25;
R_ss   =      1.03^0.25;
theta  =            0.5;
H_ss   =              1;
Y_ss   =              1;
rho_y  =            0.9;
ss     =            0.1;

RE_sw   =             1;
Cheb_sw =             0;

% dampening parameter

lambda   = 0;

% State Space and Expectations
n        = 50; % # of Chebyschev's nodes
n_poly   = 5;

% Convergence criteria parameters
diff     = 10;
max_iter = 10000;
tol      = 10^-6;
dW       = 1;

%% set up

z = NaN(n,1);

for i = 1:n
    
    z(i) = -cos(pi*(2*i - 1)/(2*n));
    
end

Y_t           = Y_ss;
q_ss          = Y_t/(1+(R_ss-1)*theta-(1-bet*R_ss)*theta-bet);
B_ss          = theta*q_ss;
C_ss          = Y_t + (1-R_ss)*B_ss;

B_min         = B_ss-.2*B_ss;
B_max         = B_ss+.05*B_ss;
B_grid        = (z + 1) * (B_max - B_min)/2 + B_min;
n_L           = length(B_grid);  % # of L_0 grid points

n_Y             = 3;
[nodes, weight] = gauss_hermite_weights_and_nodes(n_Y);
Y_grid          = Y_ss + ss*nodes;

if n_Y == 1;
    
    Trans = 1;
    
elseif n_Y == 2
    
    Trans = [0.1    0.9
            0.05   0.95];
    
elseif n_Y == 3
    
    Trans = [0.10    0.8    0.1
             0.025   0.95   0.025
             0.1    0.8    0.1];
    
end

C_init = C_ss + ss*nodes';
q_init = q_ss + ss*nodes';

C_t_grid      = repmat(C_init,n_L,1);
q_t_grid      = repmat(q_init,n_L,1);
B_t_grid      = repmat(B_grid,1,n_Y);
C_t_grid_p1   = C_t_grid;
q_t_grid_p1   = q_t_grid;
B_t_grid_p1   = B_t_grid;
Y_t_grid      = Y_ss*ones(n_L,n_Y);
lambda_t      = zeros(n_L,n_Y);
R_t_grid      = repmat(R_ss,n_L,n_Y);
V_t_grid      = repmat(log(0.04)/(1-bet),n_L,n_Y);

T = NaN(n_poly,n);

for i=1:n_poly
    if i == 1 || i == 2
        T(i,:) = z'.^(i-1);
    else
        T(i,:) = 2*z'.*T(i-1,:) - T(i-2,:);
    end
end

%% iterate

load initial_cond B_t_grid C_t_grid q_t_grid

Cheb_V    = (T*T')\T*V_t_grid;
Cheb_q    = (T*T')\T*q_t_grid;
Cheb_C    = (T*T')\T*C_t_grid;
Cheb_B    = (T*T')\T*B_t_grid;

Cheb_V_p1 = Cheb_V;
Cheb_q_p1 = Cheb_q;
Cheb_C_p1 = Cheb_C;
Cheb_B_p1 = Cheb_B;

ii = 1;

jj = 1;

while ii < max_iter && dW > tol;
    
    for yy = 1:n_Y

        Y_t_m1 = Y_grid(yy);
        
        for jj  = 1:n_L
            
            B_t_m1 = B_grid(jj);
            T_B_j  = T(:,jj);
            C_t_m1 = C_t_grid(jj,:);
            q_t_m1 = q_t_grid(jj,:);
            
            options = optimset('Display', 'off', 'Algorithm', 'interior-point','TolFun',10e-9,'TolX',10e-9);
            
            if Cheb_sw == 1
                
                [C_t, q_t, B_t, lambda_t] = FOCS(B_t_m1, Y_t_m1, yy, Trans, Cheb_C, Cheb_q, T_B_j, bet, theta, H_ss, R_ss, B_min, B_max, C_t_m1, q_t_m1,B_grid, C_t_grid, q_t_grid);
                
            else
                
                [C_t, q_t, B_t, lambda_t] = FOCS(B_t_m1, Y_t_m1, yy, Trans, Cheb_C, Cheb_q, T_B_j, bet, theta, H_ss, R_ss, B_min, B_max, C_t_m1, q_t_m1,B_grid, C_t_grid, q_t_grid);
                
            end
            
            %[C_t] = fminsearch(@(C_t) Value_function(C_t, B_t_m1, bet, theta, H_ss, Y_ss, R_ss, B_min, B_max, Cheb_V, Cheb_q, Cheb_C), C_ss, options);
            
            %[V_t, C_t, q_t, B_t, lambda_t, Y_t, R_t] = Value_function(C_t, B_t_m1, bet, theta, H_ss, Y_ss, R_ss, B_min, B_max, Cheb_V, Cheb_q, Cheb_C);
            
            %V_t_grid(jj)          = -V_t;
            C_t_grid_p1(jj,yy)          =  C_t;
            q_t_grid_p1(jj,yy)          =  q_t;
            B_t_grid_p1(jj,yy)          =  B_t;
            %Y_t_grid(jj)          =  Y_t;
            %R_t_grid(jj)          =  R_t;
            
        end
        
        
    end    
    
    
    if Cheb_sw == 1
        
        %Cheb_V_p1    = (T*T')\T*V_t_grid;
        Cheb_C_p1    = (T*T')\T*C_t_grid_p1;
        Cheb_q_p1    = (T*T')\T*q_t_grid_p1;
        Cheb_B_p1    = (T*T')\T*B_t_grid_p1;
        %Cheb_Y_p1    = (T*T')\T*Y_t_grid;
        %Cheb_R_p1    = (T*T')\T*R_t_grid;
        
        if isnan(sum(q_t_grid))
            return
        end
        
        %dW = max(max(norm(Cheb_V_p1 - Cheb_V), norm(Cheb_q_p1 - Cheb_q)), norm(Cheb_C_p1 - Cheb_C))
        
        %max(norm(Cheb_V_p1 - Cheb_V))
        
        dW = max(max(norm(Cheb_B_p1 - Cheb_B), norm(Cheb_q_p1 - Cheb_q)), norm(Cheb_C_p1 - Cheb_C))
        
        Cheb_C    = lambda*Cheb_C_p1 + (1-lambda)*Cheb_C;
        Cheb_q    = lambda*Cheb_q_p1 + (1-lambda)*Cheb_q;
        Cheb_B    = lambda*Cheb_B_p1 + (1-lambda)*Cheb_B;
        
    else
        dW = max(max(norm(C_t_grid_p1 - C_t_grid)), norm(q_t_grid_p1 - q_t_grid)) 
        
        C_t_grid = C_t_grid_p1;
        q_t_grid = q_t_grid_p1;
        B_t_grid = B_t_grid_p1;
        
    end
    
    
    ii           = ii + 1;
    
    if dW< tol
        iConvergence = 1;
        disp('converged!')
    end
    
    
end

%% plot policy functions

figure;mesh(B_grid,Y_grid,q_t_grid')
title('Asset price')
xlabel('Debt')
ylabel('Output')

figure;mesh(B_grid,Y_grid,C_t_grid')
title('Consumption')
xlabel('Debt')
ylabel('Output')


%% Simulate the model

n_simul = 1000;

C_t_simul = zeros(n_simul,1);
B_t_simul = zeros(n_simul,1);
q_t_simul = zeros(n_simul,1);
Y_t_simul = zeros(n_simul,1);

[seq, states] = hmmgenerate(n_simul, Trans, [1,0,0]');

B_t_m1 = B_ss*0.5;

for ii = 1:n_simul

    Y_t_simul(ii) = Y_grid(states(ii));
    
    if Cheb_sw == 1
    
        T_B_p1    = NaN(n_poly,1);
        
        z_B       = 2*(B_t_m1 - B_min)/(B_max - B_min) - 1 ;
        
        for i=1:n_poly
            if i == 1 || i == 2
                T_B_p1(i,:) = z_B'.^(i-1);
            else
                T_B_p1(i,:) = 2*z_B'.*T_B_p1(i-1,:) - T_B_p1(i-2,:);
            end
        end
        
        C_t_simul(ii) = T_B_p1'*Cheb_C(:,states(ii));
        q_t_simul(ii) = T_B_p1'*Cheb_q(:,states(ii));
        B_t_simul(ii) = T_B_p1'*Cheb_B(:,states(ii));
        
        B_t_m1        = B_t_simul(ii);
        
    else
        
        B_idx  = find(B_t_m1 >= B_grid(:,1), 1,'last');
        
        if isempty(B_idx)
            
            B_idx = 1;
            
        elseif B_idx >= length(B_grid(:,1))
            
            B_idx = length(B_grid(:,1)) - 1;
            
        end
        
        C_t = ((C_t_grid(B_idx+1,states(ii)) - C_t_grid(B_idx,states(ii)))/(B_grid(B_idx+1) - B_grid(B_idx)) * (B_t_m1 - B_grid(B_idx)) + C_t_grid(B_idx,states(ii)));
        q_t = ((q_t_grid(B_idx+1,states(ii)) - q_t_grid(B_idx,states(ii)))/(B_grid(B_idx+1) - B_grid(B_idx)) * (B_t_m1 - B_grid(B_idx)) + q_t_grid(B_idx,states(ii)));
        B_t = ((B_t_grid(B_idx+1,states(ii)) - B_t_grid(B_idx,states(ii)))/(B_grid(B_idx+1) - B_grid(B_idx)) * (B_t_m1 - B_grid(B_idx)) + B_t_grid(B_idx,states(ii)));
        
        C_t_simul(ii) = C_t;
        q_t_simul(ii) = q_t;
        B_t_simul(ii) = B_t;
        
        B_t_m1        = B_t_simul(ii);

    end
    
end

prob_binding_constr = sum(B_t_simul > theta*q_t_simul*H_ss)/n_simul

return