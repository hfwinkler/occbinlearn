function [V_t, C_t, q_t, B_t, lambda_t, Y_t, R] = Value_function_q(C_t, q_t_m1, bet, theta, H_ss, Y_ss, R_ss, q_min, q_max, Cheb_V, Cheb_q)
              
global n_poly

T_q    = NaN(n_poly,1);

z_q = 2*(q_t_m1 - q_min)/(q_max - q_min) - 1 ;

for i=1:n_poly
    if i == 1 || i == 2
        T_q(i,:) = z_q'.^(i-1);
    else
        T_q(i,:) = 2*z_q'.*T_q(i-1,:) - T_q(i-2,:);
    end
end

R   = R_ss;

H_t = H_ss;

Y_t = Y_ss;

B_t = C_t + R*B_t_m1 - Y_t;

q_t    = T_q'*Cheb_q;

T_q    = NaN(n_poly,1);

z_q = 2*(B_t - q_min)/(q_max - q_min) - 1 ;

for i=1:n_poly
    if i == 1 || i == 2
        T_q(i,:) = z_q'.^(i-1);
    else
        T_q(i,:) = 2*z_q'.*T_q(i-1,:) - T_q(i-2,:);
    end
end

q_t_p1 = T_q'*Cheb_q;

if B_t < theta*q_t*H_t;
    
    lambda_t = 0;
    
    C_t_p1 = bet*(C_t)*R / (1-lambda_t*C_t);
    
    q_t = C_t/H_t + lambda_t*C_t*theta*q_t + bet*(C_t/C_t_p1)*q_t_p1;
    
else
    
    q_t = B_t/(theta*H_t);
    
    C_t_p1 = (- bet*(C_t)*R*theta*q_t + bet*(C_t)*q_t_p1)/(q_t*(1 -  theta) - C_t/H_t);
    
    lambda_t = -(bet*(C_t/C_t_p1)*R - 1)/C_t;
    
end


V_t_p1    = T_q'*Cheb_V;

V_t = log(C_t) + log(H_t) + bet * V_t_p1;

V_t = -V_t;    
    

end