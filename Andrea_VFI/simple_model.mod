var C_t,   H_t,  q_t,   B_t, mu_t, lambda_t, Y_t, R;

varexo eps_a;

parameters bet, theta, H, rho_y, ss, Y_ss, R_ss;

bet       = 0.99;
R_ss      = 1/bet - 0.005;
theta     = 0.2;      
H         = 1;
ss        = 0.005;
rho_y     = 0.95;
Y_ss      = 0.1;

model;

mu_t = 1/C_t;

1-lambda_t*C_t = bet*(C_t/C_t(+1))*R;

q_t = C_t/H_t + lambda_t*C_t*theta*q_t + bet*(C_t/C_t(+1))*q_t(+1);

B_t = theta*q_t*H_t;

H_t = 1;

C_t + R*B_t(-1) = B_t + Y_t;

Y_t = Y_ss;%(1-rho_y)*Y_ss + rho_y*Y_t(-1) + eps_a;

R   = R_ss;

end;

shocks;

var eps_a      = ss^2;

end;

initval;
Y_t      = Y_ss;
q_t      = Y_t/(1+(R_ss-1)*theta-(1-bet*R_ss)*theta-bet);
B_t      = theta*q_t;
C_t      = Y_t + (1-R_ss)*B_t;
lambda_t = (1-bet*R_ss)/C_t;
mu_t     = 1/C_t;
H_t      = 1;
end;

stoch_simul(periods=0, IRF=20, order = 1, drop = 0, nograph) C_t,   H_t,  q_t,   B_t, mu_t, lambda_t, Y_t;
