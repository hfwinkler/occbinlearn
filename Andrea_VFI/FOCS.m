function [C_t, q_t, B_t, lambda_t] = FOCS(Cheb_C, Cheb_q, T_B)

C_t = T_B'*Cheb_C;

q_t = T_B'*Cheb_q;

R   = R_ss;

Y_t = Y_ss;%(1-rho_y)*Y_ss + rho_y*Y_t(-1) + eps_a;

H_t = 1;

B_t = C_t - Y_t + R*B_t_m1;

T_B_p1    = NaN(n_poly,1);

z_B    = 2*(B_t - B_min)/(B_max - B_min) - 1 ;

for i=1:n_poly
    if i == 1 || i == 2
        T_B_p1(i,:) = z_B'.^(i-1);
    else
        T_B_p1(i,:) = 2*z_B'.*T_B_p1(i-1,:) - T_B_p1(i-2,:);
    end
end


if B_t < theta*q_t*H_t

    lambda_t = 0;

    C_t_p1 = bet*(C_t)*R/(1-lambda_t*C_t);

    q_t = C_t/H_t + lambda_t*C_t*theta*q_t + bet*(C_t/C_t_p1)*q_t(+1);

else
    
    B_t = theta*q_t*H_t;
    
    C_t_p1 = T_B_p1'*Cheb_C;
    
    q_t_p1 = T_B_p1'*Cheb_q;

    lambda_t = (1 - bet*(C_t/C_t_p1)*R)/C_t;

    q_t = C_t/H_t + lambda_t*C_t*theta*q_t + bet*(C_t/C_t_p1)*q_t_p1;

end



