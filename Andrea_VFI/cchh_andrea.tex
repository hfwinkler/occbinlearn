\documentclass[12pt]{article}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage[margin=1in]{geometry}
\usepackage{helvet}

\begin{document}

\section*{The Household}

The economy is populated by a large household that in each period $t$ consumes non-durable goods $C_t$ and housing $H_t$, to maximize the utility function:

\begin{equation} \label{EQ1}
\max_{\left(C_{t}, H_{t}\right)_{t=0}^{\infty}}\mathbb{E}\sum_{t=0}\beta^{t}\left(\log C_{t}+\log H_{t}\right)
\end{equation}


 The household is composed by a measure 1 of members. At the start of every period $t$, the head of the household entrusts each member with the same endowment of wealth, equal to a fraction of the household stock of housing $q_th_{t-1}$ (with $\int h_{i,t-1} di = H_{t-1}$, $q_t$ being the unit price of real estate) plus an equal fraction of bonds issued in period $t-1$ that will come to maturity in period $t$, $b_{t-1}^{+}$, minus a fraction of the stock of debt issued by household members in period $t-1$, $b_{t-1}^{-}$. Members also receive a homogenous and exogenous income flow $y_{i,t}$, with $\int y_{t}di = Y_t$, and
\begin{equation} \label{EQ2}
 Y_t = (1-\rho_y)Y_{ss} + \rho_y Y_{t-1} + \epsilon_t^y.
\end{equation}
where $Y_{ss}$ equals to the non-stochastic steady state level of aggregate output.

After receiving their endowment and their income, members leave the household and a fraction $\chi$ of them (indexed by 'b', as in borrower) is presented with a purchase opportunity on the real estate market, $q_th_{b,t}$, where $h_{b,t}$ is the purchase of member $b$. These members can issue bonds, $b_{b,t}^{-}$, on financial markets to finance the real estate purchase.

The budget constraint of members with a real estate opportunity will then be:
\begin{equation} \label{EQ3}
c_{b,t} + q_t(h_{b,t} - h_{t-1}) + R_{t-1}b_{t-1}^{-} \leq y_{b,t} + b_{b,t}^{-} + R_{t-1}b_{t-1}^{+}.
\end{equation}
Note that $b_{b,t}^{-}$ enters the budget constraint as a source of funds. we will show that in equilibrium borrowers will optimally issue bonds to fund the purchase of housing stock. We assume that borrowers need to satisfy the borrowing constraint:
\begin{equation} \label{EQ4}
b_{b,t}^{-} \leq \theta q_t h_{b,t}
\end{equation}
so that the amount of bonds issued cannot be higher than a fraction $\theta$ of the value of their intended housing stock

Household members without a real estate opportunity (indexed by 'l' as in lenders) instead will face a budget constraint equal to:

\begin{equation} \label{EQ5}
c_{l,t} + R_{t-1}b_{t-1}^{-} + b_{l,t}^{+} \leq y_{b,t} + R_{t-1}b_{t-1}^{+} + q_t h_{t-1}
\end{equation}
Note that $b_{l,t}^{+}$ enters the budget constraint as a use of funds. I will show that in equilibrium lenders will optimally purchase bonds issued by borrowers to fund the purchase of housing stock.

Note also that on aggregate, in a closed economy, bonds issued in each period are in zero net supply and do not enter the definition of wealth of the household, so that $\int b_{l,t}^{+} di - \int b_{b,t}^{-} di = 0$. Similarly for each member $R_{t-1}b_{t-1}^{-} = R_{t-1}b_{t-1}^{+}$, since they all receive the same fraction of assets and liabilities. Note that we are assuming that bonds are default-free and that no intermediation costs affect the spread between interest raid paid to/by household members.

On aggregate, the budget constraint of the household is then equal to:

\begin{equation} \label{EQ6}
C_t + q_t(H_t - H_{t-1}) \leq Y_t.
\end{equation}
with $C_t = \int c_{i,t} di$ and $H_t = \int h_{i,t} di$.

\subsection*{The Maximization Problem}

Before uncertainty over real estate opportunity is resolved, the head of the household will choose members' contingent plans for the purchase of consumption and housing goods, to maximize aggregate utility:
\begin{equation} \label{EQ7}
\max_{\left(c_{i,t}, h_{i,t}\right)_{t=0}^{\infty}}\mathbb{E}\sum_{t=0}\beta^{t}\left(\log \int c_{i,t} di+\log \int h_{i,t} di\right)
\end{equation}
subject to the constraints from (\ref{EQ8}) - (\ref{EQ12}).

Budget and borrowing constraints for members $i = b \in [0, \chi]$:
\begin{equation} \label{EQ8}
 c_{b,t} + q_t(h_{b,t} - H_{t-1}) + R_{t-1}b_{t-1}^{-} \leq y_{b,t} + b_{b,t}^{-} + R_{t-1}b_{t-1}^{+}.
\end{equation}

\begin{equation} \label{EQ9}
b_{b,t}^{-} \leq \theta q_t h_{b,t}
\end{equation}

Budget constraint for members $i = l \in (\chi, 1]$:
\begin{equation} \label{EQ10}
  c_{l,t} + R_{t-1}b_{t-1}^{-} + b_{l,t}^{+} \leq y_{l,t} + R_{t-1}b_{t-1}^{+} + q_t H_{t-1}
\end{equation}
Definition of the aggregate housing stock:
\begin{equation} \label{EQ11}
H_t = \int h_{i,t} di
\end{equation}
The head of the household will also take into account the aggregate process for output:
\begin{equation} \label{EQ12}
 Y_t = (1-\rho_y)Y_{ss} + \rho_y Y_{t-1} + \epsilon_t^y.
\end{equation}
We also impose that consumption of members needs to be non-negative:
\begin{equation} \label{EQ13}
c_{b,t} \geq 0
\end{equation}
\begin{equation} \label{EQ14}
c_{l,t} \geq 0
\end{equation}
We attach multipliers $\mu_t^b$, $\lambda_t^b$, $\mu_t^l$, $\mu_t^{\Sigma h}$  to equations (\ref{EQ8}) to (\ref{EQ11}), and multipliers $\mu_t^{c_b +}$ and $\mu_t^{c_l +}$  to the non-negativity constraints (\ref{EQ13}) and (\ref{EQ14}). The aggregate resource constraint in (\ref{EQ6}) is equal to the sum of (\ref{EQ8}) and (\ref{EQ10}) over the two sets of household members and is therefore reduntant.

\subsection*{The First Order Conditions for the Household}

\begin{equation}\label{EQ15}
c_{b,t}: \mu_t^b - \mu_t^{c_b +} = \frac{1}{C_t}
\end{equation}

\begin{equation}\label{EQ16}
h_{b,t}: q_t =  \frac{1}{\mu_t^b - \lambda_{t}^b \theta} \left( \frac{1}{H_t} + \mu_t^{\Sigma h} \right)
\end{equation}

\begin{equation}\label{EQ17}
c_{l,t}: \mu_t^l - \mu_t^{c_l +} = \frac{1}{C_t}
\end{equation}

\begin{equation}\label{EQ18}
H_{t}: \mu_t^{\Sigma h} = \beta E_t\left[\chi \mu_{b,t+1} q_{t+1} + (1-\chi) \mu_{l,t+1} q_{t+1} \right] 
\end{equation}

\begin{equation}\label{EQ19}
b_{b,t}^-: \mu_{b,t} - \lambda_{t}^b = \beta E_t\left[ R_t (\chi \mu_{b,t+1} + (1-\chi) \mu_{l,t+1}) \right] 
\end{equation}

\begin{equation}\label{EQ20}
b_{b,t}^+: \mu_{l,t} = \beta E_t\left[ R_t (\chi \mu_{b,t+1} + (1-\chi) \mu_{l,t+1}) \right] 
\end{equation}

Complementary slackness conditions:
\begin{equation}
\lambda_{t}^b(b_{b,t}^{-} - \theta q_t h_{b,t}) = 0
\end{equation}

\begin{equation}
\mu_t^{c_b +}c_{b,t} = 0
\end{equation}

\begin{equation}
\mu_t^{c_l +}c_{l,t} = 0
\end{equation}
I omit the constraints (reported above) and the transversality condition.

We can characterize the equilibrium conditions over the state space depending on whether the borrowing constraint binds or not.

First, consider the case in which the borrowing constraint is not binding. That means that the multiplier:
\begin{equation}
\lambda_{t}^b = 0
\end{equation}
and that the amount of borrowing will be equal to:
\begin{equation}
b_{b,t}^{-} \leq \theta q_t h_{b,t}
\end{equation}
The right-hand sides of equations (\ref{EQ19}) and (\ref{EQ20}) are the same, and since $\lambda_{t}^b = 0$, it follows that the multipliers:
\begin{equation}
\mu_t^b = \mu_t^l
\end{equation}
From (\ref{EQ15}) and (\ref{EQ16}), this equality implies that:
\begin{equation}
\mu_t^{c_b +}  = \mu_t^{c_l +} = 0
\end{equation}
since  aggregate consumption $C_t$ would be zero if both $c_{l,t}$ and $c_{b,t}$ were zero ($\mu_t^{c_b +}  = \mu_t^{c_l +} > 0$).
To sum up, if the borrowing constraint does not bind, the household's Euler equations become:
\begin{equation}
	q_t =  C_t \left( \frac{1}{H_t} + \beta E_t\left[\chi \mu_{b,t+1} q_{t+1} + (1-\chi) \mu_{l,t+1} q_{t+1} \right]  \right)
\end{equation}

\begin{equation}
	\frac{1}{C_t} =  \beta E_t\left[ R_t (\chi \mu_{b,t+1} + (1-\chi) \mu_{l,t+1}) \right] 
\end{equation}
where the expected values of the multipliers $\mu_{b,t+1}$ and $\mu_{l,t+1}$ will reflect the likelihood that the borrowing constraint could be binding in period $t+1$. When the constraint is not binding, $\mu_{b,t} = \mu_{l,t} = \frac{1}{C_t}$. 

Second, consider the case in which the borrowing constraint is binding. This implies that the multiplier:
\begin{equation}
\lambda_{t}^b > 0
\end{equation}
and that borrowing will be constrained at:
\begin{equation}
b_{b,t}^{-} = \theta q_t h_{b,t}
\end{equation}
The fact that $\lambda_{t}^b > 0$ implies, from equations (\ref{EQ19}) and (\ref{EQ20}) that:
\begin{equation}
\mu_t^b > \mu_t^l.
\end{equation}
Equation \ref{EQ15} and the non-negativity requirement for aggregate consumption $C_t$ imply that:
\begin{equation}
\mu_t^{c_b +} = \lambda_{t}^b >0
\end{equation}
which means that borrowers' purchase of consumption goods is zero $c_{b,t} = 0$ when the borrowing constraint binds.
To conclude, if the borrowing constraint binds, borrowers borrow up to their limit, reduce consumption to zero and purchase as much real estate as their income, wealth and debt position allows them to do:
\begin{equation}
q_t(h_{b,t} - \theta h_{b,t}) = \left( y_{b,t} + q_t H_{t-1} - R_{t-1}b_{t-1}^{-} + R_{t-1}b_{t-1}^{+} \right).
\end{equation}
Aggregating over borrowers, one can find an expression for $H_t$:
\begin{equation}
H_{t} = \frac{\chi}{q_t(1-\theta)} \left( Y_{b,t} + q_t H_{t-1}  - R_{t-1}b_{t-1}^{-} + R_{t-1}b_{t-1}^{+} \right).
\end{equation}
 On the other hand, lenders purchase consumption goods and bonds. The Euler equations for the household then become:
\begin{equation}
		q_t =  \frac{C_t}{1 - C_t \theta \lambda_{t}^b} \left( \frac{1}{H_t} + \beta E_t\left[\chi \mu_{b,t+1} q_{t+1} + (1-\chi) \mu_{l,t+1} q_{t+1} \right] \right)
\end{equation}
which can be solved for $\lambda_{t}^b$ for each value of $q_t$. The Euler equation for bonds is instead:
\begin{equation}
\frac{1}{C_t} = \beta E_t\left[ R_t (\chi \mu_{b,t+1} + (1-\chi) \mu_{l,t+1}) \right].
\end{equation}
The expected values of the multipliers $\mu_{b,t+1}$ and $\mu_{l,t+1}$ will reflect the likelihood that the borrowing constraint could be binding in period $t+1$. When the constraint is binding, $\mu_{l,t} = \frac{1}{C_t}$, while $\mu_{b,t} = \frac{1}{C_t} + \lambda_{t}^b$. 

\section{The Monetary Policy Authority}

In this first approximation of the model, we assume that the monetary policy authority directly controls the real rate of interest, $R_t$, according to the Taylor-type rule:

\begin{equation} 
R_t = R_ss + \phi_y(Y_t - Y_{ss});
\end{equation}

In the next iteration, we will model intermediate good producers as monopolistically competitive firms, with a production function in labor inputs buffeted by TFP shocks. Intermediate producers will be subject to frictions in setting the nominal prices of their goods (most likely following Rotemberg). Consumption goods will be produced as a Dixit Stiglitz aggregator of intermediate goods. 

This formulation will give rise to a standard forward-looking Phillips curve.


\end{document}

%We will set up a model of a household with a preference for housing, and
% who is facing a borrowing constraint that depends on the value of the housing
% stock he owns.
% There is a precautionary motive in that even when the borowing constraint
% does not bind, the expectation that it might bind in the future will reduce
% the consumption of the agent today in order to reduce borrowing and move
% away from the constraint today.
%\end_layout
%
%\begin_layout Standard
%Here is a model so simple it hurts.
% A consumer solves:
%\end_layout
%
%\begin_layout Standard
%\begin_inset Formula 
%\[
%\max_{\left(C_{t}\right)_{t=0}^{\infty}}\mathbb{E}\sum_{t=0}\beta^{t}\left(\log C_{t}+\log H_{t}\right)
%\]
%
%\end_inset
%
%
%\begin_inset Formula 
%\begin{align*}
%s.t.\,C_{t}+q_{t}\left(H_{t}-H_{t-1}\right)+RB_{t-1} & =B_{t}+\bar{y}\\
%B_{t} & \leq\theta q_{t}H_{t}
%\end{align*}
%
%\end_inset
%
%Debt 
%\begin_inset Formula $B_{t}$
%\end_inset
%
% is in elastic supply at the constant interest rate 
%\begin_inset Formula $R<1/\beta$
%\end_inset
%
%.
% The housing stock is exogenous at 
%\begin_inset Formula $H_{t}=1$
%\end_inset
%
%.
% The income endowment 
%\begin_inset Formula $\bar{y}$
%\end_inset
%
% is also constant.
% There are no shocks.
% The first order conditions are: 
%\begin_inset Formula 
%\begin{align*}
%1-\lambda_{t}C_{t} & =\mathbb{E}_{t}\beta\frac{C_{t}}{C_{t+1}}R\\
%q_{t} & =C_{t}/H_{t}+\lambda_{t}C_{t}\theta q_{t}+\mathbb{E}_{t}\beta\frac{C_{t}}{C_{t+1}}q_{t+1}
%\end{align*}
%
%\end_inset
%
%
%\end_layout
%
%\begin_layout Standard
%One can solve for consumption and housing as a function of the multiplier
% on the borrowing constraint:
%\begin_inset Formula 
%\begin{align*}
%C_{t} & =\frac{1}{\sum_{s=0}^{\infty}\left(\beta R\right)^{s}\lambda_{t}}\\
%H_{t} & =\frac{C_{t}}{q_{t}-\lambda_{t}C_{t}\theta q_{t}-\mathbb{E}_{t}\beta\frac{C_{t}}{C_{t+1}}q_{t+1}}
%\end{align*}
%
%\end_inset
%
%
%\end_layout
%
%\begin_layout Standard
%The rational expectations equilibrium only has one state variable, 
%\begin_inset Formula $B_{t-1}$
%\end_inset
%
% because the housing choice is understood to be constrained to 
%\begin_inset Formula $H_{t}=1$
%\end_inset
%
%.
% What's worth noting is that the future multiplier 
%\begin_inset Formula $\lambda_{t+1}$
%\end_inset
%
% does not appear in the FOCs today: When the borrowing constraint is slack
% at 
%\begin_inset Formula $t$
%\end_inset
%
%, both intertemporal Euler equations between 
%\begin_inset Formula $t$
%\end_inset
%
% and 
%\begin_inset Formula $t+1$
%\end_inset
%
% are undistorted.
% But the household still has a precautionary motive.
%\end_layout
%
%\begin_layout Standard
%The learning equilibrium, by contrast, will have four state variables in
% the perceived law of motion (
%\begin_inset Formula $B_{t-1},H_{t-1},q_{t-1},\hat{\mu}_{t-1}$
%\end_inset
%
% ) and three in the actual law of motion (
%\begin_inset Formula $B_{t-1},q_{t-1},\hat{\mu}_{t-1}$
%\end_inset
%
% ).
% In the perceived law of motion, the household believes that 
%\begin_inset Formula $q_{t}$
%\end_inset
%
% evolves according to 
%
%\begin{align*}
%\Delta\log q_{t} & =\hat{\mu}_{t-1}+z_{t}\\
%\hat{\mu}_{t} & =\hat{\mu}_{t-1}+gz_{t}
%\end{align*}
%%
%and accordingly believes that housing is in elastic supply at the price
% given above.
% Solving the agent's problem leads to a demand function for housing 
%\[ H_{t}=f\left(B_{t-1},H_{t-1},q_{t-1},\hat{\mu}_{t-1},z_{t}\right)\]
%
%.
% The equilibrium price can then be found by solving 
%\[ 1=f\left(B_{t-1},1,q_{t-1},\hat{\mu}_{t-1},z_{t}\right)
%\]
%
%for $z_{t}$
%
% each period.
% From the Euler equation for housing, the price will satisfy
%
%\[
%q_{t}=\frac{C_{t}/H_{t}}{1-\lambda_{t}\theta C_{t}-\mathbb{E}_{t}\beta\frac{C_{t}}{C_{t+1}}\exp\left(\hat{\mu}_{t}+0.5\sigma_{z}^{2}\right)}
%\]
%
%
%It is possible, although less appealing as a a modelling device, to delegate
% the housing choice to an investment agency as in the previous notes on
% firm borrowing constraints.
%
%This model can be extended to make it closer to the standard models used
% in this literature, i.e.
% introducing a second, patient household and having the economy closed.
% This way, the interest rate as well as the housing stock holdings of the
% borrowing household become endogenous variables (and also state variables).
% Other bells and whistles like endogenous production, capital accumulation
% with adjustment costs, shocks etc.
% can be added until we get to the huge Guerrieri-Iacoviello model.
%
%\end{document}
