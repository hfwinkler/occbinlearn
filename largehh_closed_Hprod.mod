% Dynare file to simulate large household model
% with the constraint always binding
% closed economy version
% housing production subject to adjustment cost

var C H N b B;
var W Q r;
var lambda;
var disttoconstraint;
var dH Pi Hs;

varexo epsw;

parameters betta nu omega phi;
parameters chi theta;
parameters sigmaw rhow;
parameters psi deltaH;

@#include "largehh_parameters.m"

%shorthand notation:

model;

%borrower BC
exp(Q+H) = b + chi*(exp(W+N)+(1-deltaH)*exp(Q+H(-1))+B-(1+r(-1))*B(-1)+Pi);
%aggregate BC
exp(C)+exp(Q)*(exp(H)-(1-deltaH)*exp(H(-1))) = exp(W+N)+B-(1+r(-1))*B(-1)+Pi;
%borrowing constraint
chi*B+b = theta*exp(Q+H);
disttoconstraint=0;
%borrower FOC for housing:
exp(Q-phi*C)*(1+(1-theta)*lambda) = exp(-H) + betta*(1-deltaH)*exp(Q(+1))*exp(-phi*C(+1))*(1+chi*lambda(+1));
%lender FOC for loans:
exp(-phi*C) = betta*(1+r)*exp(-phi*C(+1))*(1+chi*lambda(+1));
%labor FOC:
omega*exp(nu*N)=exp(W-phi*C)*(1+chi*lambda);

%labor demand
W=rhow*W(-1)+sigmaw*epsw;
%housing supply
exp(Hs)=exp(dH)+(1-deltaH)*exp(H(-1));
exp(Q)=1+psi*(exp(dH-H(-1))-deltaH);
Pi=exp(Q+Hs)-exp(dH)-(1-deltaH)*exp(Q+H(-1))-psi/2*(exp(dH-H(-1))-deltaH)^2;
%house market clearing
H=Hs;
%aggregate loan supply
B=0;

end;

%initial SS values from case with phi=1 (fingers crossed)
initval;

%exogenous
W=0;
B=0;

%housing SS
Q=0;
Pi=0;

%multiplier:
lambda = (1-theta-chi*(1+deltaH+(1-betta)*(1-deltaH))) / chi / (1 - theta - betta*chi*(1-deltaH));

%other:
C = 1/(1+nu)*(-log(omega)+log(1+chi*lambda)) - nu/(1+nu)*log(1+deltaH*chi/(1-theta-chi));
H = C + log(chi) - log(1-theta-chi);
N = 1/nu*(-log(omega)+log(1+chi*lambda)) -1/nu*C;

Hs=H;
dH=log(deltaH)+H;

r = 1/(1+chi*lambda)/betta-1;
b = theta*exp(Q+H);
disttoconstraint=0;

end;

shocks;
var epsw; stderr 1;
end;


%stoch_simul(order=1,irf=20,nocorr,ar=0);
stoch_simul(order=1,irf=100,nomoments,nograph);
%save SS for learning solution
ys_RE=oo_.dr.ys;
save largehh_closed_Hprod_SS ys_RE;
cleanup_dyn(M_.fname);
